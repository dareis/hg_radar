
''' 
code to setup LMX2492 before writing the code in cpp

Darren Reis
10/2018

10/2019 - updated to a class
    designed for FPGA - Python commands based on "LMX Driver" on google Drive 

11/2019 - finished creating all config commands, added in ramping class
'''


import numpy as np
import struct
import argparse
import sys, os
import time
import json
import socket
import copy



# HG les import
import utils as ru



class lmxMemory(object):
    
    NUM_DEAD = 10
    def __init__(self, rList):
        ''' start a memory map object '''
        
        # registers 46-56 do not exist
        if len(rList) != 131:
            print("memory init error:  memory map "  +
                "takes in 131 registers, given %d" % len(rList))
        
        self.rList = copy.copy(rList)
        self.num = 131  # total number of registers 
        
    def get_top(self):
        ''' get the registers from 141 to 57, descending order '''
        return self.rList[:85]
    def get_bottom(self):
        ''' get the registers from 45 to 0, descending order '''
        return self.rList[85:]
    def __getitem__(self, idx):
        '''  get an indexed register value.  remember the array is
            [r141, r140, r139, ... r58, r57, r45, r44, .... r2, r1, r0]
            So we need to do a different mapping.  The below mapping
            makes so when you ask for regN, you get the right element  '''
        if idx > self.num + self.NUM_DEAD or idx < 0:
            raise IndexError('memory index error: out of bounds')
            return
        elif idx >= 57:
            return self.rList[self.num + self.NUM_DEAD - idx]
        elif idx < 57 and idx > 45:
            raise IndexError('memory index error: invalid registers')
            return
        elif idx >= 0:
            return self.rList[- 1 - idx]
    
    def __setitem__(self, idx, item):
        if idx > self.num + self.NUM_DEAD or idx < 0:
            raise IndexError('memory index error: out of bounds')
            return
        elif idx >= 57:
            self.rList[self.num + self.NUM_DEAD - idx] = copy.copy(item)
        elif idx < 57 and idx > 45:
            raise IndexError('memory index error: invalid registers')
            return
        elif idx >= 0:
            self.rList[- 1 - idx] = item

    
class Ramper(object):
    """ Class to do calculations for LMX ramping """
    
    def __init__(self,):
        self.ref = 100 # MHz
        self.dblr = 1
        self.r = 1
        
        self.n = 122.5  
        self.num = 3774874
        self.den = 16777216
        self.ramps = None
        
    def update_sys(self):
        self.Fpd = (self.ref * (1+self.dblr)) / self.r
#         self.vco_start = self.Fpd * (self.n + np.floor(self.num*1.0/self.den))
        self.vco_start = self.Fpd * (self.n + self.num*1.0/self.den)
        self.acc_start = self.den*np.floor(self.n)+self.num
    
        self.acc_starts = [self.acc_start,]
        self.freq_starts = [self.vco_start,]  # might not be used
    
    def load_sys(self, n, num, den, dblr, r):
        ''' load in the config settings for doing calculations '''
        self.set_n(n)
        self.set_num(num)
#         self.set_den(den)    # den is always 16777216
        self.set_dblr(dblr)
        self.set_r(r)
        self.update_sys()
    
    def calc_ramps(self, req):
        '''  set the requested end freq and time of ramps
        @param req [2d array]: np array of ramp target freq and time settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        @return cfg [2d array]: np array of ramp cfg's for loadup
            # [n_ramps, 2] - each as [inc, length]
        '''
        n_ramps = len(req)
        req = np.array(req)
        
        lengs = self.Fpd*req.T[1]  # numb of phase detector cycles
        incs = []  # row vector of incs
        
        for idx in np.arange(n_ramps):
            des_freq = req[idx][0]
            # err = (des_end_freq/F*DEN) - acc_start
            acc_err = ((des_freq/self.Fpd)*self.den)-self.acc_starts[idx]
            this_inc = int(round(acc_err/lengs[idx]))   # pos or neg
            if this_inc < 0:
                pos_inc = self._make_increasing(this_inc)   # pos only
            else:
                pos_inc = this_inc
            acc_end = this_inc*lengs[idx] + self.acc_starts[idx]
            freq_end = self.Fpd*acc_end/self.den
            
            # store away results for next ramp
            self.acc_starts.append(acc_end)
            self.freq_starts.append(freq_end)
            # store away results for loading
            incs.append(pos_inc)
        
        cfg = np.array([incs, lengs])
        cfg = cfg.T
        if (n_ramps, 2) != np.shape(cfg):
            print("error! cfg output is the wrong shape, %d,%d" % np.shape(cfg))
            return
        return cfg
    
    def read_ramps(self, regs):
        ''' public function to load out the values of user params for ramps 
            trims away the unused rows of ramps 
        @param regs [np.array]: 2d array of values in ramp registers, verbose as
            # [8, 9] - [idx, inc, fl, dly, leng, flag, rst, n_trig, nexty]
        @return userParam [np.array]: np array of ramp target freq and time settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        '''
        regs = np.array(regs)
        
        try:
            # find last unique ramp by +1 on last nonzero nexty value
            n_ramps = np.flatnonzero(regs.T[-1])[-1] +1
        except IndexError:
            self.printer("No ramps loaded yet")
            return
        # trim the 2d array to valid ramps
        valid_regs = regs[0:n_ramps+1][:]
        return self._read_ramps(valid_regs)
    def _read_ramps(self, regs):
        ''' private function to load out the values of user params for ramps 
        @param regs [np.array]: 2d array of values in ramp registers, verbose as
            # [n_ramps, 9] - [idx, inc, fl, dly, leng, flag, rst, n_trig, nexty]
        @return userParam [np.array]: np array of ramp target freq and time settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        '''
        dur = regs.T[4]/self.Fpd  # us
        
        userParams = []
        for idx, ramper in enumerate(regs):
            this_inc = ramper[1]
            this_leng = ramper[4]
            
            # overflow protection
            inc = self._remove_increasing(this_inc)
            acc_end = inc*this_leng + self.acc_starts[idx]
            req_freq = acc_end/self.den*self.Fpd
            userParams.append([int(round(req_freq)), dur[idx]])
            
            self.acc_starts.append(acc_end)
            self.freq_starts.append(req_freq)
        
        self.ramps = userParams
        return userParams
        
    def pack_helper(self, str):
        ''' helper to grab out the 2d array of user ramp inputs
        @param str [str]: string of [[a0,b0],...[an,bn]]
        @return tbl [np.array]: np array of params 
        '''
        if not ru.check_paren(str):
            print("pack_helper, unbalanced brackets")
    
        tbl = np.array(str)
        return tbl
        
         
    def pack_ramp(self, userParam):
        ''' load in ramp registers in ultra conveinent format
        @param userParam [np.array]: np array of ramp target freq and time settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        @return cfg [list of list]: the list of ramp settings to load up, callable by driver
            # [n_ramps, 9] - [idx, inc, fl, dly, leng, flag, rst, n_trig, nexty]
        '''
        # use default extras
        nRamp, _ = np.shape(userParam)
        extras = []
        self.ramps=userParam
        for idx, _ in enumerate(userParam):
            if idx == nRamp - 1:
                nexty = 0
            else:
                nexty = idx + 1
            flg = idx + 1 % 4   # (starting ramp is non-zero)
            extras.append([0, flg, nexty])  # fl, flag, next_idx
        return self._pack_ramp(userParam, extras)
            
    def _pack_ramp(self, userParam, extraParam):
        ''' load in ramp registers in conveinent format
        @param userParam [np.array]: np array of ramp target freq and time settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        @param extraParam [np.array]: np array of the extra settings for other functions
            # [n_ramps, 3] - [fl, flag, nexty]
            @note: we fix some of the extra settings internally here, we could come
            back to this function and add in flexibility if needed 
        @return cfg [list of list]: the list of ramp settings to load up, callable by driver
            # [n_ramps, 9] - [idx, inc, fl, dly, leng, flag, rst, n_trig, nexty]
        '''
        userCfg = self.calc_ramps(userParam)
        cfg = []
        for rIdx, extras in enumerate(extraParam):
            if rIdx == 0:
                rst = 1
            else:
                rst = 0
            # build up full cfg set
            # [idx, inc, fl, dly, leng, // flag, rst, n_trig, nexty]
            this_ramp = [rIdx, userCfg[rIdx][0], extras[0], 0, userCfg[rIdx][1],
                    extras[1], rst, 0, extras[2]]
            this_ramp = [int(x) for x in this_ramp]
            cfg.append(this_ramp)
        return cfg
    
    ## --- Simple set/get methods
    def set_ref(self, f):
        self.ref = f
    def get_ref(self,):
        return self.ref
    def set_dblr(self, dblr):
        # 0 or 1
        self.dblr = dblr
    def get_dblr(self):
#         return self.dblr
        return 1
    def set_r(self, r):
        self.r = r
    def get_r(self,):
        return self.r
    def set_n(self, n):
        self.n = n
    def get_n(self,):
        return self.n
    def set_num(self, num):
        self.num = num
    def get_num(self,):
        return self.num
    def set_den(self, den):
        self.den = den
    def get_den(self):
        return self.den
    def get_ramps(self):
        ''' get back the instructed ramp settings
        @return ramps [np.array]: the ramp target settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        '''
        if self.ramps is not None:
            return self.ramps
        else:
            self.printer("ramps not yet set, using radar preset")
            return [[12250, 66], # end_freq, duration
                [12225, 6]]
            
    def _make_increasing(self,n_dec):
        ''' make a decreasing number into its increasing equiv
        @param n_dec [int]: the decreasing number (negative)
        @return n_inc [int]: the increasing number 
        '''
        n_inc = 2**30 + n_dec
        return n_inc
    def _remove_increasing(self,n_inc):
        ''' make an increasing number back to its negative equiv
        @param n_inc [int]: a number as postive version
        @return n [int]: the number as its negative intention
        ''' 
        return ru.from_twos(n_inc, 30)
    def printer(self,str):
        name = "Ramp Controller"
        print("%s: %s" % (name,str))  
    
class Lmx2492(object):

    SLP_TIME = .05
    # spi type commands
    READ = 0
    WRITE = 1
    # channel identifiers
    CMD_SPI = 0xAA
    RSP_SPI = 0xA8
    nHEADER = 4
    
    def __init__(self, file=None, sock=None, do_debug=False, do_sim=False):
        ''' create an LMX2492 controller using default hex settings
        @param file [str]: the relative file name
        @param socket [sock]: the tcp socket to write over
        @param do_debug [bool]: run with debug prints on
        @param do_sim [bool]: run with simulated hw
        '''
        self.simHW = do_sim       # when no cora connected
        
        ip = "192.168.1.10" 
        port = 7
        
        # Set up HW
        if self.simHW is True:
            self.s = None  # don't try to connect
        else:    
            # when using hardware
            if sock is None:
                # when running lmx2492.py instead of called from radar.py
                try:
                    self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.s.connect((ip, port))
                except socket.error as err:
                    print("socket exception %s" % err)
            else:
                # use inherited sock from radar.py 
                self.s = sock

        self.set_cmds()
        self._debug = do_debug
        
        # set up local memory
        self.file = file  # safe even if file=None
        self.rampy = Ramper()
        self.reset_reg() 
        self.ver = '1.4.0'   # 2/28/20

           
    ## --   Commands ------------------
    
    def reset_reg(self):
        ''' reload the starting register config from default .txt'''
        self.load_mem(self.file)
        if self.simHW is False:
            self.write_mem(), time.sleep(self.SLP_TIME)        # enable startup load
            self.reset_io(), time.sleep(self.SLP_TIME)
            self.set_n(122), time.sleep(self.SLP_TIME)
            self.set_other_dividers(0, 0, 1, 0)
            self.set_frac_num(4194304), time.sleep(self.SLP_TIME)
            self.set_frac_den(16777215), time.sleep(self.SLP_TIME)
            self.set_ramp_limit(201326592, 117440512), time.sleep(self.SLP_TIME)
            self.set_pll_r(1), time.sleep(self.SLP_TIME)
            
            self.update_rampy()
    
    def get_power(self):
        addr = 2
        n = 1
        rd_addr, rd_values = self.spi_read(addr, n)
        return rd_addr, rd_values
        
    def set_power(self,state):
        ''' power up/down the chip
        @param state [int]: 0 for off, 1 for on
        '''
        if state != 0 and state != 1:
            print("set_charge_pump erorr, pol must be 0 or 1")
            return
        
        addr = 2
        new_reg = state
        self.spi_write(addr, new_reg)
    
    def get_ver(self):        
        ''' get the sw version '''
        print("LMX Driver Version: %s" % self.ver)
        
    
    ## PLL METHODS
    
    def get_reg0(self):
        addr = 0
        n = 1
        rd_addr, rd_values = self.spi_read(addr, n)
        return rd_addr, int(rd_values)
    
    def get_n(self):
        addr = 18
        num = 3
        rd_addr, rd_values = self.spi_read(addr, num)
        r18, r17, r16 = rd_values
        n = ((r18 & 0x3) << 16) + (r17 << 8) + r16
        return rd_addr, n

    def set_n(self,n):
        ''' set the N value in the PLL loop
        @param n [int]: the number 
        '''
        addr = 18  # the highest reg
        n_bits = ru.to_bits(n,18)


        rd_addr, rd_value = self.spi_read(18, 1)
        r18_int = rd_value
        
        r18_int = r18_int | (n >> 16)
        r18_data = r18_int
        r17_data = (n >> 8) & 0xff
        r16_data = n & 0xff
        
        new_reg = [r18_data, r17_data, r16_data]
        self.spi_write(addr, new_reg)
    def test_n(self):
        ''' perform sanity check that n is what it should be '''
        true_n = 122
        test_addr, test_n = self.get_n()
        assert test_addr==18, self.printer("lmx sanity failure, exiting")
        assert true_n==test_n, self.printer("lmx sanity failure, exiting")
        
    def get_frac_num(self):
        addr = 21
        num = 3
        rd_addr, rd_values = self.spi_read(addr, num)
        r21, r20, r19 = rd_values
        frac_num = (r21 << 16) + (r20 << 8) + r19
        return rd_addr, frac_num
    
    def set_frac_num(self,frac_num):
        ''' set the fractional numerator
        @param frac_num [int]: the fractional numerator
        '''
        addr = 21     # 21 is the highest reg
        
        r21_data = frac_num >> 16 & 0xff
        r20_data = frac_num >> 8 & 0xff
        r19_data = frac_num & 0xff      
        new_reg = [r21_data, r20_data, r19_data]
        self.spi_write(addr, new_reg)
        
    def get_frac_den(self):
        addr = 24
        num = 3
        rd_addr, rd_values =  self.spi_read(addr, num)
        r24, r23, r22 = rd_values
        frac_den = (r24 << 16) + (r23 << 8) + r22
        return rd_addr, frac_den
    
    def set_frac_den(self,frac_den):
        ''' set the fractional denominator
        @param frac_den [int]: the fractional denominator
        '''
        fden_bits = ru.to_bits(frac_den, 24)
        addr = 24     # 24 is the highest reg
        
        r24 = ru.to_int(fden_bits[16:24])
        r23 = ru.to_int(fden_bits[8:16])
        r22 = ru.to_int(fden_bits[0:8])
        new_reg = [r24, r23, r22]
        
        self.spi_write(addr, new_reg)
    
    def get_pll_r(self):
        addr = 26
        num = 2
        rd_addr, rd_values = self.spi_read(addr, num)
        r = (rd_values[0] << 8) + rd_values[1]
        return rd_addr, r
    
    def set_pll_r(self,r):
        ''' set the R divider 
        @param r [int]: the r divider; 16bits, on [0,65535]
        '''
        if r > 65535 or r<0:
            print("set_r error, r must be on [0,65535], given %d" % r)
            return
        addr = 26  # 26 is the highest reg
        
        r26 = (r >> 8) & 0xff
        r25 = r & 0xff
        new_reg = [r26, r25]
        self.spi_write(addr, new_reg)
    
    def get_charge_pump(self):
        addr = 28
        num = 1
        rd_addr, rd_values = self.spi_read(addr, num)
        pol = rd_values >> 5 & 0x01
        pump = rd_values & 0x1f
        return rd_addr, pump, pol
    def set_charge_pump(self,pump, pol):
        ''' set the charge pump gain setting and polarity
        @param pump [int]: 0-31 setting value for charge pump gain
        @param pol [int]: 0 for negative, 1 for positive polarity
        '''
        addr = 28 # reg 28
        reg = 0
        if pump >31 or pump<0:
            print("set_charge_pump error, pump must be on [0,31]")
            return
        if pol != 0 and pol != 1:
            print("set_charge_pump erorr, pol must be 0 or 1")
            return
        # include commanded polarity
        reg |= 0x0020 if pol==1 else 0
        # include pump value
        reg |= pump
        
        new_reg = reg
        # write command
        self.spi_write(addr, new_reg)

    def get_cp_thresh(self):
        addr = 31
        num = 2
        rd_addr, rd_values = self.spi_read(addr, num)
        
        high = rd_values[0] & 0x3f
        high_fl = (rd_values[0] >> 6) & 0x01
        low = rd_values[1] & 0x3f
        low_fl = (rd_values[1] >> 6) & 0x01
        return rd_addr, high, low, high_fl, low_fl
        
    def set_cp_thresh(self, high, low,):
        ''' set the comparison thresholds for the charge pump
        @param high [int]: lock detect triggers above this value, on [0,63]
        @param low [int]: lock detect triggers below this value, on [0,63]
            @note bit 6 on each reg is read_tbState only, the flag for triggering
        '''
        addr = 31   # highest of 2
        if low > 63 or low < 0:
            print("set_cp_thresh error, low value must be on [0,63]")
            return
        if high > 63 or high < 0:
            print("set_cp_thresh error, high value must be on [0,63]")
            return
            
        r31 = high
        r30 = low
        new_reg = [r31, r30]
        
        # write command
        self.spi_write(addr, new_reg)
       
    def get_other_dividers(self): 
        addr = 27
        num = 1
        rd_addr, rd_values = self.spi_read(addr, num)
        
        doubler = rd_values & 0x01
        diff_r = (rd_values & 0x04) >> 2
        delay = (rd_values >> 3) & 0x03
        csr = (rd_values >> 5) & 0x03
        return doubler, diff_r, delay, csr
    def print_other_dividers(self, config):
        str = "Other Divider Settings:\n"
        labs = ['DOUBLER', 'DIFF_R', 'DELAY', 'CSR']
        for id, ele in enumerate(config):
            str += labs[id] + ': %d\t' % ele
        self.printer(str)
    def set_other_dividers(self, doubler, diff_r, pfd_delay=1, csr=0):
        ''' set the other random divider settings'
        @param doubler [int]: 1bit, doubles the ref oscillator, on [0,1]
        @param diff_r [int]: 1bit, enable differential r counter, on [0,1]
        @param pfd_delay [int]: 2bit, number for pump min width, default 1, on [0,3]
        @param csr [int]: 2bit, cycle slip reduction, off normally for ramping, on [0,3]
        '''
        if doubler < 0 or doubler > 1:
            print("set_other_dividers error, doubler out of range [0,1], given %d" % doubler)
            return
        if diff_r < 0 or diff_r > 1:
            print("set_other_dividers error, diff_r out of range [0,1], given %d" % diff_r)
            return
        if pfd_delay <0 or pfd_delay > 3:
            print("set_other_dividers error, delay out of range [0,3], given %d" % pfd_delay)
            return
        if csr < 0 or csr > 3:
            print("set_other_dividers error, csr out of range [0,3], given %d" % csr)
            return
        
        addr = 27
        reg = 0
        if doubler==1:
            reg |= 0x01
        if diff_r == 1:
            reg |= 0x04
        reg |= pfd_delay << 3
        reg |= csr << 5
            
        new_reg = reg
        # write command
        self.spi_write(addr, new_reg)
        
    def get_fastlock_timer(self):
        addr = 32
        num = 1
        rd_addr1, r32 = self.spi_read(addr, num)
        addr2 = 29
        rd_addr2, r29 = self.spi_read(addr2, num)
        
        fl_cpg = r29 & 0x1f
        fl_toc = ((r29 & 0xE0) << 3) + r32
        return fl_cpg, fl_toc
    def print_fastlock_timer(self):
        str = "FastLock Settings:\n"
        labs = ['CPG', 'TOC']
        self._printback(str, labs, self.get_fastlock_timer())
    def set_fastlock_timer(self, fl_cpg, fl_toc):
        ''' set the fast lock settings
        @param fl_cpg [int]: 5 bits, charge pump gain in fl mode, on [0,31]
        @param fl_toc [int]: 11 bits, timer countdown, on [0,2047]
        '''
        if fl_cpg < 0 or fl_cpg > 31:
            print("set_fastlock_timer error, fl_cpg out of range [0,31], given %d" % fl_cpg)
            return
        if fl_toc < 0 or fl_toc > 2047:
            print("set_fastlock_timer error, fl_toc out of range [0,2047], given %d" % fl_toc)
            return
        
        addr1 = 32
        addr2 = 29

        r32 = fl_toc & 0xff
        r29 = ((fl_toc >> 3) & 0xE0) + fl_cpg
        
        # write command
        self.spi_write(addr1, r32)
        self.spi_write(addr2, r29)
    
    def get_dld(self):
        addr = 34
        num = 2
        rd_addr, rd_values = self.spi_read(addr, num)
        tol = rd_values[0] >> 5 & 0x7
        err = rd_values[0] & 0x1f
        cnt = rd_values[1]
        return cnt, err, tol
    def print_dld(self, config):
        str = "DLD Error Settings:\n"
        labs = ['CNT', 'ERR', 'TOL']
        self._printback(str, labs, self.get_dld())
    def set_dld(self, cnt, err, tol):
        ''' set the number of phase detect compare before digital lock
        @param cnt [int]: number of passes before lock, 8bit, on [0,128]
        @param err [int]: number of passes missed before considerred unlocked, 5bit, on [0,31]
        @param tol [int]: max phase error before consider not-locked, 3bit, on [0,5]
        '''
        if cnt < 0 or cnt > 128:
            print("set_dld_count error, cnt out of range [0,128], given %d" % cnt)
            return
        if err < 0 or err > 31:
            print("set_dld_error error, ERR out of range [0,31], given %d" % err)
            return
        if tol < 0 or tol > 5:
            print("set_dld_error error, TOL out of range [0,5], given %d" % tol)
            return
        
        addr = 34
        val = []
        val.append((tol << 5) + err)
        val.append(cnt)
        self.spi_write(addr, val)

    def get_io(self, _addr):
        r35_addr, r35_value = self.spi_read(35, 1)
        
        if _addr==0:
            b5 = (r35_value >> 3) & 0x1
        elif _addr==1:
            b5 = (r35_value >> 4) & 0x1
        elif _addr==2:
            b5 = (r35_value >> 5) & 0x1
        else:
            b5 = (r35_value >> 7) & 0x1    

        targ = 36 + _addr
        # then read_tbState target reg
        rd_addr, rd_value = self.spi_read(targ, 1)
        
        the_addr = rd_addr - 36
        the_type = rd_value & 0x7
        the_mode = (b5 << 5) + (rd_value >> 3)
        return the_addr, the_type, the_mode

    def print_io(self, _addr=None):
        if _addr is not None:
            return self._print_io(_addr)
        else:
            io_list = []
            for idx in np.arange(4):
                io_list.append(self._print_io(idx))
            return io_list
        
    def _print_io(self, _addr):    
        mux_config = self.get_io(_addr)
        ch = ['TRIG1', 'TRIG2', 'MOD', 'MUX']
        io_str = "Mux Setting on %s:\n" % ch[_addr]
        labs = ['TYPE', 'MODE']
        for el_id, ele in enumerate(mux_config[1:]):
            io_str += labs[el_id] + ': %d\t' % ele
        self.printer(io_str)
        return mux_config
        
    def reset_io(self):
        ''' set the io back to preferred settings '''
        self.set_io(0, 2, 30)       # trig1 = flag 0        24F2
        time.sleep(.1)
        self.set_io(1, 2, 31)       # trig2 = flag 1        25FA
        time.sleep(.1)
        self.set_io(2, 2, 7)        # mod = readback        263A
        time.sleep(.1)
        self.set_io(3, 2, 10)       # muxout = lock detect  2751
        time.sleep(.1)
        self.printer('finished reseting io')
    def set_io(self, _addr, _type, _mode):
        ''' settings for one of the mux pins
        @param _addr [int]: the address id of the pins, on [0, 3] => addr[36, 39]
            0 - trig1, 1 - trig2, 2 - mod, 3 - mux
        @param _type [int]: i/o type, 3 bits, on b0:2, on [0,7]
            see ti datasheet page 22, table 6
            0 - TRI, 1 - OD Output, 2 - Pullup Output, 3 - Reserved
            4 - GND, 5 - Invert OD Output, 6 - Invert Pullup Output, 7 - Input
        @param _mode [int]: destination mode, 5 bits, on b3:7
            se ti datasheet p23, table 7
            0 - GND, 
            
        ::: set mod pin :::
        lmx set io 2 2 7
        
        ::: set muxout to n/4 :::
        lmx set io 3 2 19
        
        '''
        if _addr < 0 or _addr > 3:
            print("_set_mux error, address out of range [0,3], given %d" % _addr)
            return
        if _type < 0 or _type > 7:
            print("_set_mux error, type out of range [0,7], given %d" % _type)
            return
        if _mode < 0 or _mode > 38:
            print("set_io error, mode out of range [0,38], given %d" % _mode)
            return
        
        # readback the 5th mode bit for all 4 ios
        r35 = self.mem[35]

        
        # convert to bits the mode param
        mode_b5 = (_mode >> 5) & 0x1
        
        # set the appropriate 5th bit on reg35
        if _addr == 0:
            # trig1 case
            mask = mode_b5 << 3
        elif _addr == 1:
            # trig2 case
            mask = mode_b5 << 4
        elif _addr == 2:
            # mod case
            mask = mode_b5 << 5
        elif _addr == 3:
            mask = mode_b5 << 7
        r35 |= mask
        self.spi_write(35, int(r35))
        
        # then write the dedicated register for this IO
        addr = 36 + _addr
        reg = (_mode << 3) + _type
        self.spi_write(addr, int(reg))
    
    ## --- HIGH LEVEL RAMPING -----------
    def update_rampy(self):
        ''' update the local PLL model with values in LMX '''
        _, n = self.get_n()
        time.sleep(self.SLP_TIME)
        _, num = self.get_frac_num()
        time.sleep(self.SLP_TIME)
        _, den = self.get_frac_den()
        try:
            n = n + num/den
        except:
            # @todo: bad hack
            n = 122.25
            
        time.sleep(self.SLP_TIME)
        _, r = self.get_pll_r()
        time.sleep(self.SLP_TIME)
        dblr, _, _, _ = self.get_other_dividers()
        time.sleep(self.SLP_TIME)
        # make sure no old ramp values persist
#         self.clear_ramps()
#         time.sleep(.05)
        self.rampy.load_sys(n, num, den, dblr, r)
        
    def set_ramping(self, userParam):
        ''' highest level ramp setup function 
        @param userParam [np.array]: np array of ramp target freq and time settings
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        '''
        self.update_rampy(), time.sleep(self.SLP_TIME)
        self.ramp_off()
        cfg = self.rampy.pack_ramp(userParam)
        # cfg as [n_ramps, 9] - [idx, inc, fl, dly, leng, flag, rst, n_trig, nexty]
        for rampCfg in cfg:
            rIdx = rampCfg[0]
            self.printer("loading ramp %d" % rIdx)
            self.set_ramp_reg(*rampCfg) 
            time.sleep(self.SLP_TIME)
#         time.sleep(.05), self.ramp_on()

    def set_trap(self):
        """ Pack a trapezoid wave saved as a default """
        user = [[12300, 200], # end_freq (MHz), duration (us)
                [12300, 200],
                [12250, 200],
                [12250, 200]]
        self.set_ramping(user)
    def set_triangle(self):
        """ Pack a triangle wave  """
        user = [[12325, 100], # end_freq, duration
                [12225, 100]]
        return self.set_ramping(user)
    def set_simple(self):
        """ Pack a simple 1-leg wave  """
        return self.set_ramp_reg(0, 100, 0, 0, 10000, 1, 1, 0, 0)
    def set_radar(self):
        """ Pack a radar wave  """
        user = [[12250, 33], # end_freq, duration/2
                [12225, 3]]
        return self.set_ramping(user)
    def set_precise(self):
        """ Pack a precision ramp waveform """
        user = [[12325, 100], # end_freq, duration/2
                [12225, 5]]
        return self.set_ramping(user)
    def set_fine(self):
        user = [[12425, 100],
                [12225, 5]]
        return self.set_ramping(user)
    def test_ramp_precise(self,as_reset=False):
        time.sleep(.01)
        true_output = [[12325, 100], # end_freq, duration/2
                    [12225, 5]]
        if as_reset:
            # set sys to 12250
            self.set_n(122), time.sleep(self.SLP_TIME)
            self.set_frac_num(4194304), time.sleep(self.SLP_TIME)
            self.set_frac_den(16777215), time.sleep(self.SLP_TIME)
            self.update_rampy(), time.sleep(self.SLP_TIME)
            self.set_precise(), time.sleep(self.SLP_TIME)
        test_output = self.get_user_ramps()
        
        if true_output != test_output:
            print("\tfailed ramp_precise case")
            print("\ttrue: %s" % (str(true_output)))
            print("\ttest: %s" % (str(test_output)))
            return
        print('passed set_precise')    
    
    def get_user_ramps(self,):
        ''' get the target [freq, dur] in current ramp reg memory '''
        reg_list = self._get_all_ramps()
        usrParam = self.rampy.read_ramps(reg_list)
        return usrParam      
    def print_user_ramps(self):
        labs = '[EndFreq (MHz), LEN (us)]'
        vals = self.get_user_ramps()
        self.printer("Ramp User Values:\n%s\n%s" % (labs, vals))
    
    def clear_ramps(self):
        ''' clear all ramp regs'''
        for idx in np.arange(8):
            self.clear_ramp(idx)
    def clear_ramp(self,idx):
        ''' clear out the values of a given ramp '''
        self.debug("clearing ramp %d" % idx)
        self.set_ramp_reg(idx, 0, 0, 0, 0, 0, 0, 0, 0)
    def ramp_on(self):
        self.set_ramp_power(1)
    def ramp_off(self):
        self.set_ramp_power(0)
    ## ------ RAMP SETTINGS -------        
    def get_ramp_power(self):
        r58_addr, r58_value = self.spi_read(58, 1)
        pwr = r58_value & 0x01
        return pwr
    def print_ramp_power(self):
        pwr = self.get_ramp_power()
        return self.printer("Ramp Power is %d" % pwr)
    def set_ramp_power(self,pwr):
        """ set the ramp power 
        @param pwr [int]: 1bit, on [0,1], turn ramping on or off
        """
        if pwr < 0 or pwr > 1:
            print("ramp_power, power out of range [0,1], given %d" % pwr)
            return
        
        addr = 58
        val = 0
        # read_tbState present state for high bits
        r58_addr, r58_value = self.spi_read(addr, 1)
        time.sleep(.05)
        val = (r58_value & 0xfe) + pwr
        self.spi_write(addr, val)
    
    def get_ramp_general(self):
        addr1 = 58
        n1 = 1
        addr2 = 84
        n2 = 2
        
        r58_addr, r58_value = self.spi_read(addr1, n1)
        pm = (r58_value >> 2) & 0x01
        clk = (r58_value >> 1) & 0x01
        time.sleep(.1)
        r2_addr, r2_values = self.spi_read(addr2, n2)
        cnt = ((r2_values[0] & 0x1f) << 8) + r2_values[1]
        auto = (r2_values[0] >> 5) & 0x01
        inc_src = r2_values[0] >> 6
        
        return clk, pm, cnt, auto, inc_src
    
    def print_ramp_general(self):
        str = 'Ramp General Settings:\n'
        labs = ['CLK', 'PM', 'CNT', 'AUTO', 'INC_SRC']
        return self._printback(str, labs, self.get_ramp_general())
    def set_ramp_general(self,clk, pm, cnt, auto, inc_src):
        """ set ramp general settings
        @param clk [int]: <58.D1> 1bit, on [0,1], 1 to use MOD input for clock, default 0
        @param pm [int]: <58.D2> 1bit on [0,1], 1 to do phase mod, default off
        @param cnt [int]: <84.D4:0, 83.D8:0> 12bit, on [0,4095], _____
        @param auto [int]: <84.D5> 1bit, on [0,1], 1 autoclear shutoff ramp after counter
        @param inc_src [int]: <84.D7:6> 2bit on [0,3], source for ramp counter increment
        """
        if clk < 0 or clk > 1:
            print("ramp_general, clk out of range [0,1], given %d" % clk)
            return
        if pm < 0 or pm > 1:
            print("ramp_general, pm out of range [0,1], given %d" % pm)
            return
        if cnt < 0 or cnt > 4095:
            print("ramp_general, cnt out of range [0,4095], given %d" % cnt)
            return
        if auto < 0 or auto > 1:
            print("ramp_general, auto out of range [0,1], given %d" % auto)
            return
        if inc_src < 0 or inc_src > 3:
            print("ramp_general, inc_src out of range [0,3], given %d" % inc_src)
            return
        addr1 = 58
        # handle r58, r84, r83
        # read_tbState present state for high bits
        r58_addr, r58_value = self.spi_read(addr1, 1)
        val = (r58_value & 0xf1) + (pm << 2) + (clk << 1)
        self.spi_write(addr1, val)
        time.sleep(.1)
        addr2 = 84
        r84 = (inc_src << 6) + (auto << 5) + (cnt >> 8)
        r83 = cnt & 0xff
        val = [r84, r83]
        self.spi_write(addr2, val)
    
    def get_ramp_limit(self):
        addr = 82
        nRead = 8
        
        _, r70 = self.spi_read(70, 1)
        time.sleep(.1)
        h32 = (r70 >> 4) & 0x01
        l32 = (r70 >> 3) & 0x01
        
        rd_addr, rd_values = self.spi_read(addr, nRead)
        high = (h32 << 32) + (rd_values[0] << 24) + (rd_values[1] << 16) \
             + (rd_values[2] << 8) + rd_values[3]
        low = (l32 << 32) + (rd_values[4] << 24) + (rd_values[5] << 16) \
             + (rd_values[6] << 8) + rd_values[7]
        return high,low
    
    def print_ramp_limit(self):
        str = "Ramp Limit Settings:\n"
        labs = ['High', 'Low']
        return self._printback(str, labs, self.get_ramp_limit())
    def set_ramp_limit(self,high, low):
        """ set the freq limit for the ramping
        @param high [int]: <70D5, 82D8:0 - 79D8:0> 32 bits, on [0,4.29B] in MHz, the high freq limit
        @param low [int]: <70D4, 78D8:0 - 75D8:0> 32 bits, on [0,4.29B] in MHz, the low freq limit
        """
        if high < 0 or high > 4.294967295e9:
            print("ramp_limit, high out of range [0,4.29B], given %e" % high)
            return
        if low < 0 or low > 4.294967295e9:
            print("ramp_limit, low out of range [0,4.29B], given %e" % low)
            return
        
        _, r70 = self.spi_read(70, 1)
        h32 = high >> 32 
        l32 = low >> 32
        r70 &= h32 << 4
        r70 &= l32 << 3
        self.spi_write(70, r70)
        time.sleep(.1)
        
        addr = 82
        r82 = high >> 24
        r81 = (high >> 16) & 0xff
        r80 = (high >> 8) & 0xff
        r79 = high & 0xff
        
        r78 = low >> 24
        r77 = (low >> 16) & 0xff
        r76 = (low >> 8) & 0xff
        r75 = low & 0xff
        val = [r82, r81, r80, r79, r78, r77, r76, r75]
        self.spi_write(addr, val)
    
    def get_ramp_comp(self):
        addr = 70
        nRead = 11
        _, vals = self.spi_read(addr,nRead)
        
        c0_32 = vals[0] & 0x01
        c1_32 = (vals[0] >> 1) & 0x01
        
        comp1_en = vals[1]
        comp1 = (c1_32 << 32) + (vals[2] << 24) + (vals[3] << 16) \
             + (vals[4] << 8) + vals[5]
        comp0_en = vals[6]
        comp0 = (c0_32 << 32) + (vals[7] << 24) + (vals[8] << 16) \
             + (vals[9] << 8) + vals[10]
        return comp0, comp0_en, comp1, comp1_en
    def print_ramp_comp(self):
        str = "Ramp Comparator Settings:\n"
        labs = ['Comp0', 'Comp0_EN', 'Comp1', 'Comp1_EN']
        return self._printback(str, labs, self.get_ramp_comp())
    def set_ramp_comp(self,comp0, comp0_en, comp1, comp1_en):
        """ set the comparator value, and which comparator is enabled to which leg
        @param comp0 [int]: <70D0, 63-60D7:0> 32 bits, on [0,4.29B] in MHz, the 0th comparason freq value 
        @param comp0_en [int]: <64D7:0> 7 bits, on [0,255], comp0 active during enabled bits
        @param comp1 [int]: <70D1, 68-65D7:0> 32 bits, on [0,4.29B] in MHz, the 1st comparason freq value 
        @param comp1_en [int]: <69D7:0> 7 bits, on [0,255], comp1 active during enabled bits
        """
        if comp0 < 0 or comp0 > 4.294967295e9:
            print("ramp_comp, comp0 out of range [0,4.29B], given %e" % comp0)
            return
        if comp1 < 0 or comp1 > 4.294967295e9:
            print("ramp_comp, comp1 out of range [0,4.29B], given %e" % comp1)
            return
        if comp0_en < 0 or comp0_en > 255:
            print("ramp_comp, comp0_en out of range [0,255], given %d" % comp0_en)
            return
        if comp1_en < 0 or comp1_en > 255:
            print("ramp_comp, comp1_en out of range [0,4.29B], given %d" % comp1_en)
            return
        
        _, r70 = self.spi_read(70, 1)
        time.sleep(.1)
        c0 = comp0 >> 32 
        c1 = comp1 >> 32
        r70 &= c0 
        r70 &= c1 << 1
        
        addr = 70
        vals = []
        vals.append(r70)
        vals.append(comp1_en)
        vals.append(comp1 >> 24)
        vals.append((comp1 >> 16) & 0xff)
        vals.append((comp1 >> 8) & 0xff)
        vals.append(comp1 & 0xff)
        vals.append(comp0_en)
        vals.append(comp0 >> 24)
        vals.append((comp0 >> 16) & 0xff)
        vals.append((comp0 >> 8) & 0xff)
        vals.append(comp0 & 0xff)
        self.spi_write(addr, vals)
    
    def get_ramp_trig(self,):
        addr = 59
        nRead = 2
        _, vals = self.spi_read(addr,nRead)
        b = vals[0] & 0x0f
        c = vals[0] >> 4
        a = vals[1] >> 4
        return a,b,c
    def print_ramp_trig(self):
        str = "Ramp Trigger Settings:\n"
        labs = ['A', 'B', 'C']
        return self._printback(str, labs, self.get_ramp_trig()) 
    def set_ramp_trig(self, a,b,c):
        ''' set the ramp trigger sources 
        @param a [int]: <58D7:4>, 4bit, on [0,15] the type for trigger A  
        @param b [int]: <59D3:0>, 4bit, on [0,15] the type for trigger B  
        @param c [int]: <59D7:4>, 4bit, on [0,15] the type for trigger C   
        '''
        if a < 0 or a > 15:
            print("ramp_trig, A out of range [0,15], given %d" % a)
            return
        if b < 0 or b > 15:
            print("ramp_trig, B out of range [0,15], given %d" % b)
            return
        if c < 0 or c > 15:
            print("ramp_trig, C out of range [0,15], given %d" % c)
            return
        
        # read_tbState back current setting
        _, r58 = self.spi_read(58, 1)
        r58 = (a << 4) + (r58 & 0x0f)
        r59 = (c << 4) + b
        self.spi_write(59, [r59, r58])
        
    ## ----- LOW LEVEL RAMPING 
    def _get_all_ramps(self,):
        # NOTE: lengthy operation
        reg_list = []
        for idx in np.arange(8):
            reg_list.append(self.get_ramp_reg(idx))
        return reg_list
    
    def get_ramp_reg(self,idx):
        ''' verbose return of ramp register '''
        base_addr = 141
        addr = 141 - 7*(7 - idx)
        nRead = 7
        _, vals = self.spi_read(addr,nRead)
        
        nexty = vals[0] >> 5
        n_trig = (vals[0] >> 3) & 0x03
        rst = (vals[0] >> 2) & 0x01
        flag = vals[0] & 0x03   
        leng = (vals[1] << 8) + vals[2]
        dly = (vals[3] >> 7) & 0x01
        fl = (vals[3] >> 6) & 0x01
        inc = ((vals[3] & 0x3f) << 24) + (vals[4] << 16) + (vals[5] << 8) + vals[6]
        return idx, inc, fl, dly, leng, flag, rst, n_trig, nexty
    def _print_ramp_reg(self, idx):
        str = "Ramp %d Settings:\n" % idx
        labs = ['INC', 'FL', 'DELAY', 'LEN', 'FLAG','RESET','NXT_TRIG', 'NXT_RMP']
        return self._printback(str, labs, self.get_ramp_reg(idx)[1:])
    def print_ramp_reg(self, _addr=None):
        ''' print given ramp idx, else print all '''
        if _addr is not None:
            return self._print_ramp_reg(_addr)
        else:
            reg_list = []
            for idx in np.arange(8):
                reg_list.append(self._print_ramp_reg(idx))
            return reg_list       
    def set_ramp_reg(self, idx, inc, fl, dly, leng, flag, rst, n_trig, nexty):
        ''' verbose load in the register
        @param idx [int]: ramp id to load correct register maps
        @param inc [int]: (increasing) signed number of freq steps in ramp, 30bit, on [0,5.3k‬]
        @param fl [bool]: conditional for doing fast lock; 1bit, on [0,1]
        @param dly [bool]: condidtional for having a delay; 1bit, on [0,1]
        @param leng [int]: number of fpd cycles for duration; on; 16bits, on [0,65535]
        @param flag [int]: mux flag to trip on events; on [0,3]
        @param rst [bool]: flag for forcing clear of accumulator; 1bit, on [0,1]
        @param n_trig [int]: starting event for this ramp; 2bit, on [0,3]
        @param nexty [int]: the next ramp to follow this one; 3bit, on [0,7]
        '''
        
        # register to write, value,     hex_reg[start bit], length
    #     _rampX_inc = 0                  # 0x56[0], 29
    #     _rampX_fl = 0                   # 0x59[6], 1
    #     _rampX_dly = 0                  # 0x59[7], 1
    #     _rampX_len = 0                  # 0x5A[0], 16
    #     _rampX_flag = 0                 # 0x5C[0], 2
    #     _rampX_rst = 0                  # 0x5C[2], 1
    #     _rampX_next_trig = 0            # 0x5C[3], 2
    #     _rampX_next = 0                 # 0x5C[5], 3
        
        if idx <0 or idx > 7:
            print("set_rampX error, idx out of range [0,7], given %d" % idx)
            return
        # protect 2's complement range
        limH = (1 << 30) -1
        if inc < 0 or inc > limH:
            print("set_rampX error, inc out of range [0,%d], given %d" % (limH, inc))
            return
        if fl < 0 or fl > 1:
            print("set_rampX error, fl out of range [0,1], given %d" % fl)
            return
        if dly <0 or dly > 1:
            print("set_rampX error, delay out of range [0,1], given %d" % dly)
            return
        if leng < 0 or leng > 65535:
            print("set_rampX error, len out of range [0,65535], given %d" % len)
            return
        if flag < 0 or flag > 3:
            print("set_rampX error, flag out of range [0,3], given %d" % flag)
            return
        if rst <0 or rst > 1:
            print("set_rampX error, rst out of range [0,1], given %d" % rst)
            return
        if n_trig < 0 or n_trig > 3:
            print("set_rampX error, next_trig out of range [0,3], given %d" % n_trig)
            return
        if nexty < 0 or nexty > 7:
            print("set_rampX error, nexty out of range [0,7], given %d" % nexty)
            return    
      
        base_addr = 141
        # control which registers are written to, ramps are separated by 7 reg
        addr = base_addr - 7*(7 - idx)
        
        # convert types
        leng = int(leng)
        inc = int(inc)
        
        # debug print the flag
        if flag==0:
            flags = "flag0=0  flag1=0"
        elif flag==1:
            flags = "flag0=1  flag1=0"
        elif flag==2:
            flags = "flag0=0  flag1=1"
        elif flag==3:
            flags = "flag0=1  flag1=1"
        self.debug("ramp %d, %s" %(idx, flags))
        
        reg = []
        reg.append((nexty << 5) + (n_trig << 3) + (rst << 2) + flag)
        reg.append(leng >> 8)
        reg.append(leng & 0xff)
        reg.append((dly << 7) + (fl << 6) + (inc >> 24))
        reg.append((inc >> 16) & 0xff)
        reg.append((inc >> 8) & 0xff)
        reg.append(inc & 0xff)
        self.spi_write(addr, reg)
        
        
    ## --   Low Level ----------------
    def _pack_write(self,addr, values_list):
        """ pack in a spiData_t for SPI command
        @param addr [u8]: the highest register value 
        @param value_list [list or u8]: list of int's, the values to set in reg's
             or 
               value_list [u8]: a single int to set in the register
        @return packet_binary [spiData_t]: a binary packed bytes object to tx
        """
        word1 = Lmx2492.CMD_SPI     # lmx address
        word2 = Lmx2492.WRITE   # write command
        word3 = int(addr)           # high reg address
        packet_header = [word1, word2, word3,]
        
        if type(values_list) == list:
            word4 = int(len(values_list))   # num of bytes
            packet_header.append(word4)    
            packet_data = values_list       # bytes_list (in rev order)    
            packet_binary = struct.pack(('>4B %dB' % word4), *packet_header,
                                         *packet_data)
        else:
            packet_header.append(1)         # num of bytes
            packet_data = values_list
            packet_binary = struct.pack('>4BB', *packet_header,packet_data)
  
        return packet_binary
        
    def _pack_read(self, addr, length):
        """ pack in a spiData_t for SPI command
        @param addr [u8]: the highest register value 
        @param length [u8]: the number of reg to read_tbState back
        @return packet_binary [spiData_t]: a binary packed bytes object to tx
        """
        
        word1 = Lmx2492.CMD_SPI     # lmx address
        word2 = Lmx2492.READ        # read_tbState command
        word3 = int(addr)           # high reg address
        word4 = length              # num of bytes
        
        packet_header = [word1, word2, word3, word4]
        packet_binary = struct.pack('>4B', *packet_header)
        return packet_binary    
    
    def _unpack_read(self,package):
        """ unwrap the spiResponse_t package from a read_tbState
        @param package [spiResponse_t]: the packed struct to unwrap
        @return addr [u8]: the max register address 
        @return values_list [u8, or list of u8]: the value of registers read_tbState
        """
        m = Lmx2492.nHEADER
        read_ack, addr, length = struct.unpack(('>3B'),package[1:m])
        values_list = struct.unpack('>%dB' % length, package[m:])
        if len(values_list)==1:
            values_list = values_list[0]
        return addr, values_list

    ## --   Socket Level  ----
    def _zynq_write(self,data):
        ''' write a zynq layer SPI command 
        @param data [spiData_t]: data to be written, in packed structure
        '''
        self.debug("write cmd %s" % data)
        if self.simHW is False:
            self.s.sendall(data)
        
    def _zynq_read(self,num,data):
        ''' send a zynq layer SPI read command
        @param num [int]: the number of bytes to readback
        @param data [spiData_t]: data to be written, for a read, in packed structure
        @return package [spiResponse_t]: readback data
        '''
        self.debug("read_spiData_t cmd %s" % data)
        if self.simHW is False:
            self.s.sendall(data)
        time.sleep(.1)
        package = self.s.recv(1024).replace(b'\r\n',b'')
        package = package[:num]
        return package
        
    
    ## -- ADMIN LEVEL ------
    def spi_write(self,max_addr,values_list):
        '''  lmx SPI write command, regardless of list length
        @param start_addr [int]: the starting register address
        @param value_list [list]: list of the register values to set, in descending order
        '''
        if type(values_list)==int:
            # 1 byte case
            rData = self._pack_write(max_addr, values_list)
            self._zynq_write(rData)
            
            # store to local mem
            self.mem[max_addr] = values_list
        else:
            # loop case, 
            if len(values_list) > 3:            
                iterNum = int(np.floor((len(values_list))/3))
                rem = len(values_list) % 3
                for idx in np.arange(iterNum):
                    self.debug('wrote regs %d through %d' % (max_addr-3*idx,
                                                               max_addr-3*idx-(3-1)))
                    items = values_list[3*idx:3*idx+3]
                    rData = self._pack_write(max_addr - 3*idx, items)
                    self._zynq_write(rData)
                    time.sleep(.01)
                # last loop might be length 3 or less
                idx = iterNum 
                self.debug('wrote regs %d through %d' % (max_addr-3*idx,
                                               max_addr-3*idx-(rem-1)))
                items = values_list[3*idx:3*idx+rem]
                rData = self._pack_write(max_addr - 3*idx, items)
                self._zynq_write(rData)
            else:
                # send 3-byte or less command
                rData = self._pack_write(max_addr, values_list)    
                self._zynq_write(rData)
            
            # store to local mem
            for idx, ele in enumerate(values_list):
                if idx < 85 and idx > 0:
                    self.mem[max_addr-idx] = ele
                elif idx >= 85:
                    self.mem[max_addr - (11 + idx)] = ele
        


    
    def spi_read(self, max_addr, length):
        ''' read a zynq layer SPI command
        @param max_addr [int]: the max register address
        @param len [int]: number of registers to read_tbState
        @return r_addr [int]: the read_tbState address
        @return r_vList [int or list]: the readback register values
        '''
        # note the fw spi max read_tbState length is 3
        if length>3:            
            values_list = []
            iterNum = np.int(np.floor(length/3))
            # NOTE: works when i put +1 on test1
            rem = length % 3
            for idx in np.arange(iterNum):
                rData = self._pack_read(max_addr - 3*idx, 3)
                n_rcv = Lmx2492.nHEADER + 3
                package = self._zynq_read(n_rcv, rData)
                self.debug('read_tbState regs %d through %d' % (max_addr-3*idx,
                                                            max_addr-3*idx-(3-1)))
                addr, values = self._unpack_read(package)
                values_list += list(values[::-1])
                time.sleep(.01)
            # last time through loop
            idx = iterNum
            rData = self._pack_read(max_addr - 3*idx, rem)
            n_rcv = Lmx2492.nHEADER + rem
            package = self._zynq_read(n_rcv, rData)
            self.debug('read_tbState regs %d through %d' % (max_addr-3*idx,
                                                      max_addr-3*idx-(rem-1)))

            addr, values = self._unpack_read(package)
            if rem==1:
                values_list.append(values)
            else:
                values_list += list(values[::-1])
            values_list = tuple(values_list)
        else:
            rData = self._pack_read(max_addr, length)
            # calc total received bytes on readback 
            n_rcv = Lmx2492.nHEADER + length
            package = self._zynq_read(n_rcv, rData)
            self.debug("read_tbState back %s" % package)
            addr, values_list = self._unpack_read(package)
            if type(values_list)==tuple:
                values_list = values_list[::-1]
        return addr, values_list
    
    def write_mem(self):
        ''' write from memory to the LMX         '''
        # do write in 2 steps
        self.spi_write(self.msb_addr, self.mem.get_top())
        time.sleep(self.SLP_TIME)
        self.spi_write(45, self.mem.get_bottom())
        self.printer('finished writing from mem')
    def reads_regs(self):
        ''' read out the lmx registers '''
        top = self.spi_read(self.msb_addr, 88)
        time.sleep(self.SLP_TIME)
        bottom = self.spi_read(45, 45)
        regs = top + bottom
        self.printer("reg printout:\n")
        print(regs)
        
    def load_mem(self,file):
        ''' load a hex .txt file to the LMX
        @param file [str]: the abs path .txt file to grab, output from TICS PRO
        '''
        from textwrap import wrap
        
        if file is not None:
            reg = ru.load_reg_map(file, go_fast=True)      
                # bytes-like str of [addrN, dataN, dataN-1 ... data1]
            start_addr = int(reg[0:2],16)  
            bValues = reg[2:]
            sList = wrap(bValues.decode(),2)    # elements are string hex of each register
            values_list = [int(x,16) for x in sList] # elements are int values for each reg
        else:
            values_list = np.zeros(131,dtype=int)
            start_addr = 131
        self.mem = lmxMemory(values_list)
        self.msb_addr = start_addr

    def printer(self,str):
        name = "LMX Controller"
        print("%s: %s" % (name,str))  
        
    def debug(self,str):
        if self._debug:
            self.printer(str)
    def __del__(self):
        if self.s is not None:
            self.s.close()
    def _printback(self, title, labs, val):
        ''' generic printback of a feature setting
        @param title [str]: the title for the readback"
        @param labs [str or list]: single or list of param names
        @param val [int or list]: single or list of param values
        @return val 
        '''
        if type(val)==tuple:
            for idx, ele in enumerate(val):
                title += labs[idx] + ': %d\t' % ele
        else:
            title += labs + ': %d\t' % val
        self.printer(title)
        return val    
    def set_cmds(self,):
        self.cmds = {
        'get' : {'power' : 'get the on/off state',
                 'ver' : 'get the version',
                 'reg0' : 'get the reg 0 value, should be 24',
                 'n' : 'get the n value',
                 'num' : 'get back the numerator value',
                 'den' : 'get back the denominator value',
                 'r' : 'get the r divider value',
                 'pump' : 'get the PUMP, POL config settings',
                 'thresh' : 'get the cp threshold values, HIGH, LOW',
                 'other' : 'get the other loop divider settings',
                 'fl_timer' : 'get the fastlock configs, CPG, TIMER',
                 'dld' : 'get the dig lock detect settings, CNT, ERR, TOL',
                 'io ADDR' : 'get the configurable i/o settings for channel ADDR, on [0,3]',
                  },
        'set' : {'on' : 'turn the pll chip on',
                 'off' : 'turn the pll chip off',
                 'n VAL' : 'set pll loop n value',
                 'num NUM' : 'set the pll loop numerator',
                 'den DEN' : 'set the pll loop denominator',
                 'r R' : 'set the pll loop R value',
                 'pump PUMP POL' : 'set the charge pump with value and polarity',
                 'thresh HIGH LOW' : 'set the charge pump comparator thresholds',
                 'other DBLR DIFF DLY CSR' : 'set the other settings for loop dividers',
                 'fl_timer CPG TIMER' : 'set the fastlock mode settings',
                 'dld ' : 'set the dig lock detect params, passes b4 lock detect, error, tol',
                 'io ADDR TYPE MODE' : 'set the i/o pin ',
                  },
        'ramp' : {'on' : 'turn on the ramping functions',
                  'off' : 'turn off the ramping functions',
                  'update' : 'reload the values of PLL from LMX hw',
                  'trap' : 'load in a trapezoid ramp config',
                  'simple'  : 'load in directly a 1-leg wave',
                  'tri' : 'load in a triangle wave config',
                  'user [[],...[]]' : 'load up ramps by end frequency and ramp duration',
                  'radar' : 'set up for plane radar waveform',
                  'precise' : 'set up for high res ranging waveform',
                  'fine' : 'set up super high res ranging',
                  'set' : {
                      'gen CLK PM CNT AUTO INC_SRC' : 'set the general ramp settings',
                      'limit HIGH LOW' : 'set the ramp freq limits',
                      'comp COMP0 COMP0_EN COMP1 COMP1_EN' : 'set the comparator settings',
                      'trig A B C' : 'set the trigger sources',
                      'reg N INC FL DLY LEN FLAG RST N_TRIG NEXT' : 'set the ramp config for ramp N',
                      },
                  'get' : {
                      'power' : 'get the power state of the ramp',
                      'gen' : 'get back the general settings',
                      'limit' : 'get back the ramp freq limits',
                      'comp' : 'get back the comparator settings',
                      'trig' : 'get the trigger source settings',
                      'reg N' : 'get the verbose ramp reg settings of ramp N',
                      'user' : 'get user settings as [[f0, dur0],...[fn,durn]]',
                      }
                  },
        'print' : 'print out the LMX memory',
        'reset' : 'sw reset the LMX driver, reload default registers',
        'end' : 'close the lmx',
                    }
        self.help = {
                    'set' : {'n' : 'VAL on [0, 262,143]',
                             'num' : 'NUM on [0, 16,777,215]',
                             'den' : 'DEN on [0, 16,777,215]',
                             'r' : 'R on [0, 65,535]',
                             'pump' : 'PUMP on [0,31], POL on [0,1]',
                             'thresh' : 'HIGH on [0,63], LOW on [0,63]',
                             'other'  : 'DBLR on  [0,1], DIFF on [0,1], DLY on [0,3], CSR on [0,3]',
                             'fl_timer' : 'CPG on [0,31], TIMER on [0,2047]',
                             'dld' : 'CNT on [0,127], ERR on [0,31], TOL on [0,5]',
                             'io' : 'ADDR on [0,3], TYPE on [0,7], MODE on [0,38]',
                            },  
                    'ramp' : {'gen' : 'CLK on [0,1], PM on [0,1], CNT on [0,4095]' \
                                    + 'AUTO on [0,1], INC_SRC on [0,3]',
                                'limit' : 'HIGH on [0,4.29B] in MHz, LOW on [0,4.29B] in MHz',
                                'comp' : 'COMP0 on [0,4.29B] in MHz, COMP0_EN on [0,255]' \
                                    + 'COMP1 on [0,4.29B] in MHz, COMP1_EN on [0,255]',
                                'trig' : 'A,B,C on [0,15]',
                                'reg' : 'IDX on [0,7], INC on [], FL on [0,1], DLY on [0,1]' \
                                    + 'LEN on [0,65536], FLAG on [0,3], RST on [0,1]' \
                                    + 'N_TRIG on [0,3], NEXT on [0,7]',
                                'user' : 'params as [[end_f0, dur0], ... [end_fn, durn]]',
                            },                   
                    }
        
    def get_cmds(self,):
        print(json.dumps(self.cmds, indent=4))
    def help_cmds(self, subcmd):
        print(json.dumps(self.help[subcmd], indent=4))
    def cmd_parse(self, cmd):
        ''' parse out user given commands 
        @param cmd [str]: string input from user or super-class
        '''
        inpt = cmd.lower()
        
        if inpt.startswith("help"):
            input_list = inpt.split(" ")
            if len(input_list) == 1:
                self.get_cmds()
            else:
                try:
                    maincmd = input_list[1]
                    # for now just print params for all commands of 1 type
                    self.help_cmds(maincmd)
                except:
                    print('Bad help command:  %s, use SET or RAMP' % maincmd)
                    
        elif inpt.startswith('reset'):
            input_list = inpt.split(" ")
            if len(input_list)==1:
                self.reset_reg()
            else:
                subcmd = input_list[1]
                if subcmd == 'io':
                    self.reset_io()
                else:
                    print('Bad reset command:  %s' % subcmd)  
        elif inpt.startswith('print'):
            input_list = inpt.split(" ")
            if len(input_list)==1:
                self.reads_regs()
            else:
                subcmd = input_list[1]
                if subcmd == 'io':
                    self.print_io()
                else:
                    print('Bad print command:  %s' % subcmd)  
        elif inpt == "end":
            self.__del__()
            exit()
        elif inpt.startswith("get"):
            input_list = inpt.split(" ")
            subcmd = input_list[1]
            if subcmd == 'ver':
                self.get_ver()
            elif subcmd == 'power':
                addr, val = self.get_power()
                self.printer("Power: %d" % val)
            elif subcmd == 'reg0':
                addr, val = self.get_reg0()
                self.printer("reg0 as %d" % val)
            elif subcmd == 'n':
                addr, n = self.get_n()
                self.printer("N value: %d" % n)
            elif subcmd == 'num':
                addr, num = self.get_frac_num()
                self.printer("NUM value: %d" % num)
            elif subcmd == 'den':
                addr, den = self.get_frac_den()
                self.printer("DEN value: %d" % den)
            elif subcmd == 'r':
                addr, r = self.get_pll_r()
                self.printer("R value: %d" % r)
            elif subcmd == 'pump':
                addr, pump, pol = self.get_charge_pump()
                self.printer("PUMP value: %d\tPOL value: %d" % (pump, pol))
            elif subcmd == 'thresh':
                addr, high, low = self.get_cp_thresh()
                self.printer("HIGH value: %d\tLOW value: %d" % (high, low))
            elif subcmd == "other" or subcmd == 'others':
                other_config = self.get_other_dividers()
                self.print_other_dividers(other_config)
            elif subcmd == "dld":
                err_config = self.get_dld()
                self.print_dld(err_config)
            elif subcmd == 'io':
                try:
                    # if given a ch
                    param1 = int(input_list[2])
                    mux_config = self.print_io(param1)
                except:
                    # otherwise print all 4
                    self.print_io()
            else:
                print("Bad GET command:  %s" % inpt)
        ## SET COMMANDS ###
        elif inpt.startswith("set"):
            input_list = inpt.split(" ")
            subcmd = input_list[1]
            if subcmd == 'on':
                val = 1
                self.set_power(val)
            elif subcmd == 'off':
                val = 0
                self.set_power(val)
            elif subcmd == 'n':
                #@TODO: put try/catch for all these so if i forget a param it fails safe
                val = int(input_list[2])
                self.set_n(val)
            elif subcmd == 'num':
                num = int(input_list[2])
                self.set_frac_num(num)
            elif subcmd == 'den':
                den = int(input_list[2])
                self.set_frac_den(den)
            elif subcmd == "r":
                r = int(input_list[2])
                self.set_pll_r(r)
            elif subcmd == "pump":
                pump = int(input_list[2])
                pol = int(input_list[3])
                self.set_charge_pump(pump, pol)
            elif subcmd == 'thresh':
                high = int(input_list[2])
                low = int(input_list[3])
                self.set_cp_thresh(high, low)
            elif subcmd == 'other':
                doubler = int(input_list[2])
                diff_r = int(input_list[3])
                pfd_delay = int(input_list[4])
                csr = int(input_list[5])
                self.set_other_dividers(doubler, diff_r, pfd_delay, csr)
            elif subcmd == 'fl_timer':
                cpg = int(input_list[2])
                timer = int(input_list[3])
                self.set_fastlock_timer(cpg, timer) 
            elif subcmd == 'dld':
                cnt = int(input_list[2])
                err = int(input_list[3])
                tol = int(input_list[4])
                self.set_dld(cnt, err, tol)
            elif subcmd == 'io':
                _addr = int(input_list[2])
                _type = int(input_list[3])
                _mode = int(input_list[4])     
                self.set_io(_addr, _type, _mode)        
            else:
                print("Bad SET command:  %s" % inpt)
        
        ## RAMP COMMANDS  ###
        elif inpt.startswith("ramp"):
            input_list = inpt.split(" ")
            cmd_len = len(input_list)
            subcmd = input_list[1]
            if subcmd == 'on':
                self.ramp_on()
            elif subcmd == 'off':
                self.ramp_off()
            elif subcmd == 'help':
                self.help_cmds('ramp')
            elif subcmd == 'update':
                self.update_rampy()
            elif subcmd == 'clear':
                self.clear_ramps()
                self.rampy.printer("cleared all ramps")
            elif subcmd == 'trap':
                self.set_trap()
#                 self.ramp_on()
                self.rampy.printer("set trap")
            elif subcmd == 'simple':
                self.set_simple()
                self.rampy.printer("set simple")
            elif subcmd == 'tri':
                self.set_triangle()
                self.rampy.printer("set triangle")
            elif subcmd == 'user':
                usr_str = input_list[2]
                usr_param = self.rampy.pack_helper(usr_str)
                self.set_ramping(usr_param)
                self.rampy.printer("set user ramps")
            elif subcmd == 'radar':
                self.set_radar()
#                 self.ramp_on()
                self.rampy.printer("set radar")
            elif subcmd == 'precise':
                self.set_precise()
#                 self.ramp_on()
                self.rampy.printer("set precise")
            elif subcmd == 'fine':
                self.set_fine()
                self.rampy.printer("set fine")
            ## RAMP GET ###
            elif subcmd == 'get':
                subsub = input_list[2]
                if subsub == 'power':
                    m = self.print_ramp_power()
                elif subsub == 'gen':
                    m = self.print_ramp_general()
                elif subsub == 'trig':
                    m = self.print_ramp_trig()
                elif subsub == 'comp':
                    m = self.print_ramp_comp()
                elif subsub == 'limit':
                    m = self.print_ramp_limit()
                elif subsub == 'reg':
                    ''' print verbose ramp settings '''
                    try:
                        # if given a ramp IDX
                        idx = int(input_list[3])
                        m = self.print_ramp_reg(idx)
                    except:
                        # else print all
                        self.printer('Not given ramp idx, printing all')
                        m = self.print_ramp_reg()  
                elif subsub == 'user':
                    ''' print abbrev ramp settings '''
                    m = self.print_user_ramps()                   
                else:
                    print("Bad Ramp GET command:  %s" % subsub)
            ## RAMP SET ####
            elif subcmd == 'set':
                subsub = input_list[2]
                if subsub == 'gen':
                    cnt = int(input_list[3])
                    err = int(input_list[4])
                    tol = int(input_list[5])
                    self.set_ramp_general()
                elif subsub == 'trig':
                    a = int(input_list[3])
                    b = int(input_list[4])
                    c = int(input_list[5])
                    self.set_ramp_trig(a, b, c) 
                elif subsub == 'comp':
                    comp0 = int(input_list[3])
                    comp0_en = int(input_list[4])
                    comp1 = int(input_list[5])
                    comp1_en = int(input_list[6])
                    self.set_ramp_comp(comp0, comp0_en, comp1, comp1_en)
                elif subsub == 'limit':
                    high = int(input_list[3])
                    low = int(input_list[4])
                    self.set_ramp_limit(high, low)
                elif subsub == 'reg':
                    nParam = len(input_list[3:])
                    if nParam == 9:
                        idx = int(input_list[3])
                        inc = int(input_list[4])
                        fl = int(input_list[5])
                        dly = int(input_list[6])
                        leng = int(input_list[7])
                        flag = int(input_list[8])
                        rst = int(input_list[9])
                        n_trig = int(input_list[10])
                        nexty = int(input_list[11])
                        self.set_ramp_reg(idx, inc, fl, dly, leng, flag, rst, n_trig, nexty)
                    else:
                        print("RAMP SET REG requires 9 params, given %d" % nParam)
                elif subsub == 'user':
                    # duplicate version of 'lmx ramp user ...'
                    usr_str = input_list[3]
                    usr_param = self.rampy.pack_helper(usr_str)
                    self.set_ramping(usr_param)
                else:
                    print("Bad Ramp SET command:  %s" % subsub)
        else:
            print("Bad user command:  %s" % inpt)
        

if __name__ == '__main__':
    
    
    print("lmx2492.py startup\n")
    
    
    parser = argparse.ArgumentParser(description=''' 
    This script is a controller for the LMX PLL chip on the HG radar.   
    The code is at the application layer, sitting on top of the tcp-socket layer''')


    # ACTIONS
    parser.add_argument('-r','--read_tbState', action='store', nargs=1, type=int,
                        help='read_tbState the state of a register')
    parser.add_argument('-ra','--readall', action='store_true', 
                        help='read_tbState the state of all register')
    parser.add_argument('-w','--write', action='store', nargs=2, default=None,
                        help='write a register as [-w ADDR HEX] ' +
                        'where ADDR is an int, and HEX is the ascii hex characters')
    parser.add_argument('-l','--load', action='store', nargs=1, default=None,
                        help="load in the relative path of register settings file")
    
    # USER INTERACTIVE
    parser.add_argument('-u','--user', action='store_true', default=False,
                        help='run script in user interactive mode')
    
    # get arguments
    args = parser.parse_args(sys.argv[1:])
    
    absFilePath = os.path.abspath(__file__)          # Absolute Path of the script
    fileDir = os.path.dirname(absFilePath)           # Directory of the script
    if args.load is None:
        fname = fileDir + '\\LMX2492_19_10_25.txt'
    else:
        # load the special reg file given on CLI
        fname = fileDir + args.load
        
    lmx = Lmx2492(fname)
    
        
    if args.read_tbState is not None:
        addr = args.read_tbState[0]
        rd_reg, rd_bValues = lmx.zynq_read(addr, 2)

    if args.readall is True:
        pass
    if args.write is not None:
        addr = int(args.write[0])
        val = bytes(args.write[1],'utf-8')
        lmx.spi_write(addr, val)

    
    if args.user is True:
        prompt = "> Enter a command to continue, use 'help' for options\n"

        inpt = input(prompt).lower()        
        while True:
            # ask for user input again
            lmx.cmd_parse(inpt)
            inpt = input(prompt).lower()
                
    print("lmx2492.py close()")
    exit()
        
    

