"""Sending and Receive TCP Packets wrapping SPI."""
# ====================================================
#
#  Title    : Sending and Receive TCP Packets
#             to / from Zynq Server wrapping SPI DMA
#
#  Author   : James Bonanno <james@atlantixeng.com>
#
#  Filename : regress_Cora.py
#
#  Platform : python 3.6+
#
#  History  :
#  0.1 -- 25 Oct 2019 -- Initial release
#  0.2 -- 29 Oct 2019 -- Expansion for Reading/Writing ADC BRAM
#  0.3 -- 03 Dec 2019 -- Testing of Emulated AD9649 Trace Buffer 
#                         
#
#  Descrip  : Test sending and requesting data from the
#             Zynq server, using the packet module with
#             functions of sender and request.
#
#             Overall, this module serves as a basic
#             REGRESSION TEST for the communications
#             interface between the PC and Zynq. 
#
#             A unit test framework with closed loop PASS/FAIL
#             determination has been created for the 
#             BRAM ADC. 
#
#             A unit test exists for triggering the internal
#             emulated AD9649 module, then for reading the 
#             BRAM. In this case, a golden reference is 
#             generated corresponding to the way the Verilog
#             AD9649 module is generating data. 
#
#
# ====================================================
import socket
import struct
import numpy as np
from random import *
import time 

HOST = '192.168.1.17'  # The remote host
PORT = 7                # The same port as used by the server


CMD_SPI = 0xAA
SPI_ADC = 0xBB
CMD_ADC = 0xCC
NUM_HEADER_BYTES = 4

def unpack_header(header_bytes,data):
    """ Process a received TCP socket header. """
    jj=0
    print("****  Processing packet header.")
    beta  = struct.unpack(('<4B'),data[0:header_bytes])
 
    for item in beta:
        print("header[{}] \t\t{} \t({}) ".format((jj), hex(item), item))        
        jj += 1


def unpack_data(header_bytes,data,num_fetch, reference, offset):
    """ Unpack data coming over TCP Socket send from FPGA ARM SoC.  
	    In general, packet = header (beta) + data (alpha)

    Keyword arguments:
    header_bytes -- number of bytes in header.
    data         -- vector of 32 bit data values.
    num_fetch    -- length of data vector.
    reference    -- the golden reference vector initially created to write to memory. 
    offset       -- optional addr offset, i.e. to read a segment of memory

    """
    kk = 0
    jj = 0
    num_errors = 0
    sizeof = len(data) - header_bytes
    expected = 4*num_fetch
    if sizeof != expected:
        padding = True
        padding_diff = sizeof - header_bytes - expected
    else:
        padding = False 

    print("****  Processing packet.")
    #print("****  len(packet_data)={}".format(sizeof))
    #print("padding={}".format(padding))
    #print("****  Processing packet.\r\n")
    beta  = struct.unpack(('<{}B'.format(header_bytes)),data[0:header_bytes])
    alpha = struct.unpack(('<{}I'.format(num_fetch)),data[header_bytes:(header_bytes+(4*num_fetch))])

    #for item in beta:
    #    print("header[{}] \t\t{:02X} ".format((jj), item))        
    #    jj += 1
    for item in alpha:
        #print("data[{}] \t\t{:02X} expected \t {}".format((offset+kk), item, reference[kk]))
        print("data[{}] \t\t{} expected \t {}".format((offset+kk), item, reference[kk]))
        if (item != reference[kk]):
            num_errors += 1
        
        kk += 1
    if (num_errors == 0):
    	print("******** Errors = 0")
    	print("******** Unit Test SUCCESSFULL.\r")

def trace_trigger():
	""" The request packet is based on 4 bytes cmd, 1-3 bytes data
	"""
	word1 = 0xDD
	word2 = 0x02
	packet_header_cmd  = [word1, word2 ]

	packet_binary = struct.pack(('<2B '), *packet_header_cmd)
	s.sendall(packet_binary)
	print("Triggered the trace buffer.\r\n")
	
def trace_testTrigger():
	""" The request packet is based on 4 bytes cmd, 1-3 bytes data
	"""
	word1 = 0xDD
	word2 = 0x0F
	packet_header_cmd  = [word1, word2 ]

	packet_binary = struct.pack(('<2B '), *packet_header_cmd)
	s.sendall(packet_binary)
	word1 = 0xDD
	word2 = 0x02
	packet_header_cmd  = [word1, word2 ]

	packet_binary = struct.pack(('<2B '), *packet_header_cmd)
	s.sendall(packet_binary)
	print("Emulated AD9649 enabled and triggered the trace buffer.")

def request_spi(addr, values_list):
	""" The request packet is based on 4 bytes cmd, 1-3 bytes data
	"""
	word1 = CMD_SPI
	word2 = 0x01
	word3 = int(addr)
	word4 = int(len(values_list))
	packet_header_cmd  = [word1, word2, word3, word4 ]
	packet_header_data = values_list
	packet_binary = struct.pack(('<4B {}B').format(word4), *packet_header_cmd, *packet_header_data)
	return packet_binary

def read_spi(addr, length):
	""" The request packet is based on 4 bytes cmd, 1-3 bytes data
	"""
	word1 = CMD_SPI
	word2 = 0x00
	word3 = int(addr)
	word4 = int(length)
	packet_header_cmd  = [word1, word2, word3, word4 ]


	packet_binary = struct.pack(('>4B'), *packet_header_cmd)

	return packet_binary

def request_adc(RnW, addr, values_list, len):
	""" The request packet is based on 4 bytes cmd, 1-3 bytes data
	"""
	word1 = CMD_ADC

	word2 = RnW 
	word3 = int(addr)
	word4 = len
	packet_header_cmd  = [word1, word2, word3,word4 ]
		
	if(RnW):
		packet_header_data = values_list
		packet_binary = struct.pack(('<4B {}I').format(word4), *packet_header_cmd, *packet_header_data)
	else:
		packet_binary = struct.pack(('<4B').format(word4), *packet_header_cmd)

	return packet_binary

def gen_reference(seed, size, marker, baseX):
    """ Return a python list from numpy vector of length size with randomized values.
        Also note that if seed ==1, the list list is fixed slope linear. 

    Keyword arguments:
    seed -- sets the range of randomized stochastic multiplier.
    size -- the length of the vector.
    marker -- used to distinguish the first value in vector. 
    baseX  -- the normal scalar to the vector, i.e. baseX = 100 
              results in an arry of [0,100,200,300,..]  
    """
    values = randint(1, seed)*np.linspace(1, size, size)
    num_send = len(values)
    values = [int(baseX * item - baseX) for item in values]
    values[0] = marker
    return values 

def unit_test_1():
	packet = request_spi(0x1A, [0x00, 0x02])
	s.sendall(packet)
	print("***** UNIT TEST 1A Sending SPI DATA @ 3 Bytes - Completed ")

def unit_test_1B():
	packet = read_spi(0x0A, 3)
	s.sendall(packet)
	print("***** UNIT TEST 1B Reading SPI DATA @ 3 Bytes - Completed ")

def unit_test_2():
	packet = request_spi(0x05, [0x05, 0x07])
	s.sendall(packet)
	print("***** UNIT TEST 2 Sending SPI DATA @ 2 Bytes - Completed ")

def unit_test_3():
	packet = request_spi(0x05, [0x05])
	s.sendall(packet)
	print("***** UNIT TEST 3 Sending SPI DATA @ 1 Byte  - Completed ")

def unit_test_writeADC(values_list):
	packet = request_adc(RnW = 1, addr = 0x00, values_list = values_list, len = len(values_list))
	s.sendall(packet)
	data = s.recv(2048)
	print("***** UNIT TEST 'WRITING ADC' BRAM Data @ {} Words  - Completed ".format(len(values_list)))

def unit_test_readADC(length_to_read = 10, reference = [0]):
	print("***** READING ADC BRAM Data @ {} Words ".format(length_to_read))
	packet = request_adc(RnW = 0, addr = 0x00, values_list = [0], len = length_to_read)
	s.sendall(packet)
	time.sleep(.5)
	num_bytes_rx = 4*length_to_read + NUM_HEADER_BYTES
	data = s.recv(num_bytes_rx)
	#unpack_header(4, data)
	unpack_data(header_bytes = NUM_HEADER_BYTES, data = data, num_fetch = length_to_read, reference = reference, offset = 0)
	#print("***** READING OF BRAM Data @ {} Words - Completed ".format(length_to_read))


if __name__=="__main__":
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((HOST, PORT))

	
	""" Unit test scenario for ADC Block RAM (BRAM)
		
		This unit test is comprised of 3 steps:
		1. generate a reference
		2. write the reference to the block RAM
		3. read the block RAM back 
		4. compare golden reference to readback values
		5. issue a pass / fail for this unit test 

		NUM_WORDS -- number of words to generate in golden vector
		unit_test_writeADC -- writes to ADC BRAM 
		unit_test_readADC  -- reads from ADC BRAM & issues PASS/FAIL

	"""

	TRACE_TEST = False
	SPI_TEST = False
	BRAM_TEST = True 

	TRACE_BUFFER_EMULATED_TEST = False

	A = 0
	B = 1


	if SPI_TEST:
		if A:
			unit_test_1()
		elif B:
			unit_test_1B()

	if BRAM_TEST:
		NUM_WORDS = 15
		golden_vector = gen_reference(seed=1, size = NUM_WORDS, marker = 555, baseX = 100)
		unit_test_writeADC(values_list = golden_vector);
		unit_test_readADC(length_to_read = NUM_WORDS, reference = golden_vector)

	if TRACE_BUFFER_EMULATED_TEST: 
		print("*****  EMULATED AD9649 with TRACE BUFFER UNIT TEST.\r\n")
		trace_testTrigger()
		time.sleep(1)
		NUM_WORDS = 125
		golden_vector = gen_reference(seed=1, size = NUM_WORDS, marker = 0, baseX = 3)
		unit_test_readADC(length_to_read = NUM_WORDS, reference = golden_vector)