'''
Created on Sep 25, 2019

@author: Darren
'''



class ZynqReg(object):
    
    class REG0(object):
        # dpA - 16 bits
        EN_CH8_NEG = 0x0001         # bit 0
        EN_CH8_POS = 0x0002         # bit 1
        EN_CH2_NEG = 0x0004         # bit 2
        EN_CH2_POS = 0x0008         # bit 3
        BB_DENB_B = 0x0010          # bit 4
        BB_DENB_A = 0x0020          # bit 5
        BB_GAIN_BIT3 = 0x0040       # bit 6
        BB_GAIN_BIT2 = 0x0080       # bit 7
        BB_GAIN_BIT1  = 0x0100      # bit 8
        BB_GAIN_BIT0 = 0x0200       # bit 9
        RX_ATTEN2_A0 = 0x0400       # bit 10
        RX_ATTEN2_A1 = 0x0800       # bit 11
        RX_ATTEN2_A2 = 0x1000       # bit 12
        RX_ATTEN1_A0 = 0x2000       # bit 13
        RX_ATTEN1_A1 = 0x4000       # bit 14
        RX_ATTEN1_A2 = 0x8000       # bit 15

    class REG1(object):
        # dpB - 11 bits
        OPEN_R1_0 = 0x0001          # bit 0
        LO_PLL_MUXOUT = 0x0002      # bit 1
        LO_TRIG1 = 0x0004           # bit 2
        LO_TRIG2 = 0x0008           # bit 3
        LO_SW_B2 = 0x0010           # bit 4
        LO_SW_A2 = 0x0020           # bit 5
        LO_SW_B1 = 0x0040           # bit 6
        LO_SW_A1 = 0x0080           # bit 7
        CH_SEL_A0 = 0x0100          # bit 8
        CH_SEL_A1 = 0x0200          # bit 9
        CH_SEL_A2 = 0x0400          # bit 10
    
    class REG2(object):
        # dio - 12 bits
        EN_CH6_POS = 0x0001         # bit 0
        EN_CH6_NEG = 0x0002         # bit 1
        EN_CH7_POS = 0x0004         # bit 2
        EN_CH7_NEG = 0x0008         # bit 3
        EN_CH5_POS = 0x0010         # bit 4 
        EN_CH5_NEG = 0x0020         # bit 5
        EN_CH4_POS = 0x0040         # bit 6
        EN_CH4_NEG = 0x0080         # bit 7
        EN_CH3_POS = 0x0100         # bit 8
        EN_CH3_NEG = 0x0200         # bit 9
        EN_CH1_POS = 0x0400         # bit 10
        EN_CH1_NEG = 0x0800         # bit 11
    
    class REG3(object):
        # jA - 8 bits
        ADC_D0 = 0x0001             # bit 0
        ADC_D2 = 0x0002             # bit 1
        ADC_D4 = 0x0004             # bit 2
        ADC_D6 = 0x0008             # bit 3
        ADC_DCO = 0x0010            # bit 4
        ADC_D1 = 0x0020             # bit 5
        ADC_D3 = 0x0040             # bit 6
        ADC_D5 = 0x0080             # bit 7
    
    class REG4(object):
        # jB - 8 bits
        ADC_D8 = 0x0001             # bit 0
        ADC_D10 = 0x0002            # bit 1
        ADC_D12 = 0x0004            # bit 2
        ADC_MODE_OR = 0x0008        # bit 3
        ADC_D7 = 0x0010             # bit 4
        ADC_D9 = 0x0020             # bit 5
        ADC_D11 = 0x0040            # bit 6
        ADC_D13 = 0x0080            # bit 7
    
    class REG5(object):
        # dAux  - 6 bits
        AUX_DOUT0 = 0x0001          # bit 0    # A11
        AUX_DOUT1 = 0x0002          # bit 1    # A10
        AUX_DOUT2 = 0x0004          # bit 2    # A7
        AUX_DOUT3 = 0x0008          # bit 3    # A6
        AUX_DIN0 = 0x0010           # bit 4    # A8?
        AUX_DIN1 = 0x0020           # bit 5    # A9?

    class REG6(object):
        # dSPI  - 5 bits
        SPI_CLK = 0x0001            # bit 0    CLK
        SPI_MOSI = 0x0002           # bit 1    MOSI
        SPI_LO_LE = 0x0004          # bit 2    CS_PLL
        LO_MOD = 0x0008             # bit 3    MISO
        SPI_ADC_CS = 0x0010         # bit 4    CS_ADC       
        
        
    def __init__(self):
        
        # initial settings 
        self.reset()

        self.reg_names = [b'reg0', b'reg1', b'reg2', b'reg3',
                           b'reg4', b'reg5', ]
#                            b'reg6']
        self.ch_pos = [[2, ZynqReg.REG2.EN_CH1_POS],
                       [0, ZynqReg.REG0.EN_CH2_POS],
                       [2, ZynqReg.REG2.EN_CH3_POS],
                       [2, ZynqReg.REG2.EN_CH4_POS],
                       [2, ZynqReg.REG2.EN_CH5_POS],
                       [2, ZynqReg.REG2.EN_CH6_POS],
                       [2, ZynqReg.REG2.EN_CH7_POS],
                       [0, ZynqReg.REG0.EN_CH8_POS]]
        self.ch_neg = [[2, ZynqReg.REG2.EN_CH1_NEG],
                       [0, ZynqReg.REG0.EN_CH2_NEG],
                       [2, ZynqReg.REG2.EN_CH3_NEG],
                       [2, ZynqReg.REG2.EN_CH4_NEG],
                       [2, ZynqReg.REG2.EN_CH5_NEG],
                       [2, ZynqReg.REG2.EN_CH6_NEG],
                       [2, ZynqReg.REG2.EN_CH7_NEG],
                       [0, ZynqReg.REG0.EN_CH8_NEG]]
        
    
    def get_name(self,id):
        return self.reg_names[id]
    def __getitem__(self,i):
        return self.regs[i]
    def __len__(self):
        return len(self.regs)
    
    def reset(self):
        # initial startup settings 
        # reg0 - dpA - 16 bits
        self.reg0 = 0
        
        # reg1 - dpB - 11 bits
        self.reg1 = 0
        
        # reg2 - dio - 12 bits
        self.reg2 = 0
        
        # reg3 - jA - 8 bits
        self.reg3 = 0
        
        # reg4 - jB - 8 bits
        self.reg4 = 0
        
        # reg5 - aux - 6 bits
        self.reg5 = 0
        
#         # reg6 - dpSPI  - 5 bits
#         self.reg6 = 0
        
        # now store the regs into the reg-array   
        self.regs = [self.reg0, self.reg1, self.reg2, self.reg3,
                     self.reg4, self.reg5,]
#                      self.reg6]
    
    
    def set_channel(self,ch):
        '''
        set the baseband mux channel output, controlling which channel goes to ADC
        @param ch [int]: one of [1,8] to select
        '''
        if ch<1 or ch>8:
            print("ch must be int in [1,8]")
        
        if ch == 1:
            # ch = 0 | 0 | 0
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A1 
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A0 
        elif ch == 2:
            # ch = 0 | 0 | 1
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A1  
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A0 
        elif ch == 3:
            # ch = 0 | 1 | 0
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A1
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A0 
        elif ch == 4:
            # ch = 0 | 1 | 1
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A1
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A0 
        elif ch == 8:
            # ch = 1 | 0 | 0
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A1 
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A0  
        elif ch == 7:
            # ch = 1 | 0 | 1
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A1  
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A0 
        elif ch == 6:
            # ch = 1 | 1 | 0
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A2 
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A1
            self.regs[1] &= ~ZynqReg.REG1.CH_SEL_A0 
        elif ch == 5:
            # ch = 1 | 1 | 1
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A2
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A1
            self.regs[1] |= ZynqReg.REG1.CH_SEL_A0
        
    def set_bbGain(self,multi,level):
        ''' set the baseband gain level by level and multiplier
                gain = multi * code_to_db(level)
        @param multi [int]: an int of 0,1,2 
        @param level [int]: an integer for the gain code, on [0,15]
        '''
        # enables on reg0 
        # gain bits on reg0, reg2
        if multi<0 or multi>2:
            print("multi must be an int, one of 0,1,2")
        if level<0 or level>15:
            print("level must be an int on [0,15]")
        
        if multi==0:
            self.regs[0] &= ~ZynqReg.REG0.BB_DENB_A
            self.regs[0] &= ~ZynqReg.REG0.BB_DENB_B
        elif multi==1:
            self.regs[0] &= ~ZynqReg.REG0.BB_DENB_A
            self.regs[0] |= ZynqReg.REG0.BB_DENB_B
        elif multi==2:
            self.regs[0] |= ZynqReg.REG0.BB_DENB_A
            self.regs[0] |= ZynqReg.REG0.BB_DENB_B
            
        if level==0:
            # -10 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3   
        elif level==1:
            # -7 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3
        elif level==2:
            # -4 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3        
        elif level==3:
            # -1 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3
        elif level==4:
            # 2 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3  
        elif level==5:
            # 5 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3  
        elif level==6:
            # 8 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3
        elif level==7:
            # 11 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT3  
        elif level==8:
            # 14 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3  
        elif level==9:
            # 17 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3  
        elif level==10:
            # 20 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3   
        elif level==11:
            # 23 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3           
        elif level==12:
            # 26 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3   
        elif level==13:
            # 29 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3     
        elif level==14:
            # 32 dB
            self.regs[0] &= ~ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3     
        elif level==15:
            # 35 dB
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT0
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT1
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT2
            self.regs[0] |= ZynqReg.REG0.BB_GAIN_BIT3   
    
    def set_rxAtten(self,level):
        ''' set the rx attenuator amount by level 
        @param level [int]: an integer for the atten level, on [0,7], 7 is max
        '''
        # enables on reg0 
        # atten bits on reg0
        if level<0 or level>7:
            print("level must be an int on [0,7]")
             
        if level==0:
            # min atten
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A2
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A2
        elif level==1:
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A2   
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A2
        elif level==2:
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A2     
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A2
        elif level==3:
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A2
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A2
        elif level==4:
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A2            
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A2
        elif level==5:
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A2
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A2  
        elif level==6:
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A2                  
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A2
        elif level==7:
            # max atten
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN1_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN1_A2           
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A0
            self.regs[0] &= ~ZynqReg.REG0.RX_ATTEN2_A1
            self.regs[0] |= ZynqReg.REG0.RX_ATTEN2_A2
                
    def set_loChan(self,chan):
        ''' set the lo sig distribution to a given 
        @param chan [int]: the desired lo channel to activate, on [1,8]
        '''
        if chan<1 or chan>8:
            print("level must be an int on [1,8]")
        
        # pins are on reg1
        if chan==1 or chan==5:
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A2
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B2
        elif chan==2:
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B1
            self.regs[1] |= ZynqReg.REG1.LO_SW_A2
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B2
        elif chan==3:
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A2
            self.regs[1] |= ZynqReg.REG1.LO_SW_B2
        elif chan==4:
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B1
            self.regs[1] |= ZynqReg.REG1.LO_SW_A2
            self.regs[1] |= ZynqReg.REG1.LO_SW_B2
        elif chan==6:
            self.regs[1] |= ZynqReg.REG1.LO_SW_A1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A2
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B2
        elif chan==7:
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A1
            self.regs[1] |= ZynqReg.REG1.LO_SW_B1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A2
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B2
        elif chan==8:
            self.regs[1] |= ZynqReg.REG1.LO_SW_A1
            self.regs[1] |= ZynqReg.REG1.LO_SW_B1
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_A2
            self.regs[1] &= ~ZynqReg.REG1.LO_SW_B2