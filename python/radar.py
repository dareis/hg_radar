#!/usr/bin/env python

'''
Python controller for the Cora board
Darren Reis
9/2019 
'''


import os, sys
import socket
import pprint
import argparse
import json 
import struct

import matplotlib.pyplot as plt
import numpy as np
from time import sleep

pp = pprint.PrettyPrinter(indent=4)


# HG imports 
import radTests as rt
from zynq_registers import ZynqReg 
import utils as ru
from lmx2492 import Lmx2492
from ad9649 import AD9649

class DDRBuffer(object):
    
    # channel identifiers
    CMD_DDR = 0xEE  
    RSP_DDR = 0xE8

    nHEADER = 2 + 4*2   # u8, u8, u32, u32
    DDR_CHUNK = 2000
    
    def __init__(self, sock=None, do_debug=False):
        
        the_ip = "192.168.1.35"  # Darren
        port = 7
        
        # config settings
        self.rwait_time = .05    # time (s) between status poll and response
        self._debug = do_debug
        
        if sock is None:
            try:
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.connect((the_ip, port))
            except socket.error as err:
                print("socket exception %s" % err)
        else:
            self.s = sock
      
        self._debug = do_debug
        self.set_cmds()
    ## -----   Config Settings   ------
    def set_rwait_time(self, sec):
        self.rwait_time = sec 
    def get_rwait_time(self):
        return self.rwait_time
      
    ## ------  HIGH LEVEL ------
    def read_samp(self,num=30e3):
        ''' read the adc sampling in proper format with timing 
        @param num [int]: the number of sample-pairs to grab
        @return samp [np.array]: the data not-yet-parsed [b15, b14, val14], where
            the 0th element is oldest item
        @return dtm [datetime]: the ending time of the collection 
        '''
        data = self.get_burst(addr=0x0, leng=num)
        samp = np.array(data)
        from datetime import datetime
        dtm = datetime.utcnow()  
        return samp, dtm
    
    def get_burst(self, addr=0, leng=400, ):
        ''' run a single trigger of the adc collection. grab the results from
        the ddr memory, noting that newest samples are higher addresses.
        @param addr [u32]: the lowest bram address to start read from, default 0
        @param leng [u32]: the number of u32 registers to read, default 255
        @return data [np.array]: signals collected over full experiment
        '''
        rd_addr, data = self.read_ddr(addr, leng)
        assert rd_addr == addr
        return data
    
        
    ## ----    DDR   -------
    def write_ddr(self, addr, values_list):
        ''' write data into the ddr memory
        @param addr [u32]: the low ddr address to start writing to
        @param values_list [u16 list]: the data to actually put in ddr
        '''
        leng = len(values_list)
        packet = self._pack_bram1(1, addr, leng, values_list)
        rsp = self.sock_write(packet)
    
    
    def _read_ddr(self, addr, leng):
        ''' read the ddr memory in chunks
        @param addr [u32]: the low bram address to start the read from  @TODO: make u16
        @param leng [u32]: the number of bytes to read
        @param r_addr [u32]: the starting bram address read from (reads upwards)
        @return data [u16 list]: the readback data 
        '''
#         if leng>100:
#             self.printer('length above tcp safety limit 100, %d' % leng)
#             return
        
        packet = self._pack_ddr(RnotW = 0, addr = addr,
                                        leng = leng, values_list = [0])
        rsp = self.sock_write(packet)
        rsp_packet = rsp.replace(b'\r\n', b'')  # dump '\r\n'
        r_addr, data = self._unpack_ddr(rsp_packet)         

        return r_addr, data

    def read_ddr(self, addr, leng):
        ''' read the ddr memory back
        @param addr [u32]: the low bram address to start the read from, must be even
        @param leng [u32]: the number of bytes to read
        @return data [u16 np.array]: the readback data
        '''
        if addr % 2 == 1:
            print("addr must be even, given %d" % addr)
            return
        sze = self.DDR_CHUNK
        idx = 0

        if leng>sze:
            data = []
             
            rem = leng
            while rem>sze:
                if idx % 10 == 0:
                    print('loop %d of %d' % (idx, int(leng/sze)-1))
                the_addr = addr + idx*sze
                r_addr, chunk = self._read_ddr(the_addr, sze)
                assert r_addr == the_addr
                data += list(chunk)
                rem -= sze
                idx += 1
            _, chunk = self._read_ddr(addr + idx*sze, rem)
            data += list(chunk)
            data = np.array(data)
            r_addr = addr
        else:
            r_addr, data = self._read_ddr(addr, leng)
            
        return r_addr, data
    
    # ADC READ/WRITE
    def _pack_ddr(self, RnotW, addr, leng, values_list=[0]):
        """ build a ddr command, used for read and write
        @param RnotW [u8]: conditional for doing readback, 
                            0 for read, 1 for write
        @param addr [u32]: the highest register value   
        @param leng [u32]: length of list or length of intended read         
        @param value_list [list]: list of u16's, the values to put in reg's; 
                unused if RnW==0

        @return packet_binary [ddrData_t]: a binary packed bytes object
        """
        word1 = self.CMD_DDR
        word2 = RnotW
        word3 = int(addr)
        word4 = int(leng)
        packet_header_cmd  = [word1, word2, word3, word4 ]
            
        if(RnotW):
            print("untested _pack_ddr write case")
            packet_header_data = values_list
            packet_binary = struct.pack(('<BBII {}H').format(word4),
                                         *packet_header_cmd, *packet_header_data)
        else:
            packet_binary = struct.pack('<BBII', *packet_header_cmd)

        return packet_binary
    
    def _unpack_ddr(self,package):
        """ unwrap the ddrData_t package back from the PS
        @param package [ddrData_t]: the packed struct to unwrap
        @return addr [u32]: the starting register address
        @return values_list [list of u16]: the values of registers read
        """
        m = self.nHEADER
        ch_type, _, addr, length = struct.unpack(('<BBII'),package[0:m])
        assert ch_type == self.RSP_DDR

        #length refers to number of bytes
        # load list of bytes
        values_list = struct.unpack('<{}H'.format(int(length/2)), package[m:])
     
        return addr, values_list    

    ## ----- SOCKET LOW LEVEL METHODS  -------
    def sock_write(self,cmd):
        '''  send a command over the tcp socket, do a read to hear instant response
        @param cmd [str]: the command to send
        '''
        if cmd[-4:]!=b'\r\n':
            cmd += b'\r\n'
            
        self.s.sendall(cmd)
        sleep(.5)
        rsp = self.s.recv(2048)
        return rsp

    ## ------ ADMIN ------
    def __del__(self):
        if self.s is not None:
            self.s.close()
            
    def printer(self,strg):
        name = "DDR Controller"
        print("%s: %s" % (name,strg))  
    def debug(self,strg):
        if self._debug:
            self.printer(strg)
    def set_cmds(self,):
        self.cmds = {'clear' : 'clear out the bram memory',
                     'get' : {
                            'burst' : 'one burst at memory of ADC samples',
                             }, 
                    }
                        
        self.help = {
                     }                
                
    def get_cmds(self,):
        print(json.dumps(self.cmds, indent=4))
    def help_cmds(self, subcmd):
        print(json.dumps(self.help[subcmd], indent=4)) 
    def cmd_parse(self,cmd):
        ''' parse out user given commands 
        @param cmd [str]: string input from user or super-class
        '''
        inpt = cmd.lower()  
        if inpt.startswith("help"):
            input_list = inpt.split(" ")
            if len(input_list) == 1:
                self.get_cmds()
            elif len(input_list) == 2:
                print("todo")
            else:
                self.printer("Bad HELP cmds: %s" % input_list)
            
        elif inpt.startswith("get"):
            inpt_list = inpt.split(" ")
            subcmd = inpt_list[1]
            if subcmd == "burst":
                data, t = self.read_samp()
                mu = np.mean(data)
                pk = np.max(data)
                vl = np.min(data)
                self.printer("got burst data like: %d\n%d\n%d" % (data[0], data[1], data[2]))
                self.printer("\nNum: %d\tMean: %.1f\tMax/Min: %.1f/%.1f" % \
                             (len(data), mu, pk, vl))
            else:
                self.printer("Bad GET command:  %s" % subcmd)
        elif inpt.startswith("clear"):
            self.clear_ddr()
            self.printer("DDR cleared")
        else:
            self.printer("Bad command:  %s" % inpt) 
            
class Zynq(object):
    ''' driver class for communicating with the tcpip server and cora  '''
    
    ## Class Attributes
    VERSION = "1.2.0"  # 2/28/20
    SLP_TIME = .05
    
    def __init__(self, ip, simMode=False, do_debug=False, playbackMode=False):
        ''' 
        Constructor
        '''
        self.name = "HG Radar"
        self.ip = ip
        self._debug = do_debug
        self.ver = Zynq.VERSION
        self.default = [[0, 0x0000], [1, 0x0000], [2,0x0000], [3, 0x3],
                        [4, 0x4], [5, 0x5]]
        
        # internal registers
        self.mem = ZynqReg()
        self.chan = None
        
        if playbackMode:
            self.set_cmds()
            self.quick_load()
        else:
            
            # set up socket        
            try:
                self.printer("setting up socket for ip %s" % ip)
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.connect((ip, 7))
            except socket.error as err:
                print("socket exception %s" % err)
                exit
            
            # lmx driver
            absFilePath = os.path.abspath(__file__)          # Absolute Path of the script
            fileDir = os.path.dirname(absFilePath)           # Directory of the script
            fname = os.path.join(fileDir, 'bench_19_7_11.txt')
            self.lmx = Lmx2492(fname, self.s, do_debug, do_sim=simMode)
            if not simMode:
                self.lmx.test_n()   # sanity check
            
            # DDR
            self.ddr = DDRBuffer(self.s, do_debug)
            sleep(self.SLP_TIME)
            
            self.Fs = 50e6          # Hz
            self.dt = 1.0/self.Fs   # s
            
            # adc driver
            self.adc = AD9649(self.s, do_debug)
            sleep(self.SLP_TIME)
            self.adc.set_output_mode(2, 0, 1), sleep(self.SLP_TIME)   # use twos complement encoding
            
            self.set_cmds()
            
            # darren unit
            self.set_loChan(2), sleep(self.SLP_TIME)
            self._enable_rf_pos(1), sleep(self.SLP_TIME)
            self.lmx.test_ramp_precise(True)  # set and test ramp precise setting
            self.lmx.ramp_on()
            self.set_bbGain(2, 2), sleep(self.SLP_TIME)
            
        
    
    
    def __del__(self, ):
        self.s.close()
        self.printer("Deleting Zynq")
        
    def get_default(self,):
        ''' get the default register values '''
        return self.default
    def set_default(self,regList):
        ''' set the default register values for all reg 
        @param regList [2d list]: reg settings, w/ each element as [regIdx, state_int]
        '''
        self.default = regList           
        
    ## -----  local Memory  ------
    def reset_regs(self):
        self.write_all_regs(self.default)
        self.debug(b'reset all registers')
        self.adc.set_output_mode(2, 0, 1)   # use twos complement encoding
        self.printer('Reset all regs')
    def write_all_regs(self,rList):
        ''' write all the registers with the list of reg values
        @param rList [list]: list of int values for each register
        '''
        for ele in rList:
            self.put_reg(ele[0], ele[1])
        self.debug(b'written all registers')
    def read_all_regs(self):
        ''' read all the registers 
        @return out_list [2d list]: elements are [regIdx, state_int]
        '''
        rsp = self.sock_write(b'get regs')
        rsp_lines = rsp.replace(b'\x00',b'').split(b'\r\n')
        rsp_lines = [x.replace(b'\n',b'') for x in rsp_lines]  # remove pesky \x00
        rsp_lines = [x for x in rsp_lines if len(x)>1]
        
        out_list = []
        for line in rsp_lines:
            reg, state = self.parse_reg(line)
            out_list.append([reg,state])
            self.debug(b"reg %d read as 0x%x" % (reg, state))
            self.mem.regs[reg] = state
        self.debug('read all registers')
        return out_list
    
    
    ## ----    Radar Methods  -------
    def get_hw_tag(self,):
        '''  get hw version information '''
        version_raw = self.sock_write(b'get ver')
        version_formatted = version_raw.replace(b"*", b"").split(b"\r\n")
        pp.pprint(version_formatted)
    
    def get_options(self,):
        '''  get command options   '''
        options_raw = self.sock_write(b"get opt")
        options_formatted = options_raw.replace(b"*",b"").split(b"\r\n")
        pp.pprint(options_formatted)
    def put_led(self, color):
        '''  turn led diff colors or off  '''
        if color==b'red':
            cmd = b"put red"
        elif color==b'green':
            cmd = b"put grn"
        elif color==b'blue':
            cmd = b"put blue"
        elif color==b'off':
            cmd = b"put off"
        else:
            print('use one of [red | green | blue | off]')
        self.sock_write(cmd)
    def turn_off(self):
        ''' turn off the fgpa '''
        for idx in np.arange(8):
            self._disable_rf_neg(idx), sleep(.05)
            self._disable_rf_pos(idx), sleep(.05)
        self.printer("turned off all RF PAs")    
        self.sock_write(b"end")
        self.printer("fpga shutdown started")
        self.__del__()
    def do_lights(self):
        '''  scan through all the led colors  '''
        colors = [b'red', b'green', b'blue', b'off']
        for _ in np.arange(2):
            for col in colors:
                self.put_led(col)
                sleep(.5)

    def start(self):
        ''' run commands to start radar '''
        self.lmx.set_n(122)
        self.set_loChan(1)
        self.set_bbGain(2,15)
        
    def set_rfChannel(self,ch):
        ''' set which rf front-end turns on
        @param ch [int]: one of [1,8] to select
        '''
        self.disable_rf(self.chan)
        self.enable_rf(ch)
        self.set_bbChannel(ch)
        self.printer('set rf chan %d' % ch)
        
    def set_bbChannel(self,ch):
        ''' set the baseband mux channel select
        @param ch [int]: one of [1,8] to select
        
        HW TESTED 10/24
        '''
        self.mem.set_channel(ch)
        reg, state = self.put_reg(1, self.mem[1])
        self.printer("set bb channel to %d" % ch)
        return state
    
    def print_bbGain(self, config):
        str = "BB Gain Settings:\n"
        labs = ['MULTI', 'LEVEL']
        for id, ele in enumerate(config):
            str += labs[id] + ': %d\t' % ele
        self.printer(str)
    def get_bbGain(self):
        ''' get the baseband gain level 
        @return multi [int]: integer multipler of 0,1,2
        @return level [int]: integer for the gain level, on [0,15]
        '''
        reg_read = self.read_all_regs()  # 2d, elements are [regIdx, state_int]
        r0 = reg_read[0][1]  # reg1 setting
        r2 = reg_read[2][1]
        
        mVal = r0 & ZynqReg.REG0.BB_DENB_A + r0 & ZynqReg.REG0.BB_DENB_B
        if mVal == 0:
            multi = 0
        elif mVal == ZynqReg.REG0.BB_DENB_B:
            multi = 1
        elif mVal == ZynqReg.REG0.BB_DENB_A + ZynqReg.REG0.BB_DENB_B:
            multi = 2
            
        lVal = r0 & ZynqReg.REG0.BB_GAIN_BIT0 + r0 + ZynqReg.REG0.BB_GAIN_BIT1 \
                + r0 & ZynqReg.REG0.BB_GAIN_BIT2 + r0 & ZynqReg.REG0.BB_GAIN_BIT3
        
        if lVal == 0:
            level = 0
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0:
            level = 1
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT1:
            level = 2
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT1:
            level = 3
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT2:
            level = 4
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT2:
            level = 5
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT1 + ZynqReg.REG0.BB_GAIN_BIT2:
            level = 6
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT1 + ZynqReg.REG0.BB_GAIN_BIT2:
            level = 7
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT3:
            level = 8
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 9
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT1 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 10
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT1 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 11
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT2 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 12
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT2 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 13
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT1 + ZynqReg.REG0.BB_GAIN_BIT2 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 14
        elif lVal == ZynqReg.REG0.BB_GAIN_BIT0 + ZynqReg.REG0.BB_GAIN_BIT1 \
                + ZynqReg.REG0.BB_GAIN_BIT2 + ZynqReg.REG0.BB_GAIN_BIT3:
            level = 15
        return multi, level
    def set_bbGain(self,multi, level):
        ''' set the baseband gain level by level and multiplier
                gain = multi * code_to_db(level)
        @param multi [int]: an int of 0,1,2 
        @param level [int]: an integer for the gain code, on [0,15]
        @return state0 [int]: int values of the read-back registers 
        
        HW TESTED 10/24
        '''
        self.mem.set_bbGain(multi, level)
        reg0, state0 = self.put_reg(0, self.mem[0])
        self.printer('set bbGain (multi,level) to (%d,%d)' % (multi, level))
        return state0
        
    def set_rxAtten(self, level):
        ''' set the rx attenuator amount by level
        @param level [int]: an integer for the atten level, on [0,7], 7 is max
        @return state0 [int]: int value of the read-back registers
        '''
        self.mem.set_rxAtten(level)
        reg, state = self.put_reg(0, self.mem[0])
        self.printer('set rxAtten level to %d' % level)
        return state
    
    def print_loChan(self, config):
        self.printer("LO Channel: %d" % config)
    def get_loChan(self):
        ''' get back the current LO channel set 
        @return chan [int]: the LO channel setting 
        '''
        reg_read = self.read_all_regs()  # 2d, elements are [regIdx, state_int]
        r1 = reg_read[1][1]  # reg1 setting
        
        val = r1 & ZynqReg.REG1.LO_SW_A1 + r1 & ZynqReg.REG1.LO_SW_A2 + \
                r1 & ZynqReg.REG1.LO_SW_B1 + r1 & ZynqReg.REG1.LO_SW_B2
        
        if val == 0:
            chan = 1  # or 5
        elif val == ZynqReg.REG1.LO_SW_A2:
            chan = 2
        elif val == ZynqReg.REG1.LO_SW_B2:
            chan = 3
        elif val == ZynqReg.REG1.LO_SW_A2 + ZynqReg.REG1.LO_SW_B2:
            chan = 4
        elif val == ZynqReg.REG1.LO_SW_A1:
            chan = 6
        elif val == ZynqReg.REG1.LO_SW_B1:
            chan = 7
        elif val == ZynqReg.REG1.LO_SW_A1 + ZynqReg.REG1.LO_SW_B1:
            chan = 8
        
        return chan
        
    def set_loChan(self,chan):
        ''' set the lo sig distribution to a given 
        @param chan [int]: the desired lo channel to activate, on [1,8]
        '''
        self.mem.set_loChan(chan), sleep(.05)
        reg, state = self.put_reg(1, self.mem[1])
        self.printer('set lo channel to %d' % chan)
        return state
    
    
    # PRIVATE RF CHANNEL POWER CONTROL
    def enable_rf(self,chan):
        ''' enable the rf components on given channel
        @param chan [int]: the rf channel to enable, on [1,8]
        @return stateN, stateP [int]: int for the register state 
            after Pos then Neg step 
        '''
        if self.chan is not None:
            self.printer('turning off chan %d first' % self.chan)
            self.disable_rf(self.chan)
        if chan<1 or chan>8:
            self.printer("chan must be an int on [1,8]")
            return
        # step 1
        stateN = self._enable_rf_pos(chan-1)
        # wait 
        sleep(.1)
        # step 2
        stateP = self._enable_rf_neg(chan-1)
        # store away success
        self.chan = chan
        self.printer('enabled channel %d' % chan)
        return stateN, stateP
    
    def disable_rf(self,chan):
        ''' disable the rf components on given channel
        @chan [int]: the rf channel to disable, on [1,8]
        '''
        if self.chan is None:
            self.printer("no channel on to disable")
            return
        
        # enables/disables on reg0 and reg2
        if chan<1 or chan>8:
            self.printer("chan must be an int on [1,8]")
            return
        # step 1
        stateP = self._disable_rf_neg(chan-1)
        # wait 
        sleep(.1)   
        # step 2
        stateN = self._disable_rf_pos(chan-1)
        
        self.chan = None
        self.printer('disabled channel %d' % chan)  
        return stateN, stateP
                          
    def get_rf(self,):
        ''' get the status of the rf power registers
        @return pos_state [np.array]: 
        @return neg_state [np.array]:
        '''
        reg_read = self.read_all_regs()  # 2d, elements are [regIdx, state_int]
        
        pos_state = []
        neg_state = []
        for idx in np.arange(8):
            reg_idx, EN_CHx_POS = self.mem.ch_pos[idx]
            pos_state.append(reg_read[reg_idx][1] & EN_CHx_POS)
        for idx in np.arange(8):
            reg_idx, EN_CHx_NEG = self.mem.ch_neg[idx]
            neg_state.append(reg_read[reg_idx][1] & EN_CHx_NEG)
    
        return pos_state, neg_state
    
    def get_bookAdc(self):
        ''' poll the power detector ADC    '''
        rsp = self.sock_write(b'get adc')
        rsp_lines = rsp.split(b'\r\n')
        # throw away empty lines
        rsp_lines = [x for x in rsp_lines if len(x)>1]
        adc_value = float(rsp_lines[1].decode().split(' ')[1])
        print('got ADC of %.2f V' % adc_value)
    def get_temp(self):
        ''' poll the temperature of the FPGA
        @return temp [float]: the celcius temperature of the FPGA
        '''
        rsp = self.sock_write(b'get temp')
        rsp_lines = rsp.split(b'\r\n')
        # throw away empty lines
        rsp_lines = [x for x in rsp_lines if len(x)>1]
        temp_value = float(rsp_lines[1].decode().split(' ')[1])
        print('got temp as  %.2f deg C' % temp_value)
           
    ## ----   SUPER PRIVATE RF CHANNEL POWER CONTROL -----
    def _enable_rf_neg(self,chan):
        # enable neg
        regIdx, PinIdx = self.mem.ch_neg[chan]
        self.mem.regs[regIdx] |= PinIdx
        reg, state = self.put_reg(regIdx, self.mem[regIdx])
        return state
    def _enable_rf_pos(self,chan):
        # enable pos
        regIdx, PinIdx = self.mem.ch_pos[chan]
        self.mem.regs[regIdx] |= PinIdx
        reg, state = self.put_reg(regIdx, self.mem[regIdx])
        return state
    def _disable_rf_pos(self,chan):
        # disable pos
        regIdx, PinIdx = self.mem.ch_pos[chan]
        self.mem.regs[regIdx] &= ~PinIdx
        reg, state =self.put_reg(regIdx, self.mem[regIdx])
        return state
    def _disable_rf_neg(self,chan):
        # disable neg
        regIdx, PinIdx = self.mem.ch_neg[chan]
        self.mem.regs[regIdx] &= ~PinIdx
        reg, state = self.put_reg(regIdx, self.mem[regIdx])
        return state
    
    ## ----- Data Analysis [from DDR]-------
    
    def collect_data(self, collect_time=1, ramp_plot=False, ):
        ''' do transfer and trimming in prep for logging from the DDR
        @param collect_time [int]: length of collection in time (100 us) (div by 2 later)
        @return stacked_data [np.array]: 3 vectors of time domain data, 
            @note: the flag0/flag1 are used in FPGA to generate flag0 rise/end signals,
            which then come back to the python side as SOR, EOR (b15, b14). 
        @return N [int]: the number of samples per ramp
        '''        
        N = np.round(int(collect_time) *1e-4 *2/ self.dt)  # 2x to counteract later 1/2
        samp, dtm = self.ddr.read_samp(N)
        stacked_data = self.parse_data(samp)
        b15, b14, data14 = stacked_data

        if ramp_plot:
            t = np.arange(len(data14))*self.dt
            fig, ax = plt.subplots()
            ax.plot(t,data14, '-xb', label='raw data')
            ax.set_xlabel('time (s)')
            ax.set_ylabel('ADC Values')
            ax.tick_params(axis='y', labelcolor='b')
            ax2 = ax.twinx()
            ax2.plot(t,b15, '*r', label='flag0')
            ax2.plot(t,b14, '^m', label='flag1')
            ax2.set_ylabel('Flags')
            ax2.tick_params(axis='y', labelcolor='r')
            ax.set_title('Data and Flags Edges')
            ax.legend()
            self.printer("plotted")
        
        plt.show()

        return stacked_data, dtm, N
        
    # -----  private methods for DDR -----
    def parse_data(self, values_list):
        ''' split out the data and flags 
        @param values_list [s32 list]: the data coming from DDR
        @return b15 [np.array]: an array of 'start' pulses from the data
        @return b14 [np.array]: an array of 'end' pulses from the data
        @return data14 [np.array]: the data in 14-bit format
        @return stack_data [np.array]: the data grouped in 3 vectors
            stack_data[i][:]: the vector of interest over full time collection
        '''
        grab_data = lambda t: t & 0x3fff
        data14 = grab_data(values_list)
        data14_twos = np.array([ru.from_twos(x, 14) for x in data14])

        b15_list = []
        b14_list = []
        for itm in values_list: 
            val = ru.sign_extend16(itm)
            b15 = -(val >> 15)
            b14 = (val >> 14) & 0x01
            b15_list.append(b15)
            b14_list.append(b14)
        
        b15 = np.array(b15_list)
        b14 = np.array(b14_list)
        stacked_data = np.vstack((b15, b14, data14_twos))
        return stacked_data
    
    def save_log(self, cTime):
        ''' store away a data collection for analysis    '''
        stack_data, dtm, N = self.collect_data(cTime, ramp_plot=False)
        
        import pickle
        from datetime import datetime
        from os.path import dirname, abspath
        date_str = datetime.now().strftime('%Y_%m_%d_%H%M%S')
        homeDir = dirname(dirname(abspath(__file__)))  # /hg_radar
        datDir = os.path.join(homeDir, 'dat')
        filename = os.path.join(datDir, date_str + '.p')
        # save away results as dict date-filename
        ver = 0
        
        cdiv = self.adc.get_cdiv()
        
        ramp_settings = self.lmx.rampy.get_ramps()
            # [n_ramps, 2] - each as [end_freq (MHz), duration (us)]
        # system settings
        fs = self.Fs / cdiv / 1e6
        f1 = 2*ramp_settings[0][0]  #24.45e9  #GHz
        f0 = 2*ramp_settings[1][0]  #24.50e9  #GHz
        B = (f1 - f0)
        cf = f0 + B/2
        
        # duration of up chirp  (s)
        TRampUp = ramp_settings[0][1]  # 33e-6
        # chirp repetition period  (s)
        full_ramp_time = (ramp_settings[0][1] + ramp_settings[1][1])
    
        exp = {'data' : stack_data,         # [f0, f1, data], in time domain
                'N' : N,                    # num of actual samples in upramp
                'Tp' : full_ramp_time,      # pulse repetition interval (us)
                'ver' : ver,
                
                # extra settings for waveform
                'fs' : fs,                  # sampling freq (Mhz)
                'B'  : B,                   # freq swept over ramp (MHz)
                'cf' : cf,                  # center frequency of ramp (GHz)
                'fScale' : 1,               # scaling for digitizing
                }
        
        pickle.dump(exp, open( filename, "wb" ))
        
    
    ## ----- SOCKET LOW LEVEL METHODS  -------
    def sock_write(self,cmd):
        '''  send a command over the tcp socket, do a read to hear response
        @param cmd [str]: the command to send
        '''
        if cmd[-4:]!=b'\r\n':
            cmd += b'\r\n'
            
        self.s.sendall(cmd)
        sleep(.1)
        rsp = self.s.recv(2048)
        self.debug(rsp)
        return rsp
        
    ## CLASS LOW LEVEL METHODS    
    def user_put_reg(self, reg, hexState):  
        ''' the user puts a str hex value on one of the registers
        @param reg [int]: reg idx, one of 0-6
        @param hexState [hex str]: hex value as "0xHHHH"
        @return out_reg [int]: the register handled, in number
        @return out_state [int]: state of the register, in decimal
        '''
        if reg<0 or reg>5:
            print(b'reg must be int on [0, 5]')
            return
        if len(hexState)!=6:
            print(b'state must be a hex value as "0xHHHH"')
            return
        hexState = hexState.encode("utf-8")
        cmd = b"put reg%d %s\r\n" % (reg, hexState)
        self.debug(b"writing reg %d with %s" % (reg, hexState))
        rsp = self.sock_write(cmd)   
        out_reg, out_state = self.parse_response_verbose(rsp)
        print("  reg %d written as 0x%x" % (out_reg, out_state))
        self.mem.regs[out_reg] = out_state
        return out_reg, out_state
                
    def put_reg(self, reg, state):
        ''' put an int-encoded hex value on one of the registers
        @param reg [int]: reg idx, one of 0-5
        @param state [int]: hex value 
        @return out_reg [int]: the register handled, in number
        @return out_state [int]: state of the register, in decimal
        '''
        hexStr = ru.hexstr(state,4)
        cmd = b"put reg%d %s\r\n" % (reg, hexStr.encode())
        self.debug(b"writing reg %d with %s" % (reg, hexStr.encode()))
        rsp = self.sock_write(cmd)  
        if rsp == b'\r\n\r\n':
            # hack to handle initial reg0 empty return
            rsp = self.sock_write(cmd)
        out_reg, out_state = self.parse_response_verbose(rsp)
        self.debug(b"reg %d read as 0x%x" % (out_reg, out_state))
        self.mem.regs[out_reg] = out_state
        return out_reg, out_state
        
    def parse_response_state(self,str):
        ''' take the read response and pull out the register state 
        @param str [str]: the response from socket 
        @return state [int]: the state of the register
        '''
        reg, state = self.parse_response_verbose(str)
        return state
    
    def parse_response_verbose(self,rsp_str):
        ''' take the readback response and pull out the register state 
        @param rsp_str [str]: the response from socket 
        @return reg [int]: the register handled, in number
        @return state [int]: state of the register, in decimal
                
        # example
        str = "Put reg2 command received.
               **** Setting dio DIO GPIO (unpopulated holes on Cora).
               0x0010"
        reg, state = self.parse_response_verbose(str)
        assert reg == 2
        assert state == 0x0010
        '''
        rsp_lines = rsp_str.split(b'\r\n')
        # throw away empty lines
        rsp_lines = [x for x in rsp_lines if len(x)>1]
        try:
            reg, state = self.parse_reg(rsp_lines[1])
            return reg,state
        except IndexError:
            print('parse_response index error, probably an empty return')
    
    def parse_reg(self,reg_line):
        ''' pull out the idx and state of a reg_line
        @param reg_line [bytes]: str of 1 line with "regN 0xYYYY"
        @return state [int]: state of the register, in decimal
        @return reg [int]: the register handled, in number
        '''
        reg_idx = reg_line.find(b'reg') + 3
        reg = int(reg_line.decode('utf-8')[reg_idx])
        state = int(reg_line.decode('utf-8')[reg_idx+2:],16)
        return reg,state
        
        
    ## ------    ADMIN  --------  
    def quick_save(self,raw):
        ''' easy plot of raw data collection '''
        import pickle
        from datetime import datetime
        from os.path import dirname, abspath
        date_str = datetime.now().strftime('%Y_%m_%d_%H%M%S')
        homeDir = dirname(dirname(abspath(__file__)))  # /hg_radar
        datDir = os.path.join(homeDir, 'dat')
        filename = os.path.join(datDir, date_str + '.p')
        exp = {'data' : raw}
        pickle.dump(exp, open( filename, "wb" ))
    
    def quick_load(self):
        import fmcw_utils as fmcw
        from os.path import dirname, abspath
        homeDir = dirname(dirname(abspath(__file__)))  # /hg_radar
        datDir = os.path.join(homeDir, 'dat')
        fname = '2020_03_06_130640.p'
        filename = os.path.join(datDir, fname)
        exp = fmcw.pickle_load(filename)
        
        # cull last 50 points
        data = exp['data'][:-50]
        b15, b14, data14 = self.parse_data(data)
        ramp_valid, data, n_ramps, N = self.gather_ramps(b15, b14, data14)
      
    def debug(self,strg):
        if self._debug:
            self.printer(strg)
    def printer(self,strg):
        name = "Zynq Controller"
        print("%s: %s" % (name,strg))      
    def set_cmds(self,):
        self.cmds =  {
                    'put' : {
                            'led' : 'cycle the led\'s',
                            'REG HEX' : 'put the hex str into fpga mem reg num given',
                            }, 
                    'get' : {'opt' : 'get the fw functions',
                              'ver' : 'get the version',
                              'regs' : 'read all the registers',
                              'temp' : 'get the temp (celcius) of the FPGA',
                              'adc' : 'get the bookkeeping adc reading of the pin A4 [0,3V)',
                              'log <100us>' : 'get a N*100us worth of data, save a log',
                              'plot <100us>' : 'get N*100us worth of samples and quick plot them',
                              'bbGain' : 'get the baseband gain MULTI and LEVEL',
                              'loChan' : 'get the current LO channel',
                              },
                    'set' : {'rf CH' : 'disable active, set the given rf channel on',
                              'bb CH' : 'set the baseband mux to adc on given channel',
                              'loChan CH' : 'set the signal source to go to given channel',
                              'rxAtt LEVEL' : 'set the rx attenuation by level',
                              'bbGain MULTI LEVEL' : 'set the baseband gain with multiplier and level',
                              'enable CH' : 'set enabled channel CH, on [1,8]',
                              'disable CH' : 'set disabled channel CH, on [1,8]',
                              '_enable_neg CH' : 'set the negative voltage enabled for chan CH, on [0,7]',
                              '_enable_pos CH' : 'set the positive voltage enabled for chan CH, on [0,7]',
                              '_disable_neg CH' : 'set the negative voltage disabled for chan CH, on [0,7]',
                              '_disable_pos CH' : 'set the positive voltage disabled for chan CH, on [0,7]'
                            },
                    'lmx' : {'help' : 'get the available commands for lmx driver',
                             'help set' : 'get the range of the config\'s for set commands',
                             'help ramp' : 'get the range of the config\'s for ramp commands'
                             },
                    'adc' : {'help' : 'get the available commands for adc spi driver',
                             'help set' : 'get the range of the config\'s for set commands',
                             },
                    'ddr' : {'help' : 'get the available commands for trace buffer',
                             },
                    'reset' : 'reset all registers to defaults',
                    'start' : 'start the radar before collection',
                    'test' : {
                            'all' : 'run all submodule tests',
                            'adc' : 'run the adc regress',
                            'ddr' : 'run the ddr regression tests',
                            'gpio' : 'run the GPIO regression',
                            'lmx' : 'run the lmx controller regression',
                            },
                    'end' : 'close the radar',
                    }
        self.help = {'put' : {'REG HEX' : 'REG is fpga on [0,5], HEX is in hex',
                              },
                     'get' : {'plot <MS>' : 'MS is optionally the length to collect in ms',
                              },
                    'set' : {'rf' : 'CH on [1, 8]',
                             'bb' : 'CH on [1, 8]',
                             'loChan' : 'CH on [1, 8]',
                             'rxAtt' : 'LEVEL on [0,7]',
                             'bbGain' : 'MULTI on [0,2], LEVEL on [0,15]',
                             },                    
                    }
    def get_cmds(self,):
        print(json.dumps(self.cmds, indent=4))
    def help_cmds(self, subcmd):
        print(json.dumps(self.help[subcmd], indent=4))
    def cmd_parse(self,cmd):
        ''' parse out user given commands 
        @param cmd [str]: string input from user or super-class
        '''
        inpt = cmd.lower()
        
        ## -----  HELP CMDS -------
        if inpt.startswith("help"):
            input_list = inpt.split(" ")
            if len(input_list) == 1:
                self.get_cmds()
            elif len(input_list) == 2:
                subcmd = input_list[1]
                if subcmd == 'lmx':
                    self.lmx.get_cmds()
                elif subcmd == 'adc':
                    self.adc.get_cmds()
                elif subcmd == 'ddr':
                    self.ddr.get_cmds()
                else:
                    try:
                        self.help_cmds(subcmd)
                    except:
                        print('Bad help command:  %s' % subcmd)
            elif len(input_list) >= 2:
                print('too complicated, not implemented')
                self.get_cmds()
        elif inpt == "reset":
            self.reset_regs()  
        elif inpt == "start":
            self.start()
        elif inpt == "end":
            self.turn_off()
            exit()
        # sub command types
        elif inpt.startswith("put"):
            inpt_list = inpt.split(" ")
            subcmd = inpt_list[1]
            if subcmd == 'led' or subcmd == 'leds':
                self.do_lights()
            elif int(subcmd) >= 0 and int(subcmd) <= 5:
                regStr = inpt_list[2]
                self.user_put_reg(int(subcmd), regStr)
            else:
                self.printer("Bad PUT command:  %s" % inpt)
        ## ---- SET CMDS -------
        elif inpt.startswith("set"):
            inpt_list = inpt.split(" ")
            subcmd = inpt_list[1]
            if subcmd == "rf":
                try: 
                    chan = int(inpt_list[2])
                except:
                    self.printer("Bad SET command:  %s" % inpt)
                self.set_rfChannel(chan)
            elif subcmd == "bb" or subcmd == "bbchan":
                try: 
                    chan = int(inpt_list[2])
                except:
                    self.printer("Bad SET command:  %s" % inpt)
                self.set_bbChannel(chan)
            elif subcmd == "lochan":
                try: 
                    chan = int(inpt_list[2])
                except:
                    self.printer("Bad SET command:  %s" % inpt)
                self.set_loChan(chan)
            elif subcmd == "rxatt":
                try: 
                    level = int(inpt_list[2])
                except:
                    self.printer("Bad SET command:  %s" % inpt)
                self.set_rxAtten(level)
            elif subcmd == "bbgain":
                try: 
                    multi = int(inpt_list[2])
                    level = int(inpt_list[3])
                except:
                    self.printer("Bad SET command:  %s" % inpt)                        
                self.set_bbGain(multi, level)
            elif subcmd == '_enable_neg':
                num = int(inpt_list[2])
                self._enable_rf_neg(num)
            elif subcmd == '_enable_pos':
                num = int(inpt_list[2])
                self._enable_rf_pos(num)
            elif subcmd == '_disable_neg':
                num = int(inpt_list[2])
                self._disable_rf_neg(num)  
            elif subcmd == '_disable_pos':
                num = int(inpt_list[2])
                self._disable_rf_pos(num)
            elif subcmd == 'enable':
                ch = int(inpt_list[2])
                self.enable_rf(ch)
            elif subcmd == 'disable':
                ch = int(inpt_list[2])
                self.disable_rf(ch)
            else:
                self.printer("Bad SET command: %s" % subcmd)
        ## -----   GET CMDS -------
        elif inpt.startswith("get"):
            inpt_list = inpt.split(" ")
            subcmd = inpt_list[1]
            if subcmd == "opt":
                self.get_options()
            elif subcmd == "ver":
                self.get_hw_tag()
                self.printer("python ver %s" % self.ver)
            elif subcmd == "regs":
                self.read_all_regs()
                self.printer('registers read')
            elif subcmd == "adc":
                self.get_bookAdc()
            elif subcmd == "temp":
                self.get_temp()
            elif subcmd == "log":
                if len(inpt_list)==3:
                    c_time = inpt_list[2]  # in 100us
                else:
                    c_time = 5 # default 
                self.save_log(c_time)
                self.printer("saved log")
            elif subcmd == 'plot':        
                if len(inpt_list)==3:
                    c_time = inpt_list[2]  # in 100us 
                else:
                    c_time = 3 # default 
                self.collect_data(c_time, ramp_plot=True)
            elif subcmd == 'bbgain':
                cfg = self.get_bbGain()
                self.print_bbGain(cfg)
            elif subcmd == 'lochan':
                cfg = self.get_loChan()
                self.print_loChan(cfg)
            else:
                self.printer("Bad GET command:  %s" % subcmd)
        ## ---- TEST Commands -----
        elif inpt.startswith('test'):
            inpt_list = inpt.split(" ")
            subcmd = inpt_list[1]
            if subcmd == "lmx":
                rt.test_lmx(self)
            elif subcmd == 'adc':
                rt.test_adc(rad)
            elif subcmd == "gpio":
                rt.test_gpio(rad)
            elif subcmd == 'ddr':
                rt.test_bram(rad)
            elif subcmd == 'all':
                rt.test_all(rad)
            else:
                self.printer("Bad TEST command:  %s" % subcmd)
        ##  ----  Sub Class Commands ------
        elif inpt.startswith('lmx'):
            cmd = inpt[4:]  # grab rest of input
            self.lmx.cmd_parse(cmd)
        elif inpt.startswith('adc'):
            cmd = inpt[4:]
            self.adc.cmd_parse(cmd)
        elif inpt.startswith('ddr'):
            cmd = inpt[3:]
            self.ddr.cmd_parse(cmd)
        else:
            self.printer("Bad command:  %s" % inpt)



if __name__ == '__main__':
    
    print("radar.py startup\n")
    
    # edit IP address in Command Line Arg "-i #"

    # les network:
    # netgear33 
    # pw:  strongbox791
    parser = argparse.ArgumentParser(description=''' 
    This script is a controller for the HG radar.   
    it can be made to execute, or can interactively with user input''')

    # ACTIONS
    parser.add_argument('-u','--user', action='store_true', default=False,
                        help='run script in user interactive mode')
    parser.add_argument('-i','--ip', action='store', default=10,
                        help='the address of zynq ip (last value)')
    parser.add_argument('-t','--test', action='store_true', default=False,
                        help='run test methods to validate FW')
    parser.add_argument('-s','--sim', action='store_true', default=False,
                    help='run lmx in sim mode')
    parser.add_argument('-p','--playback', action='store_true', default=False,
                    help='run playback of stored data')
    parser.add_argument('-d','--debug', action='store_true', default=False,
                help='run in debug mode')
    
    # get arguments
    args = parser.parse_args(sys.argv[1:])
    
    dflt_ip = "192.168.1."  + str(args.ip)
    rad = Zynq(dflt_ip, args.sim, args.debug, args.playback)
    
    
    if args.user:
        # USER INTERACTIVE MODE
        prompt = "> Enter a command to continue, use 'help' for options\n"
        inpt = input(prompt).lower()
        while True:
            # ask for user input again
            rad.cmd_parse(inpt)                
            inpt = input(prompt).lower()

    
    #### FW CHANGE LOG ####   
    # 1.39 - built from source on build.sh 

    ###### TESTS  ########
    if args.test:
#         rt.test_all(rad)
#         rt.test_gpio(rad)
#         rt.test_bram(rad)
        
#         rt.test_lmx(rad)
        rt.test_adc(rad)

    
    print("\nradar.py shutdown")
    exit()