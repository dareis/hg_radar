
''' helper functions for dealing with registers

11/2018
Darren Reis

'''

import math
import numpy as np

def to_bits(number, size=8):
    ''' convert a number to a bit array
    @param number [int]: number to convert, in range [0, 2**size]
    @param size [int]: number of bits used to represent
    @return bits [array]: array of integers representing the number
    '''
    if type(number)==bytes:
        number = int(number)
                     
    if number < 0 or number > math.pow(2, size):
        raise ValueError(
            "Number provided must be in range: [0, {}], was: {}".format(int(math.pow(2, size)), number))

    bits = []
    for bit in format(number, "0{}b".format(size)):
        bits.insert(0, int(bit))

    return np.array(bits)


def to_int(bits):
    ''' convert a bit array into an integer
    @param bits [array]: array of bits to turn into an integer
    @return int:  integer value of array
    '''
    out = 0
    l = len(bits)
    for bit in reversed(bits):
        out = (out << 1) | bit

    return out

def to_bytes(val):
    ''' convert a bit array into a bytes value
    @param val [array]: array of bits to turn into an integer
        OR
           val [int]: an int value to turn into bytes nibbles
    @return bValue [bytes]:  a bytes value of the array
    '''
    if type(val) == list or type(val) == np.ndarray:
        iValue = to_int(val)
    else:
        iValue = val
    bValue = bytes_pad(iValue)
    return bValue

def bytes_pad(int):
    return b"%02x" % int

def to_twos_comp_int(bits):
    ''' convert bit array to 2's complement form of integer 
    @param bits [array]: bit array to convert
    @return int: integer result
    '''
    bits_str = ""

    for b in reversed(bits):
        bits_str += str(b)

    size = len(bits)
    v = int(bits_str, 2)

    if (v & (1 << (size - 1))) != 0:
        v = v - (1 << size)

    return v

def to_twos(val, n):
    ''' convert to twos complement
    @param val [int]: the value to convert
    @param n [int]: the number of bits
    @return val [int]: the number in n-bit two's complement binary
    '''
    limL = -(1 << (n-1))
    limH = (1 << (n-1)) -1
    if val < limL or val > limH:
        print('error: twos complement can only represent on [%d,%d]' % (limL, limH))
        return 
    if val < 0:
        val = (1 << n) + val
    else:
        if (val & (1 << (n - 1))) != 0:
            # if neg number, i.e. sign bit is set
            val = val - (1 << n)
    return val

def sign_extend16(value):
    ''' do sign extension to 16th place '''
    return (value & (0x7fff)) - (value & 0x8000)

def from_twos(val,n):
    ''' convert back from two's complement
    @param val [int]: the int number in n-bit two's comp
    @return val [int]: the int value
    '''
    limL = 0
    limH = (1 << n) -1
    if val < limL or val > limH:
        print('error: twos complement can only represent on [%d,%d]' % (limL, limH))
        return 
    if val > int((1<<n)/2):
        val = val - (1<<n)
    return val
        
def check_paren(my_string): 
    ''' check for balanced parenthesis in input 
    @return bool [bool]: result of is_balanced
    '''
    brackets = ['()', '{}', '[]'] 
    while any(x in my_string for x in brackets): 
        for br in brackets: 
            my_string = my_string.replace(br, '') 
    return not my_string 

def num_bytes_for_bits(bits):
    ''' calculate the min number of bytes needed to store a number of bits
    @param bits [int]: the number of bits
    @return size [int]: the min number of bytes needed for storage
    '''
    return int(math.ceil(float(bits) / 8.0))



def testBit(int_type, offset):
    ''' returns status of nth bit 
    @param int_type [int]: the variable to test
    @param offset [int]: the place of the testing bit
    @return bit_status [int]: the status of the that bit
    '''
    mask = 1 << offset
    bit_status = int_type & mask
    return(bit_status)


def setBit(int_type, offset):
    ''' turns on the nth bit 
    @param int_type [int]: the variable to modify
    @param offset [int]: the place of the bit
    @return output [int]: the result with nth bit as 1
    '''
    mask = 1 << offset
    output = int_type | mask
    return(output)

def clearBit(int_type, offset):
    ''' clears the nth bit
    @param int_type [int]: the variable to modify
    @param offset [int]: the place of the bit
    @return output [int]: the result with nth bit as 0
    '''
    mask = ~(1 << offset)
    output = int_type & mask
    return(output)

def clearBits(int_type, mask):
    ''' clears the mask 
    @param int_type [int]: the variable to modify
    @param offset [int]: the mask to clear
    @return output [int]: the result with bits as 0
    '''
    output = int_type & ~mask
    return(output)

def toggleBit(int_type, offset):
    '''
    @param int_type [int]: the variable to modify
    @param offset [int]: the place of the bit
    @return output [int]: the result with nth bit flipped
    '''
    mask = 1 << offset
    output = int_type ^ mask
    return(output)

def bytesListToBytes(bList):
    ''' convert a bytes list to a single bytes value joined together
    @param bList [list of bytes]: the list to join
    @return bytesL [bytes]: the single bytes value concatenated
    '''
    return b''.join(bList)

def load_reg_map(fname, go_fast=False):
    ''' take .txt register map and create stream setup 
    @param fname [str]: file name to load
    @param go_fast [bool]: use the speed load format
    @return reg_stream [bytes]: bytes-like string of for registers like
        AddrN DataN DataN-1 DataN-2 ...  Data1
    '''
    reg_list = []
    reg_dict = {}
    with open(fname, 'rb') as file_object:

        line = file_object.readline()
        line_count = 1
        
        while line:
            rIdx = int(line.split(b'\t')[0][1:])
            hex_val = line.split(b'x')[1][0:6]

            if line_count == 1:
                reg = hex_val[2:]       # grab 4 hex (address and vals)
            else:
                if go_fast:
                    reg = hex_val[4:]   # grab 2 hex (vals only)
                else:
                    reg = hex_val[2:]   # grab 4 hex (address and vals)
            int_val = int(reg,16)   
            reg_list.append(reg)
            reg_dict.__setitem__(rIdx, int_val)
            line = file_object.readline()
            line_count += 1
        
    return b''.join(reg_list)
        
def hexstr(int_type,digits):
    '''
    output a hex string with n places
    @int_type [int]: the variable
    @digits [int]: the number of places in the hex
    '''
    return('0x' + hex(int_type)[2:].zfill(digits))


def ndprint(a, format_string ='{0:.2f}'):
    print([format_string.format(v,i) for i,v in enumerate(a)])


def find_zcrossings(data,do_rising=True):
    ''' find positive or negative zero crossings in np.array
    @param data [np.array]: the signal to scan over
    @param do_rising [bool]: true for positive cross, false for negative cross
    @return edges [np.array]: summary array of zero crossings in correct sign transmission
    
    >>> find_zcrossing(np.array([-2, 1, 0, -2, 1, -2, 3]))
    [0,3,5]
    >>> find_zcrossing(np.array([-2, 1, 0, -2, 1, -2, 3]), False)
    [1,4] 
    '''
    if do_rising != True and do_rising != False:
        print('find_zcrossing error: do_rising must be bool, given %d' % do_rising)
        return 
    if do_rising:
        edges = np.where(np.diff(data)>0)[0]
    else:
        edges = np.where(np.diff(data)<0)[0]
    return edges
    
def get_array_from_ids(array,ids):
    ''' grab the elements from array based on the given indexes
    @note custom function version of     array[ids]
        used to protect against grabbing out-of-bounds id   
    '''
    # dump the out of bounds ids
    good_ids = [x for x in ids if x<np.shape(array)[0]]
    return array[good_ids]   
    
if __name__ == '__main__':
    
    print("testing util functions")
    
    print(to_twos(130, 8))