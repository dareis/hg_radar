'''
Created on Oct 1, 2019

regression tests for different parts of the radar fw

@author: Darren
'''

import numpy as np
from time import sleep
import matplotlib.pyplot as plt
    
    
# hg imports
from zynq_registers import ZynqReg
import utils as ru


''' @TODO: install intentially failing tests for each feature '''


## ----- TEST ROUTINE  -------

def test_all(rad):
    test_gpio(rad)
    test_bram(rad)
    test_lmx(rad)
    test_adc(rad)
#     test_shutdown(rad)
    

def test_shutdown(rad):
    # @TODO: make shutdown test
    print("todo finish shutdown test")

## ------  GPIO TESTS  -------

def test_gpio(rad):
    # 
#         test_hello_world(rad)
        test_lo_channel(rad)
        test_rxAtten(rad)
        test_bb_channel(rad)
        test_read_reset(rad)
        test_user_put(rad)   
        test_bbGain(rad)
        test_get_rf(rad)
        test_power_rf(rad)  # takes 60 seconds

def test_hello_world(rad):
    rad.get_options()
    sleep(1)
    rad.get_hw_tag()
    sleep(1)
    rad.do_lights()
    sleep(1)
    print('passed hello world test')
    
def test_user_put(rad):
    task = [[0, b'0x1112'], [1, b'0x10f1'], [2, b'0x00c2'], [3, b'0x0003'],
            [4, b'0x0004'], [5, b'0x0002']]
    
    for j in np.arange(len(task)):
        r, t = rad.user_put_reg(task[j][0], task[j][1])
        assert r == task[j][0] and t == int(task[j][1],16), "failed register I/O test, reg%s" % j
    print('passed all user put tests')

    
def test_read_reset(rad):
    rad.reset_regs()
    print('success reset, write-all')
    dflt_settings = rad.default
    outList = rad.read_all_regs()
    for idx, ele in enumerate(outList):
        assert dflt_settings[idx][0] == outList[idx][0], "failed reset test, reg%s.0" % idx
        assert dflt_settings[idx][1] == outList[idx][1], "failed reset test, reg%s.1" % idx
    print('passed read_tbState/reset test')
    
def test_bbGain(rad):
    ''' 3 test to be sure the bbGain function works '''
    
   
    dflt_denB = np.array([ZynqReg.REG0.BB_DENB_B, ZynqReg.REG0.BB_DENB_A])
    dflt_bits = np.array([ZynqReg.REG0.BB_GAIN_BIT0,
                          ZynqReg.REG0.BB_GAIN_BIT1,
                          ZynqReg.REG0.BB_GAIN_BIT2,
                          ZynqReg.REG0.BB_GAIN_BIT3])
    
    # tests
    test_input = [2, 6]       # multi, level
    true_denB = np.array([1,1])*dflt_denB
    true_bits = np.array([0, 1, 0, 0])*dflt_bits 
    state0 = rad.set_bbGain(test_input[0], test_input[1])

    denB = np.array([state0 & ZynqReg.REG0.BB_DENB_B,
                        state0 & ZynqReg.REG0.BB_DENB_A])
    bits = np.array([state0 & ZynqReg.REG0.BB_GAIN_BIT0,
                        state0 & ZynqReg.REG0.BB_GAIN_BIT1,
                        state0 & ZynqReg.REG0.BB_GAIN_BIT2,
                        state0 & ZynqReg.REG0.BB_GAIN_BIT3])
    assert np.array_equal(true_denB,denB), "failed bbGain test 1, wrong multi, %s != %s" % (str(true_denB), str(denB))
    assert np.array_equal(true_bits,bits), "failed bbGain test 1, wrong bits, %s != %s" % (str(true_bits), str(bits))
    
    test_inputB = [1, 1]
    true_denB2 = np.array([1, 0])*dflt_denB
    true_bits2 = np.array([1, 0, 0, 0])*dflt_bits
    state0_2 = rad.set_bbGain(test_inputB[0], test_inputB[1])
    
    denB2 = np.array([state0_2 & ZynqReg.REG0.BB_DENB_B,
                       state0_2 & ZynqReg.REG0.BB_DENB_A])
    bits2 = np.array([state0_2 & ZynqReg.REG0.BB_GAIN_BIT0,
            state0_2 & ZynqReg.REG0.BB_GAIN_BIT1,
            state0_2 & ZynqReg.REG0.BB_GAIN_BIT2,
            state0_2 & ZynqReg.REG0.BB_GAIN_BIT3])
    assert np.array_equal(true_denB2,denB2), "failed bbGain test 2, wrong multi, %s != %s" % (str(true_denB2), str(denB2))
    assert np.array_equal(true_bits2,bits2), "failed bbGain test 2, wrong bits, %s != %s" % (str(true_bits2), str(bits))
    
    test_input3 = [0, 3]
    true_denB3 = np.array([0, 0])*dflt_denB
    true_bits3 = np.array([1, 1, 0, 0])*dflt_bits
    state0_3 = rad.set_bbGain(test_input3[0], test_input3[1])
    
    denB3 = np.array([state0_3 & ZynqReg.REG0.BB_DENB_B,
                       state0_3 & ZynqReg.REG0.BB_DENB_A])
    bits3 = np.array([state0_3 & ZynqReg.REG0.BB_GAIN_BIT0,
            state0_3 & ZynqReg.REG0.BB_GAIN_BIT1,
            state0_3 & ZynqReg.REG0.BB_GAIN_BIT2,
            state0_3 & ZynqReg.REG0.BB_GAIN_BIT3])
    assert np.array_equal(true_denB3,denB3), "failed bbGain test 3, wrong multi"
    assert np.array_equal(true_bits3,bits3), "failed bbGain test 3, wrong bits"

    
    test_input4 = [0, 3]
    _ = rad.set_bbGain(*test_input4)
    output = rad.get_bbGain()
    assert np.array_equal(test_input4, output), "failed new get bbGain test 4, " \
        + "%s != %s" % (str(output), str(test_input4))
    print('passed bbGain test')
    
    
def test_rxAtten(rad):
    ''' 3 test to verify the rx attenuation works '''
    dflt_bits = np.array([ZynqReg.REG0.RX_ATTEN1_A0,
                            ZynqReg.REG0.RX_ATTEN1_A1,
                            ZynqReg.REG0.RX_ATTEN1_A2,
                            ZynqReg.REG0.RX_ATTEN2_A0,
                            ZynqReg.REG0.RX_ATTEN2_A1,
                            ZynqReg.REG0.RX_ATTEN2_A2])
    
    
    # tests
    test_input = [6]       # level
    true_bits = np.array([1, 0, 1, 0, 1, 1])*dflt_bits
    state0 = rad.set_rxAtten(test_input[0])
    
    bits = np.array([state0 & ZynqReg.REG0.RX_ATTEN1_A0,
            state0 & ZynqReg.REG0.RX_ATTEN1_A1,
            state0 & ZynqReg.REG0.RX_ATTEN1_A2,
            state0 & ZynqReg.REG0.RX_ATTEN2_A0,
            state0 & ZynqReg.REG0.RX_ATTEN2_A1,
            state0 & ZynqReg.REG0.RX_ATTEN2_A2])
    assert np.array_equal(true_bits,bits), "failed rxAtten test 1, %s != %s" % (str(true_bits), str(bits))
    
    test_input2 = [2]       # level
    true_bits2 = np.array([1, 1, 1, 0, 0, 0])*dflt_bits
    state02 = rad.set_rxAtten(test_input2[0])
     
    bits2 = np.array([state02 & ZynqReg.REG0.RX_ATTEN1_A0,
            state02 & ZynqReg.REG0.RX_ATTEN1_A1,
            state02 & ZynqReg.REG0.RX_ATTEN1_A2,
            state02 & ZynqReg.REG0.RX_ATTEN2_A0,
            state02 & ZynqReg.REG0.RX_ATTEN2_A1,
            state02 & ZynqReg.REG0.RX_ATTEN2_A2])
    assert np.array_equal(true_bits2,bits2), "failed rxAtten test 2, %s != %s" % (str(true_bits2), str(bits2))
    
    test_input3 = [0]       # level
    true_bits3 = np.array([0, 0, 0, 0, 0, 0])*dflt_bits
    state03 = rad.set_rxAtten(test_input3[0])
    
    bits3 = np.array([state03 & ZynqReg.REG0.RX_ATTEN1_A0,
            state03 & ZynqReg.REG0.RX_ATTEN1_A1,
            state03 & ZynqReg.REG0.RX_ATTEN1_A2,
            state03 & ZynqReg.REG0.RX_ATTEN2_A0,
            state03 & ZynqReg.REG0.RX_ATTEN2_A1,
            state03 & ZynqReg.REG0.RX_ATTEN2_A2])
    assert np.array_equal(true_bits3,bits3), "failed rxAtten test 3, %s != %s" % (str(true_bits3), str(bits3))    
    print('passed rxAtten test')

def test_bb_channel(rad):
    dflt_bits = np.array([ZynqReg.REG1.CH_SEL_A2,
                          ZynqReg.REG1.CH_SEL_A1,
                          ZynqReg.REG1.CH_SEL_A0])
    test_input = 3
    true_bits = np.array([0, 1, 0])*dflt_bits
    state = rad.set_bbChannel(test_input)
    bits = np.array([state & ZynqReg.REG1.CH_SEL_A2,
            state & ZynqReg.REG1.CH_SEL_A1,
            state & ZynqReg.REG1.CH_SEL_A0])
    assert np.array_equal(true_bits, bits), "failed set_bb_channel test 1, %s != %s" % (str(true_bits), str(bits))
    
    test_input2 = 6
    true_bits2 = np.array([1, 1, 0])*dflt_bits
    state2 = rad.set_bbChannel(test_input2)
    bits2 = np.array([state2 & ZynqReg.REG1.CH_SEL_A2,
            state2 & ZynqReg.REG1.CH_SEL_A1,
            state2 & ZynqReg.REG1.CH_SEL_A0])
    assert np.array_equal(true_bits2, bits2), "failed set_bb_channel test 2, %s != %s" % (str(true_bits2), str(bits2))    
    
    test_input3 = 1
    true_bits3 = np.array([0, 0, 0])*dflt_bits
    state3 = rad.set_bbChannel(test_input3)
    bits3 = np.array([state3 & ZynqReg.REG1.CH_SEL_A2,
            state3 & ZynqReg.REG1.CH_SEL_A1,
            state3 & ZynqReg.REG1.CH_SEL_A0])
    assert np.array_equal(true_bits3, bits3), "failed set_bb_channel test 3, %s != %s" % (str(true_bits3), str(bits3))    
    
    test_input4 = 5
    true_bits4 = np.array([1, 1, 1])*dflt_bits
    state4 = rad.set_bbChannel(test_input4)
    bits4 = np.array([state4 & ZynqReg.REG1.CH_SEL_A2,
            state4 & ZynqReg.REG1.CH_SEL_A1,
            state4 & ZynqReg.REG1.CH_SEL_A0])
    assert np.array_equal(true_bits4, bits4), "failed set_bb_channel test 4, %s != %s" % (str(true_bits4), str(bits4))
    print('passed set_bb_channel test')
    


def test_lo_channel(rad):
    dflt_bits = np.array([ZynqReg.REG1.LO_SW_A1,
                          ZynqReg.REG1.LO_SW_B1,
                          ZynqReg.REG1.LO_SW_A2,
                          ZynqReg.REG1.LO_SW_B2])
    
    test_input = 3
    true_bits = np.array([0, 0, 0, 1])*dflt_bits
    state = rad.set_loChan(test_input)
    bits = np.array([state & ZynqReg.REG1.LO_SW_A1,
                     state & ZynqReg.REG1.LO_SW_B1,
                     state & ZynqReg.REG1.LO_SW_A2,
                     state & ZynqReg.REG1.LO_SW_B2])
    assert np.array_equal(true_bits, bits), "failed lo_channel test 1, %s != %s" % (str(true_bits), str(bits))
    
    test_input2 = 5
    true_bits2 = np.array([0, 0, 0, 0])*dflt_bits
    state2 = rad.set_loChan(test_input2)
    bits2 = np.array([state2 & ZynqReg.REG1.LO_SW_A1,
                     state2 & ZynqReg.REG1.LO_SW_B1,
                     state2 & ZynqReg.REG1.LO_SW_A2,
                     state2 & ZynqReg.REG1.LO_SW_B2])
    assert np.array_equal(true_bits2, bits2), "failed lo_channel test 2, %s != %s" % (str(true_bits2), str(bits2))
    
    test_input3 = 8
    true_bits3 = np.array([1, 1, 0, 0])*dflt_bits
    state3 = rad.set_loChan(test_input3)
    bits3 = np.array([state3 & ZynqReg.REG1.LO_SW_A1,
                     state3 & ZynqReg.REG1.LO_SW_B1,
                     state3 & ZynqReg.REG1.LO_SW_A2,
                     state3 & ZynqReg.REG1.LO_SW_B2])
    assert np.array_equal(true_bits3, bits3), "failed lo_channel test 3, %s != %s" % (str(true_bits3), str(bits3))
    
    test_input4 = 3
    _ = rad.set_loChan(test_input4)
    output = rad.get_loChan()
    assert test_input4==output, "failed lo_channel test 4, %d != %d" % (test_input4, output)
    
    print('passed set_loChan test')

def test_get_rf(rad):
    ''' test readout of the rf power enable registers '''
    dflt_pos_bits = np.array([ZynqReg.REG2.EN_CH1_POS,
                              ZynqReg.REG0.EN_CH2_POS,
                              ZynqReg.REG2.EN_CH3_POS,
                              ZynqReg.REG2.EN_CH4_POS,
                              ZynqReg.REG2.EN_CH5_POS,
                              ZynqReg.REG2.EN_CH6_POS,
                              ZynqReg.REG2.EN_CH7_POS,
                              ZynqReg.REG0.EN_CH8_POS])
    dflt_neg_bits = np.array([ZynqReg.REG2.EN_CH1_NEG,
                              ZynqReg.REG0.EN_CH2_NEG,
                              ZynqReg.REG2.EN_CH3_NEG,
                              ZynqReg.REG2.EN_CH4_NEG,
                              ZynqReg.REG2.EN_CH5_NEG,
                              ZynqReg.REG2.EN_CH6_NEG,
                              ZynqReg.REG2.EN_CH7_NEG,
                              ZynqReg.REG0.EN_CH8_NEG])
    
    
    reg0 = ZynqReg.REG0.EN_CH8_NEG | ZynqReg.REG0.EN_CH2_NEG | ZynqReg.REG0.EN_CH2_POS | ZynqReg.REG0.EN_CH8_POS
    reg2 = ZynqReg.REG2.EN_CH1_NEG | ZynqReg.REG2.EN_CH1_POS | ZynqReg.REG2.EN_CH3_NEG | ZynqReg.REG2.EN_CH3_POS | ZynqReg.REG2.EN_CH4_NEG | ZynqReg.REG2.EN_CH4_POS | ZynqReg.REG2.EN_CH5_NEG | ZynqReg.REG2.EN_CH5_POS | ZynqReg.REG2.EN_CH6_NEG | ZynqReg.REG2.EN_CH6_POS | ZynqReg.REG2.EN_CH7_NEG | ZynqReg.REG2.EN_CH7_POS
    
    rad.put_reg(0, reg0)
    rad.put_reg(2, reg2)
    
    f = np.vectorize(int)
    true_pos = f(np.ones([8])*dflt_pos_bits)
    true_neg = f(np.ones_like(true_pos)*dflt_neg_bits)
    pwr_pos, pwr_neg = rad.get_rf() 
    assert np.array_equal(true_pos, pwr_pos), "Failed get_rf for pos, %s != %s" % (str(true_pos), str(pwr_pos))
    assert np.array_equal(true_neg, pwr_neg), "Failed get_rf for neg, %s != %s" % (str(true_neg), str(pwr_neg))
    print('passed get_rf test') 
    

def test_power_rf(rad):
    ''' test the controls for powering the RF power amps '''
    
    dflt_pos_bits = np.array([ZynqReg.REG2.EN_CH1_POS,
                              ZynqReg.REG0.EN_CH2_POS,
                              ZynqReg.REG2.EN_CH3_POS,
                              ZynqReg.REG2.EN_CH4_POS,
                              ZynqReg.REG2.EN_CH5_POS,
                              ZynqReg.REG2.EN_CH6_POS,
                              ZynqReg.REG2.EN_CH7_POS,
                              ZynqReg.REG0.EN_CH8_POS])
    dflt_neg_bits = np.array([ZynqReg.REG2.EN_CH1_NEG,
                              ZynqReg.REG0.EN_CH2_NEG,
                              ZynqReg.REG2.EN_CH3_NEG,
                              ZynqReg.REG2.EN_CH4_NEG,
                              ZynqReg.REG2.EN_CH5_NEG,
                              ZynqReg.REG2.EN_CH6_NEG,
                              ZynqReg.REG2.EN_CH7_NEG,
                              ZynqReg.REG0.EN_CH8_NEG])
    
    rad.reset_regs()
    for test_idx in np.arange(8):
        
        # test private methods for enable and disable on positive side
        true_bits = np.zeros_like(dflt_pos_bits)
        true_bits[test_idx] = 1
        
        # form the desired result
        true_bits_pos = true_bits*dflt_pos_bits
        true_bits_neg = true_bits*dflt_neg_bits
        
        # try private enable pos, then read_tbState back state
        _ = rad._enable_rf_pos(test_idx)
        bits_pos, _ = rad.get_rf()
        assert np.array_equal(true_bits_pos, bits_pos), \
            "failed _enable_rf_pos on chan %d\n%s != %s " % (test_idx,
                                                              str(true_bits_pos),
                                                               str(bits_pos))
        
        # then try to disable again, read_tbState back state
        off_bits_pos = np.zeros_like(dflt_pos_bits)
        _ = rad._disable_rf_pos(test_idx)
        bits_pos, _ = rad.get_rf()
        assert np.array_equal(off_bits_pos, bits_pos), \
                "failed _disable_rf_pos on chan %d\n%s != %s" % (test_idx,
                                                                  str(off_bits_pos),
                                                                  str(bits_pos))
        
        
        # try private enable neg, then read_tbState back state
        _ = rad._enable_rf_neg(test_idx)
        _, bits_neg = rad.get_rf()
        assert np.array_equal(true_bits_neg, bits_neg), \
                "failed _enable_rf_neg on chan %d\n%s != %s" % (test_idx,
                                                                 str(true_bits_neg),
                                                                 str(bits_neg))
        
        # now try to disable, then read_tbState back state
        off_bits_neg = np.zeros_like(dflt_neg_bits)
        _ = rad._disable_rf_neg(test_idx)
        _, bits_neg = rad.get_rf()
        assert np.array_equal(off_bits_neg, bits_neg), \
                "failed _disable_rf_neg on chan %d\n%s != %s" % (test_idx,
                                                                 str(off_bits_neg),
                                                                 str(bits_neg))
        
        print('passed rf power tests for channel %d' % test_idx)
        
def test_set_rf(rad):
    ''' test the entire rf chain '''
    # @TODO: finish this
    return

## ----   LMX TESTS -------------


SLP = .1  # sleep between write/read_tbState
def test_lmx(rad):

    # passed 2/14/20
#     test_read_reg0(rad)
#     test_read_r31(rad)
#     test_set_charge_pump(rad)
#     test_set_n(rad)
#     test_set_frac_num(rad)
#     test_set_frac_den(rad)
#     test_set_pll_r(rad)
#     test_set_cp_thresh(rad)
#     test_set_mux(rad)
#     test_set_others(rad)
#     test_set_fastlock(rad)
#     test_set_dld(rad)
#     test_ramp_pwr(rad)
#     sleep(.1)
#     test_ramp_general(rad)
#     test_ramp_trig(rad)
#     test_ramp_limit(rad)
#     test_ramp_reg(rad)
#     test_ramp_comp(rad)   

    # rampy  - WORKING
#     test_read_ramps(rad)
#     test_calc_ramps(rad)

    # some problem
    test_ramp_triangle(rad)  
    rad.lmx.test_ramp_precise()     

    


def test_set_n(rad):
    ''' test for the n config '''
    lmx = rad.lmx
    
    max_addr = 18
    n_read =  3
    # VAL on [0, 262143]
    test_input = [0, 100, 120, 5000, 262143]
    
    for idx, inpt in enumerate(test_input):
        print('testing n %d of %d' % (idx, len(test_input)))
        lmx.set_n(inpt)
        sleep(3*SLP)
        r_addr, n = lmx.get_n()
        assert r_addr == max_addr, "failed test_set_n case %d.0, %d != %d" % (idx, r_addr, max_addr)
        assert inpt == n, "failed test_set_n case %d.1, %d != %d" % (idx, inpt, n)
        print('passed test_set_n case %d' % idx)
        sleep(SLP)
    print('passed set_n')

def test_set_frac_num(rad):
    ''' test for the NUM config '''
    lmx = rad.lmx
    
    max_addr = 21
    n_read =  3
    # NUM on [0, 16777215]
    test_input = [0, 100, 5000, 262143, 16777215]
    
    for idx, inpt in enumerate(test_input):
        print('testing frac_num %d of %d' % (idx, len(test_input)))
        lmx.set_frac_num(inpt)
        sleep(SLP)
        r_addr, num = lmx.get_frac_num()
        assert r_addr == max_addr, "failed test_set_frac_num case %d.0, %d != %d" % (idx, r_addr, max_addr)
        assert inpt == num, "failed test_set_frac_num case %d.1, %d != %d" % (idx, inpt, num)
        print('passed test_set_frac_num case %d' % idx)
        sleep(SLP)
    print('passed set_frac_num')

def test_set_frac_den(rad):
    ''' test for the DEN config '''
    lmx = rad.lmx
    
    max_addr = 24
    n_read =  3
    # DEN on [0, 16777215]
    test_input = [0, 100, 5000, 262143, 16777215]
    
    for idx, inpt in enumerate(test_input):
        print('testing frac_den %d of %d' % (idx, len(test_input)))
        lmx.set_frac_den(inpt)
        sleep(SLP)
        r_addr, den = lmx.get_frac_den()
        assert r_addr == max_addr, "failed test_set_frac_den case %d.0, %d != %d" % (idx, r_addr, max_addr)
        assert inpt == den, "failed test_set_frac_den case %d.1, %d != %d" % (idx, inpt, den)
        print('passed test_set_frac_den case %d' % idx)
        sleep(SLP)
    print('passed set_frac_den')

def test_set_pll_r(rad):
    ''' test for the R config '''
    lmx = rad.lmx
    
    max_addr = 26
    n_read =  2
    # R on [0, 65535]
    test_input = [0, 100, 5000, 26100, 65535]
    
    for idx, inpt in enumerate(test_input):
        print('testing pll_r %d of %d' % (idx, len(test_input)))
        lmx.set_pll_r(inpt)
        sleep(SLP)
        r_addr, r = lmx.get_pll_r()
        assert r_addr == max_addr, "failed test_set_pll_r case %d.0, %d != %d" % (idx, r_addr, max_addr)
        assert inpt == r, "failed test_set_pll_r case %d.1, %d != %d" % (idx, inpt, r)
        print('passed test_set_pll_r case %d' % idx)
        sleep(SLP)
    print('passed set_r')

def test_set_charge_pump(rad):
    ''' test for the charge pump config '''
    lmx = rad.lmx
    
    max_addr = 28
    n_read =  1
    # pump on [0, 31]
    # pol on [0, 1]
    test_input = [[0, 0], [15, 0], [31, 0],
                  [0, 1], [15, 1], [31, 1]]
    
    for idx, inpt in enumerate(test_input):
        print('testing charge_pump %d of %d' % (idx, len(test_input)))
        lmx.set_charge_pump(inpt[0],inpt[1])  # pump, pol
        sleep(SLP)
        r_addr, pump, pol = lmx.get_charge_pump()
        assert r_addr == max_addr, "failed test_set_charge_pump case %d.0, %d != %d" \
                % (idx, r_addr, max_addr)
        assert inpt[0] == pump, "failed test_set_charge_pump case %d.1, %d != %d" \
                % (idx, inpt[0], pump)
        assert inpt[1] == pol, "failed test_set_charge_pump case %d.2, %d != %d" \
                % (idx, inpt[1], pol)
        sleep(SLP)
        print('passed test_set_charge_pump case %d' % idx)
    print('passed set_cp')

def test_read_reg0(rad):
    lmx = rad.lmx
    
    addr = 0
    n = 1
    dflt=0x18
    r_addr, rd_value = lmx.spi_read(addr, n)
    assert r_addr == addr, "failed test_read_reg0 case 0, %d != %d" \
            % (r_addr, addr)
    assert dflt == rd_value, "failed test_read_reg0 case 1, %d != %d" \
            % (dflt, rd_value)
    
def test_read_r31(rad):
    # TEST BARE BONES READ 
    lmx = rad.lmx
    
    max_addr = 31
    n_read = 1
    dflt = 0x32
    r_addr, rd_value = lmx.spi_read(max_addr, n_read)
    assert r_addr == max_addr, "failed test_read_r31 case 0, %d != %d" \
            % (r_addr, max_addr)
    assert dflt == rd_value, "failed test_read_r31 case 1, %d != %d" \
            % (dflt, rd_value)
    
def test_set_cp_thresh(rad):
    ''' test for the charge pump threshold values config '''
    lmx = rad.lmx
    
    max_addr = 31
    n_read =  2
    # low on [0, 63]
    # high on [0, 63]
    test_input = [[0, 0], [15, 0], [31, 0], [63,0],
                  [0, 1], [15, 15], [31, 31], [63, 63],
                  [63, 1], [63, 15], [63, 31],]
    
    
    for idx, inpt in enumerate(test_input):
        print('testing cp_thresh %d of %d' % (idx, len(test_input)))
        lmx.set_cp_thresh(inpt[0],inpt[1])  # low, high
        sleep(SLP)
        outpt = lmx.get_cp_thresh()
        for an_idx, val in enumerate(outpt[1:3]):
            # ignore the read_tbState-back flags
            assert inpt[an_idx] == val, "failed test_set_cp_thresh case %d.%d, %d != %d" \
                % (idx, an_idx, inpt[an_idx], val)
        print('passed test_set_cp_thresh case %d' % idx)
        sleep(SLP)
    print('passed set_cp_thresh')

def test_set_others(rad):
    lmx = rad.lmx
    test_input = [[0, 0, 0, 0],
                  [0, 1, 1, 2],
                  [0, 1, 2, 3],
                  [1, 1, 2, 2],
                  ]
    
    for idx, inpt in enumerate(test_input):
        print('testing others %d of %d' % (idx, len(test_input)))
        lmx.set_other_dividers(*inpt)  # doubler, diff_r, delay, csr
        sleep(SLP)
        outpt = lmx.get_other_dividers()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "failed test_set_others case %d.%d, %d != %d" \
                % (idx, an_idx, inpt[an_idx], val)
        sleep(SLP)        
        print('passed test_set_others case %d' % idx)
    print('passed set_others')

def test_set_fastlock(rad):
    lmx = rad.lmx
    
    test_input = [[0, 0],
                  [1, 100],
                  [0, 500],
                  [13, 2047],
                  [25, 0],
                  [21, 500],
                  [31, 2047],
                  ]
    
    for idx, inpt in enumerate(test_input):
        print('testing fastlock %d of %d' % (idx, len(test_input)))
        lmx.set_fastlock_timer(*inpt)  # doubler, diff_r, delay, csr
        sleep(SLP)
        outpt = lmx.get_fastlock_timer()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "failed test_set_fastlock case %d.%d, %d != %d" \
                % (idx, an_idx, inpt[an_idx], val)
        sleep(SLP)        
        print('passed test_set_fastlock case %d' % idx)
    print('passed set_fastlock')

    
def test_set_dld(rad):
    lmx = rad.lmx
    
    test_input = [[5, 0, 0],  # CNT, ERR, TOL
                  [16, 0, 1],
                  [33, 15, 2],
                  [61, 24, 4],
                  [128, 31, 5]]
    for idx, inpt in enumerate(test_input):
        print('testing dld %d of %d' % (idx, len(test_input)))
        lmx.set_dld(*inpt)  
        sleep(SLP)
        outpt = lmx.get_dld()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "failed test_set_dld case %d.%d, %d != %d" \
                % (idx, an_idx, inpt[an_idx], val)
        sleep(SLP)
        print('passed test_set_dld case %d' % idx)
    print('passed set_dld')

def test_set_mux(rad):
    ''' test for result of readback after 'set io 0 3 13' 
        with params ADDR, TYPE, MODE
    '''
    lmx = rad.lmx
    
    # don't mess with CH 2, MOD, the readback
    test_input = [[0, 3, 13],
                  [0, 4, 0],
                  [1, 6, 12],
                  [3, 1, 6],
                  ]
    nInput = np.shape(test_input)[0]
    
    for id in np.arange(nInput):
        this_input = test_input[id]
        # execute command
        lmx.set_io(*this_input)
        
        addr = this_input[0]
        sleep(SLP)
        answer = lmx.get_io(addr)
        
        assert np.array_equal(this_input, answer), \
                "failed lmx_set_mux case %d\n%s != %s" % (id,
                                                            str(this_input),
                                                            str(answer))
        print('passed set_io case %d' % id)
        sleep(SLP)
    print('passed lmx set mux')



def test_ramp_pwr(rad):
    lmx = rad.lmx
    
    test_input = [0, 1]
    for idx, inpt in enumerate(test_input):
        print('testing ramp_power count %d of %d' % (idx, len(test_input)))
        lmx.set_ramp_power(inpt)  
        sleep(SLP)
        outpt = lmx.get_ramp_power()
        
        assert inpt == outpt, "failed ramp_power case %d, %d != %d" \
            % (idx, inpt, outpt)
        sleep(SLP)
        print('passed ramp_power case %d' % idx)
    print('passed ramp_power')
    
def test_ramp_general(rad):
    lmx = rad.lmx 
    
    test_input = [[0, 0, 0, 0, 1],  # clk, pm, cnt, auto, inc_src
                   [1, 1, 100, 1, 3],
                   [1, 0, 1500, 0, 2],
                   [0, 1, 4095, 1, 1]]

    for idx, inpt in enumerate(test_input):
        print('testing ramp_general count %d of %d' % (idx, len(test_input)))
        lmx.set_ramp_general(*inpt)  
        sleep(SLP)
        outpt = lmx.get_ramp_general()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "failed ramp_general case %d.%d, %d != %d" \
            % (idx, an_idx, inpt[an_idx], val)
        sleep(SLP)        
        print('passed ramp_general case %d' % idx)
    print('passed ramp_general')
    
def test_ramp_trig(rad):
    lmx = rad.lmx 
    
    test_input = [[0, 0, 1],  # A, B, C
                   [1, 12, 3],
                   [6, 0, 2],
                   [0, 1, 11]]

    for idx, inpt in enumerate(test_input):
        print('testing ramp_trig %d of %d' % (idx, len(test_input)))
        lmx.set_ramp_trig(*inpt)  
        sleep(SLP)
        outpt = lmx.get_ramp_trig()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "\tfailed ramp_trig case %d.%d, %d != %d" \
            % (idx, an_idx, inpt[an_idx], val)
        print('passed ramp_trig case %d' % idx)
        sleep(SLP)
    print('passed ramp_trig')
        
def test_ramp_comp(rad):
    lmx = rad.lmx 
    
    test_input = [[1200, 0, 0, 0],  # comp0, comp0_en, comp1, comp1_en
                   [1210000, 1,  1, 3],
                   [10000, 15, 1200000, 200],
                   [1500, 7,  1203000, 255]]

    for idx, inpt in enumerate(test_input):
        print('testing ramp_comp count %d of %d' % (idx, len(test_input)-1))
        lmx.set_ramp_comp(*inpt)  
        sleep(SLP)
        outpt = lmx.get_ramp_comp()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "\tfailed ramp_comp case %d.%d, %d != %d" \
            % (idx, an_idx, inpt[an_idx], val)
        print('passed ramp_comp case %d' % idx)
        sleep(SLP)
    print('passed ramp_comp')

def test_ramp_limit(rad):
    lmx = rad.lmx 
    
    test_input = [[1200, 0],  # high, low
                   [12000000, 3],
                   [10000, 1200000,],
                   [1, 70000],
                   [1203000, 255]]

    for idx, inpt in enumerate(test_input):
        print('testing ramp_limit count %d of %d' % (idx, len(test_input)-1))
        lmx.set_ramp_limit(*inpt)  
        sleep(SLP)
        outpt = lmx.get_ramp_limit()
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "\tfailed ramp_limit case %d.%d, %d != %d" \
            % (idx, an_idx, inpt[an_idx], val)
        print('passed ramp_limit case %d' % idx)
        sleep(SLP)
    print('passed ramp_limit')
    
def test_calc_ramps(rad):
    # needs refinement
    test_input = [[[12500, 100], # end_freq, duration
                   [12250, 100]],
                   
                  [[12300, 50], # end_freq, duration
                   [12250, 50]],
                  
                  [[12300, 50],
                   [12300, 5],
                   [12250, 50],
                   [12250, 5]],
                  ]
    true_output = [[[4194, 10000], # inc, leng
                    [rad.lmx.rampy._make_increasing(-4194), 10000]],
                    
                   [[1678, 5000], # inc, leng
                    [rad.lmx.rampy._make_increasing(-1678), 5000]],
                   
                    [[1678, 5000], # inc, leng
                    [rad.lmx.rampy._make_increasing(-3), 500],  # 1073741821
                    [rad.lmx.rampy._make_increasing(-1678), 5000], #1073740146
                    [3, 500]],
                   ]
                   
    lmx = rad.lmx
    
    for idx, inpt in enumerate(test_input):
        sleep(SLP)
        # set sys to 12250
        lmx.set_n(122), sleep(SLP)
        lmx.set_frac_num(8388608), sleep(SLP)
        lmx.set_frac_den(16777215), sleep(SLP)
        lmx.rampy.update_sys(), sleep(SLP)
        
        n_ramps = np.shape(test_input[idx])[0]
        print('testing calc_ramps count %d of %d' % (idx, len(test_input)-1))
        outpt = lmx.rampy.calc_ramps(inpt)  
        
        for an_idx, val in enumerate(outpt):
            targ = true_output[idx][an_idx]
            if not np.allclose(targ, val,atol=1):
                print("\tfailed calc_ramps case %d.%d" % (idx, an_idx))
                print("\ttruth: %s" % targ)
                print("\ttest: %s" % val)
                return
            else:
                print('passed calc_ramps case %d.%d' % (idx, an_idx))
    print('passed calc_ramps')


def test_read_ramps(rad):
    sleep(.01)
    # basically the reverse of calc_ramps
    true_output = [[[12500, 100], # end_freq, duration
                   [12250, 100]],
                   
                  [[12300, 50], # end_freq, duration
                   [12250, 50]],
                  
                  [[12300, 50],
                   [12300, 5],
                   [12250, 50],
                   [12250, 5]],
                  ]
    test_input = [[[4194, 10000], # inc, leng
                    [-4194, 10000]],
                    
                   [[1678, 5000], # inc, leng
                    [-1678, 5000]],
                   
                    [[1678, 5000], # inc, leng
                    [0, 500],
                    [-1678, 5000],
                    [0, 500]],
                   ]
                   
    lmx = rad.lmx
    
    for idx, inpt in enumerate(test_input):
        sleep(SLP)
        # set sys to 12250
        lmx.set_n(122), sleep(SLP)
        lmx.set_frac_num(8388608), sleep(SLP)
        lmx.set_frac_den(16777215), sleep(SLP)
        lmx.update_rampy(), sleep(SLP)
        
        n_ramps = np.shape(test_input[idx])[0]
        print('testing read_ramps count %d of %d' % (idx, len(test_input)-1))
        extended_input = np.zeros((n_ramps,9))
        extended_input.T[1] = np.array(inpt).T[0]
        extended_input.T[4] = np.array(inpt).T[1]
        outpt = lmx.rampy._read_ramps(extended_input)  
        
        for an_idx, val in enumerate(outpt):
            targ = true_output[idx][an_idx]
            if not np.allclose(targ, val,atol=1):
                print("\tfailed read_ramps case %d.%d" % (idx, an_idx))
                print("\ttruth: %s" % targ)
                print("\ttest: %s" % val)
                return
            else:
                print('passed read_ramps case %d.%d' % (idx, an_idx))
    print('passed read_ramps')

def test_ramp_triangle(rad):
    sleep(.01)
    lmx = rad.lmx
    true_output = [[12325, 100], # end_freq, duration
                [12225, 100]]
    # set sys to 12250
    lmx.set_n(122), sleep(.05)
    lmx.set_frac_num(3774874), sleep(SLP)
    lmx.set_frac_den(16777215), sleep(SLP)
    lmx.update_rampy(), sleep(SLP)
    lmx.set_triangle(), sleep(SLP)
    test_output = lmx.get_user_ramps()
    
    if true_output != test_output:
        print("\tfailed ramp_simple case")
        print("\ttrue: %s" % (str(true_output)))
        print("\ttest: %s" % (str(test_output)))
        return
    print('passed set_triangle')

    
def test_ramp_reg(rad):
    ''' test actual command to set a ramp reg '''
    lmx = rad.lmx 
    sleep(.01)
    test_input = [[7, 2517, 0, 0, 10000, 0, 0, 0, 1],  # ramp_num, inc, fl, delay, len, flag, rst, next_trig, nexty
                   [1, 1739307, 0, 1, 10000, 1, 0, 1, 2],
                   [2, 4793, 1, 1, 17500, 3, 1, 1, 0],
                   ]

    for idx, inpt in enumerate(test_input):
        print('testing ramp_reg count %d of %d' % (idx, len(test_input)-1))
        lmx.set_ramp_reg(*inpt)  
        sleep(3*SLP)
        outpt = lmx.get_ramp_reg(inpt[0])
        for an_idx, val in enumerate(outpt):
            assert inpt[an_idx] == val, "\tfailed ramp_reg case %d.%d, %d != %d" \
            % (idx, an_idx, inpt[an_idx], val)
        print('passed set_ramp_reg case %d' % idx)
    print('passed set_ramp_reg')
    

    


## ----    ADC TEST -------------

def test_adc(rad):
    # All Passed 2/11/20
    test_adc_id(rad)
    test_adc_grade(rad)
    test_adc_set_offset(rad)
    test_adc_set_clock(rad)
    test_adc_set_output_mode(rad)
    test_adc_output_adj(rad)
    test_adc_test(rad)
    test_adc_delay(rad)
    test_adc_pattern(rad)
    test_adc_bist(rad)


def test_adc_id(rad):
    true_addr, true_id = 0x01, 0x6F
    addr, idx = rad.adc.read_id()
    assert addr == true_addr, "failed adc_id test, wrong address"
    assert idx == true_id, "failed adc_id test, wrong id: %d" % idx
    
    print('passed adc read_tbState id')

def test_adc_grade(rad):
    true_addr, true_grade = 0x02, 2
    addr, grade = rad.adc.read_grade()
    grade = int(grade >> 4)
    assert addr == true_addr, "failed adc_grade test, wrong address"
    assert grade == true_grade, "failed adc_grade test, wrong grade: %d" % grade
    
    print('passed adc read_tbState grade')
    
def test_adc_set_offset(rad):
    true_offset = [-125, -31, 0, 25, 125]
    
    for idx, off_val in enumerate(true_offset):
        rad.adc.set_offset(off_val)
        sleep(SLP)
        test_off = rad.adc.get_offset()
        assert off_val == test_off, \
            "failed adc_set_offset test %d of %d, wrong offset: %d!=%d" \
            % (idx, len(true_offset), test_off, off_val)
    print('passed adc set offset')

def test_adc_set_clock(rad):
    input_clk = [1, 2, 4]
    
    for idx, val in enumerate(input_clk):
        rad.adc.set_cdiv(val)
        sleep(SLP)
        test_clk = rad.adc.get_cdiv()
        assert val == test_clk, \
            "failed adc_set_clock test %d of %d, wrong targ: %d!=%d" \
            % (idx, len(input_clk), test_clk, val)
    print('passed adc set clock')
def test_adc_set_output_mode(rad):
    inpt = [[2, 0, 0],
             [2, 1, 1],
             [0, 0, 2]
             ]
    for idx in np.arange(len(inpt)):
        this_input = inpt[idx]
        rad.adc.set_output_mode(*this_input)
        sleep(SLP)
        rsp = rad.adc.get_output_mode()
        assert np.array_equal(this_input, rsp), \
                "failed adc_set_output_mode case %d\n%s != %s" % (idx,
                                                                 str(this_input),
                                                                 str(rsp))
    print('passed adc set output mode')

def test_adc_output_adj(rad):
    inpt = [[3, 2, 1, 0],
             [3, 1, 0, 1],
             [1, 0, 0, 2],
             [3, 3, 3, 3],
             [0, 0, 0, 0]
             ]
    for idx in np.arange(len(inpt)):
        this_input = inpt[idx]
        rad.adc.set_output_adj(*this_input)
        sleep(SLP)
        rsp = rad.adc.get_output_adj()
        assert np.array_equal(this_input, rsp), \
                "failed adc_set_output_adj case %d\n%s != %s" % (idx,
                                                                str(this_input),
                                                                str(rsp))
    print('passed adc set output adjust')
    
def test_adc_bist(rad):
    bList = []
    for idx in np.arange(5):
        bist_rtn = rad.adc.set_bist()
        bList.append(bist_rtn)
        sleep(SLP)
     
    assert all(x==0 for x in bList), "failed adc_set_bist, not consistent bist return: " \
        + print(bList)
    print('passed adc bist')

    
def test_adc_pattern(rad):
    ch = [0, 1]
    inputs = [102, 55044, 1000, 26000]
    
    for ch_idx,a_ch in enumerate(ch):
        for in_idx, a_input in enumerate(inputs):
            rad.adc.set_pattern(a_ch, a_input)
            sleep(SLP)
            patt = rad.adc.get_pattern(a_ch)
            assert inputs[in_idx] == patt, \
                "failed adc_pattern case %d.%d, %d != %d" % (ch_idx,
                                            in_idx,inputs[in_idx],patt)
    print('passed adc set pattern')
    
def test_adc_test(rad):
    seq = [0, 1, 2,]
    inputs = [0, 1, 2, 3]
    
    for seq_idx,a_seq in enumerate(seq):
        for in_idx, a_input in enumerate(inputs):
            rad.adc.set_test(a_seq, a_input)
            sleep(SLP)
            the_seq, the_curve = rad.adc.get_test()
            
            assert a_seq == the_seq, \
                "failed adc_set_test case %d.%d, bad sequence, %d != %d" % (seq_idx,
                                            in_idx, a_seq, the_seq)
            assert a_input == the_curve, \
                "failed adc_set_test case %d.%d, bad curve, %d != %d" % (seq_idx,
                                            in_idx, a_input, the_curve)
                
    print('passed adc test')

def test_adc_delay(rad):
    inputs = [[0, 1, 5],
              [0, 1, 2],
              [1, 0, 5],
              [1, 0, 2]
              ]           
    for idx, a_input in enumerate(inputs):
        rad.adc.set_delay(*a_input)
        sleep(SLP)
        outpt = rad.adc.get_delay()
        for an_idx, val in enumerate(outpt):
            assert a_input[an_idx] == val, "failed adc_delay case %d.%d, %d != %d" \
            % (idx, an_idx, a_input[an_idx], val)
    print('passed adc set delay')       
        
## ----    Trace Buffer Tests  -----
def test_bram(rad):
    nSeed = 6
    num_words = 30
    golden_vector = gen_reference(seed= nSeed, size = num_words, marker = 234,
                                   baseX = 100)
    
    test_writeBRAM(rad, golden_vector)
    test_readBRAM(rad, num_words, golden_vector, 0)  # failed 2/5/20
    test_emulated(rad)
    test_clearBRAM(rad)
    test_read_samples(rad)

    

def test_read_samples(rad):
    import datetime
    samp, t = rad.tb.read_samp()
    assert len(samp) == 255, "failed read_samples, wrong length samples, %d != 255" % len(samp)
#   #fw 1.29      assert len(samp) == 2048, "failed read_samples, wrong length read_tbState, %d != 2048" % len(samp)
    assert samp[0] & 0xC000 == 0, "failed read_samples 14-bit test"
    assert type(t)== datetime.datetime, "time entry in wrong format"
    
    samp2, t = rad.tb.read_samp(100)
    assert len(samp2) == 100, "failed read_samples2, wrong length samples, %d != 100" % len(samp2)
    
    
    # MORE sanity check on samples 
    print("passed read_samples")
    
def test_clearBRAM(rad):   
    ''' this also serve as test for long-write and long-read_tbState '''
    true_vec = np.zeros(255)
    rad.tb.clear_bram1()
    rd_addr, rd_list = rad.tb.read_bram1(0, 255)
    assert rd_addr == 0, "failed clearBRAM, wrong read_tbState addr, %d" % rd_addr
    assert np.array_equal(true_vec, rd_list), "failed clearBRAM, unequal vectors"
    print("passed clear_samples")

def test_emulated(rad):
    rd_data = rad.tb.get_emBurst()
    NUM_WORDS = 125
    golden_vector = gen_reference(seed=1, size = NUM_WORDS,
                                   marker = 0, baseX = 3)
    same_vec(rd_data, golden_vector, NUM_WORDS)
    print("passed emulated")    

def test_writeBRAM(rad, vec):
    length = len(vec)
    packet = rad.tb.write_bram1(0, vec)
    print("passed 'WRITING' BRAM Data @ {} Words  - Completed ".format(length))

def test_readBRAM(rad, len_to_read, reference, offset=0):  
    _, rd_list = rad.tb.read_bram1(0, len_to_read)
    same_vec(rd_list, reference, len_to_read, offset)
    
    
def same_vec(a,b, leng, offset=0): 
    ''' check vectors are the same '''
    # check
    kk = 0
    num_errors = 0
      
    do_print = False
    
    for item in a:
        if do_print:
            print("data[{}] \t\t{} expected \t {}".format((offset+kk), item, b[kk]))
        if (item != b[kk]):
            num_errors += 1
        kk += 1
    
    if (num_errors == 0):
        print("passed 'READING' BRAM Data @ {} Words ".format(leng))
        if do_print:
            print("******** Errors = 0")
            print("******** BRAM Unit Test SUCCESSFULL.\r")

def test_tb_states(rad):
    ack_state = rad.tb.write_read(rad.tb.CMD_ARM)
    assert ack_state == rad.tb.sARM, "failed tb_states case %d\t%s != %s" \
         % (1, ack_state, rad.tb.sARM)
    ack_state = rad.tb.write_read(rad.tb.CMD_TRIG)
    assert ack_state == rad.tb.sTRIG, "failed tb_states case %d\t%s != %s" \
        % (2, ack_state, rad.tb.sTRIG)
    ack_state = rad.tb.read_tbState(rad.tb.CMD_STATUS)
    assert ack_state == rad.tb.sIDLE, "failed tb_states case %d\t%s != %s" \
        % (3, ack_state, rad.tb.sIDLE)
    ack_state = rad.tb.write_read(rad.tb.CMD_AUTO)
    assert ack_state == rad.tb.sAUTO, "failed tb_states case %d\t%s != %s" \
        % (4, ack_state, rad.tb.sAUTO)
    ack_state = rad.tb.read_tbState(rad.tb.CMD_STATUS)
    assert ack_state == rad.tb.sAUTO, "failed tb_states case %d\t%s != %s" \
        % (5, ack_state, rad.tb.sAUTO)
    ack_state = rad.tb.write_read(rad.tb.CMD_DISABLE)
    assert ack_state == rad.tb.sIDLE, "failed tb_states case %d\t%s != %s" \
        % (6, ack_state, rad.tb.sIDLE)
        
        
## ----    DDR TESTS   ----------
def test_ddr(rad):
    # settings
    
    packed_data, true_data = gen_packed_data()
#     b15, b14, data = rad.parse_data(packed_data)
    

def test_unpack(rad):
    def parse_data(values_list):
        ''' split out the data and flags 
        @param values_list [s32 list]: the data coming from DDR
        @return b15 [np.array]: an array of 'start' pulses from the data
        @return b14 [np.array]: an array of 'end' pulses from the data
        @return data14 [np.array]: the data in 14-bit format
        '''
        grab_data = lambda t: t & 0x3fff
        data14 = grab_data(values_list)
        data14_twos = np.array([ru.from_twos(x, 14) for x in data14])

        b15_list = []
        b14_list = []
        for itm in values_list: 
            val = ru.sign_extend16(itm)
            b15 = -(val >> 15)
            b14 = (val >> 14) & 0x01
            b15_list.append(b15)
            b14_list.append(b14)
        
        b15 = np.array(b15_list)
        b14 = np.array(b14_list)
        stacked_data = np.vstack((b15, b14, data14_twos))
        return stacked_data
    
    test_input = np.array([0x80E6, 0x3F1A, 0x7F1A, 0x1F1A, 0x20E6])
    truth_data = np.array([[1, 0, 230],
                  [0, 0, -230],
                  [0, 1, -230],
                  [0, 0, 7962],
                  [0, 0, -7962],
                  ]).T 
    
    test_output = parse_data(test_input)
    if not np.array_equal(truth_data, test_output):
        print("unpack parse output error")
        print('test output\n')
        print(test_output)
        print('true values\n')
        print(truth_data)
        return
    print('passed unpack data')
    

    
def gen_packed_data(do_plot=True):
    ''' generate data packed like from ADC '''
    import struct
    leng = 255
    num_ramp = 14
    N = 15  # ramp samples
    D = 4  # recovery samples
    f = 0.25
    dt = 1.0/50 # s
    start_idx = 4       
    b15 = np.zeros((1,leng))
    b14 = np.zeros_like(b15)
    i = np.arange(leng)*dt
    data = np.sin(2*np.pi*f*i)
    up = np.ones(N)
    down = np.zeros(D)
    ramp_valid = []
    for rIdx in np.arange(num_ramp):
        chunk = np.hstack((up,down))
        ramp_valid.append(chunk)
    ramp_valid = np.ravel(np.array(ramp_valid))  # flatten
    
    # prepend 0's at start, trim to 255 (not a complete last ramp)
    ramp_valid = np.insert(ramp_valid, 0, np.zeros(start_idx))[:leng]
    
    ld_edges = ru.find_zcrossings(ramp_valid)
    fl_edges = ru.find_zcrossings(ramp_valid, do_rising=False)
    
    pack_data = np.copy(data)
    # join edges to data
    for riseIdx in ld_edges:
        pack_data[riseIdx] = (1 << 15) + data[riseIdx]
    for fallIdx in fl_edges:
        pack_data[fallIdx] = (1 << 14) + data[fallIdx]

    if do_plot:
        t = np.arange(len(data))*dt
        fig, ax = plt.subplots()
        ln1 = ax.plot(t,pack_data, '-b', label='pack data')
        ax.set_xlabel('time (s)')
        ax.set_ylabel('Packed')
        ax.yaxis.label.set_color('b')
        ax.tick_params(axis='y', labelcolor='b')
        ax2 = ax.twinx()
    
        ln2 = ax2.plot(t,ramp_valid, '*r', label='ramping')
        ax2.set_ylabel('Ramp Valid')
        ax2.yaxis.label.set_color('r')
        ax2.tick_params(axis='y', labelcolor='r')
        ax.set_title('Packed Data and Ramp_Valid')
        
        legs = ln1 + ln2
        
        labs = [l.get_label() for l in legs]
        ax.legend(legs, labs, loc='lower right')
    return pack_data, data
        
## ----    TEST UTILS  ----------
def gen_reference(seed, size, marker, baseX):
    """ Return a python list from numpy vector of length size with randomized values.
        Also note that if seed ==1, the list list is fixed slope linear. 

    Keyword arguments:
    seed -- sets the range of randomized stochastic multiplier.
    size -- the length of the vector.
    marker -- used to distinguish the first value in vector. 
    baseX  -- the normal scalar to the vector, i.e. baseX = 100 
              results in an arry of [0,100,200,300,..]  
    """
    from random import randint
    values = randint(1, seed)*np.linspace(1, size, size)
    num_send = len(values)
    values = [int(baseX * item - baseX) for item in values]
    values[0] = marker
    return values 




if __name__ == '__main__':
    
    
    print("radTests.py startup\n")
    
    rad = 0
    test_unpack(rad)
    plt.show()