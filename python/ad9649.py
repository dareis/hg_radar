
''' 
code to handle the AD9649 driver

Darren Reis
3/2020

'''



import struct
import utils
import json
import socket
from time import sleep

# HG imports
from lmx2492 import Lmx2492

class AD9649(object):
    
    # channel identifiers
    CMD_ADC = 0xBB
    RSP_ADC = 0xB8
    nHEADER = 4
    
    READ = 0
    WRITE = 1
    
    # Internal Registers
    REG_CFG = 0x00          # config SPI port
    REG_ID = 0x01           # READ chip id, should be 0x6F
    REG_GRD = 0x02          # READ chip speed
    
    # normal op writable
    REG_TRNF = 0xFF         # shift data from master
    REG_MODE = 0x08         # set up main modes
    REG_CDIV = 0x0B         # clock divide ratio
    REG_TEST = 0x0D         # set and use test data as output
    REG_BIST = 0x0E         # start built-in-self-test
    REG_OFF_ADJ = 0x10      # offset trim
    REG_OMODE = 0x14        # output level, format
    REG_OADJ = 0x15         # output cmos adjust
    REG_OPHS = 0x16         # output phase
    REG_ODEL = 0x17         # output delay
    REG_USR_P1L = 0x19      # user pattern 1 lsb
    REG_USR_P1M = 0x1A      # user pattern 1 msb
    REG_USR_P2L = 0x1B      # user pattern 2 lsb
    REG_USR_P2M = 0x1C      # user pattern 2 msb
    REG_BIST_SIG = 0x24     # BIST signature
    REG_OR_MODE = 0x2A
    REG_USR2 = 0x101        # internal oscillators

    def __init__(self, sock=None, do_debug=False):

        the_ip = "192.168.1.35"  # Darren
        port = 7
        
        if sock is None:
            try:
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.connect((the_ip, port))
            except socket.error as err:
                self.printer("socket exception %s" % err)
        else:
            self.s = sock
        
        self.set_cmds()
        self._debug = do_debug

        # defaults
        self.sw_reset()
        
    ## ------ HIGH LEVEL -----        
    def read_reg0(self):
        addr, val = self.spi_read(0)
        return addr,val
    def print_reg0(self):
        _, val = self.read_reg0()
        self.printer("REG 0 : %s" % val)
    def read_id(self):
        addr, val = self.spi_read(self.REG_ID)
        return addr,val
    def print_id(self):
        _, val = self.read_id()
        if val==0x6f:
            self.printer("Chip Id: %d, PASS" % val)
        else:
            self.printer("Chip Id: FAIL, %s" % val)
    def read_grade(self):
        addr, val = self.spi_read(self.REG_GRD)
        return addr, val
    def print_grade(self):
        _, val = self.read_grade()
        if val == 0x20:
            self.printer("Speed Grade: PASS, 65 MSPS")
        else:
            self.printer("Speed Grade: FAIL, %x" % val)
    def set_power(self, pstate):
        ''' set the power of the adc
        @param pstate [int]: power state, on [0,7]
            0 - normal, 1 - sleep, 2 - hibernate, 3 - reset, 4 - analog down, 
            5 - other, else - reserved
        '''
        val = 0
        if pstate > 7 or pstate < 0:
            self.printer("set_power error, pstate value must be on [0,7], %d" % pstate)
            return
        if pstate <= 8 and pstate >= 6:
            # reserved states, assume normal opp
            pVal = 0
        else:
            pVal = pstate
        
        val |= pVal       
        
        self.spi_write(self.REG_MODE, val)
        self.update_transfer()
    def set_mode(self, high):
        ''' set the OR/MODE settings
        @param high [int]: 2bit, function for when pin23 is high, on [0,3]
        '''
        if high > 3 or high < 0:
            self.printer("set_mode error, HIGH value must be on [0,3], %d" % high)
            return        
        val = 0
        val |= high < 5
        self.spi_write(self.REG_MODE, val)
        self.update_transfer()
        
    def get_mode(self):
        ''' read back the Modes reg to see MODE settings '''
        _, val = self.spi_read(self.REG_MODE)
        hi_cfg = (val >> 5) & 0x03
        pwr = val & 0x03
        return hi_cfg, pwr
    def print_mode(self):
        strg = "MODE Settings:\n"
        labs = ['HIGH', 'PWR']
        self._printback(strg, labs, self.get_mode())
    def update_transfer(self):
        ''' initiate settings transfer from shadow reg's 
        @note:  reg 0x08 - 0x18 are shadowed, others are direct controlled'''
        self.spi_write(self.REG_TRNF, 1)
    def sw_reset(self):
        ''' do a software reset of ADC '''
        val = 0
        val |= 1 << 5
        val |= 1 << 2
        self.spi_write(self.REG_CFG, val)
        # set the defaults again
        self.set_cdiv(1)
        self.set_power(0)
        self.set_output_mode(2, 0, 1)    
    
    def get_cdiv(self):
        _, val = self.spi_read(self.REG_CDIV)
        div = (val & 0x07) + 1
        return div
    def print_cdiv(self):
        strg = "Clock Divide Settings:\n"
        labs = 'VAL'
        self._printback(strg, labs, self.get_cdiv())
    def set_cdiv(self, div):
        ''' set the clock divide ratio
        @param div [int]: the target div ratio, one of [1, 2, 4]
        '''
        if div == 1:
            tar = 0
        elif div == 2:
            tar = 1
        elif div == 4:
            tar = 3
        else:
            self.printer("set_cdiv error, DIV value must be one of [1,2,4], given %d" % div)
            return
        self.spi_write(self.REG_CDIV, tar)
        self.update_transfer()
        
    def get_test(self):
        _, val = self.spi_read(self.REG_TEST)
        seq = (val >> 6) & 0x03
        curve = val & 0xf       
        return seq, curve
    def print_test(self):
        seq, curve = self.get_test()
        if seq == 0:
            sequence = '0: user pattern static'
        elif seq == 1:
            sequence = '1: user pattern 1 and 2 alternate'
        elif seq == 2:
            sequence = '2: user pattern 1 once then 0s'
        elif seq == 3:
            sequence = '3: user pattern alternate once, then 0s'
            
        if curve == 0:
            curvey = '0: off'
        elif curve == 1:
            curvey = '1: 1000...000'
        elif curve == 2:
            curvey = '2: 111....111'
        elif curve == 3:
            curvey = '3: 000...0000'
        elif curve == 4:
            curvey = '4: 1010....1010'
        elif curve == 5:
            curvey = '5: PN long'
        elif curve == 6:
            curvey = '7: PN short'
        else:
            curvey = 'Not implemented'
        self.printer('Output Test\n\tSequence: %s\tCurve: %s' % (sequence, curvey))

    def set_test(self, seq, curve):
        ''' set a test curve on output pins
        @param seq [int]: 2 bits for sequencing, on [0, 3]
            0 - single value, 1 - alternating, 2 - single once, 3 - alternate once
        @param curve [int]: 4 bits for value of bits, on [0, 12]
            0 - off, 1 - midscale short, 2 - +FS (all 1s), 3 - -FS (all zero),
            4 - checkers, 5 - PN long, 6 - PN short, ...
        '''
        if seq > 3 or seq < 0:
            self.printer("set_test error, SEQ value must be on [0,3], %d" % seq)
            return
        if curve > 12 or curve < 0:
            self.printer("set_test error, CURVE value must be on [0,12], %d" % curve)
            return
        val = 0
        val |= (seq << 6)
        val |= curve
        
        self.spi_write(self.REG_TEST, val)
        self.update_transfer()
        
    def set_bist(self):
        ''' run the built-in-self-test
        @return bist_val [int]: a value from dig core, should be same every time
        '''
        self.spi_write(self.REG_BIST, 5)
        _, bist_val = self.spi_read(0x024)
        return bist_val

    def print_offset(self):
        str = "Level Offset Settings:\n"
        labs = 'VAL'
        self._printback(str,labs, self.get_offset())
    def get_offset(self):
        rd_addr, reg = self.spi_read(self.REG_OFF_ADJ)
        val = utils.from_twos(reg, 8)
        return val
    def set_offset(self, val):
        ''' set the digital level offset, represented in 2's complement
        @param val [int]: the offset trim, [-128, 127]
        '''
        val_2 = utils.to_twos(val,8)
        self.spi_write(self.REG_OFF_ADJ, val_2)
        self.update_transfer()
    
    def get_output_mode(self):
        ''' grab the settings on the OMODE register 
        @return logic, invert, coding [int]: param setting readback
        '''
        rd_addr, reg = self.spi_read(self.REG_OMODE)
        logic = int(reg >> 6)
        invert = (reg >> 2) & 0x01
        coding = int(reg & 0x03)
        return logic, invert, coding 
    def print_output_mode(self):
        strg = "Output Mode Settings:\n"
        labs = ['LOGIC', 'INVERT', 'CODING']
        self._printback(strg, labs, self.get_output_mode())
    def set_output_mode(self, logic, invert=0, coding=0):
        ''' set the output mode settings
        @param logic [int]: 2 bits, the logic level, one of [0,2]
        @param invert [int]: 1 bit, on [0,1], enable output inversion
        @param coding [int]: 2 bits, the output encoding, on [0,3]
        '''
        if logic != 2 and logic != 0:
            self.printer("set_output_mode error, LOGIC value must be either 0,2, %d" % logic)
            return
        if invert > 1 or invert < 0:
            self.printer("set_output_mode error, INVERT value must be on [0,1], %d" % invert)
            return
        if coding > 3 or coding < 0:
            self.printer("set_output_mode error, CODING value must be on [0,3], %d" % coding)
            return
        val = 0
        val |= logic << 6
        val |= 0 << 4 # output enabled when 0
        val |= invert << 2
        val |= coding
        self.spi_write(self.REG_OMODE, val)
        self.update_transfer()
    
    def get_output_adj(self):
        ''' grab the settings on the OUTPUT ADJUST register 
        @return v3dco, v1dco, v3data, v1data [int]: param setting readback
        '''
        rd_addr, reg = self.spi_read(self.REG_OADJ)
        v3dco = int(reg >> 6)
        v1dco = int((reg >> 4) & 0x03) 
        v3data = int((reg >> 2) & 0x03)
        v1data = int(reg & 0x03)
        return v3dco, v1dco, v3data, v1data
    def print_output_adj(self):
        strg = "Output Adjust Levels:\n"
        labs = ['V3DCO', 'V1DCO', 'V3DATA', 'V1DATA']
        self._printback(strg, labs, self.get_output_adj())
    def set_output_adj(self, v3dco, v1dco, v3data, v1data):
        ''' set the output voltage level config
        @param v3dco [int]: 2bits, on [0,3], drive strength of v3.3_dco
        @param v1dco [int]: 2bits,  on [0,3], drive strength of v1.8_dco
        @param v3data [int]: 2bits,  on [0,3], drive strength of 3.3V data
        @param v1data [int]: 2bits,  on [0,3], drive strength of 1.8V data
        '''
        val = 0
        if v3dco > 3 or v3dco < 0:
            self.printer("set_output_adj error, V3DCO value must be on [0,3], %d" % v3dco)
            return
        if v1dco > 3 or v1dco < 0:
            self.printer("set_output_adj error, V1DCO value must be on [0,3], %d" % v1dco)
            return
        if v3data > 3 or v3data < 0:
            self.printer("set_output_adj error, V3DATA value must be on [0,3], %d" % v3data)
            return
        if v1data > 3 or v1data < 0:
            self.printer("set_output_adj error, V1DATA value must be on [0,3], %d" % v1data)
            return
        
        val |= v3dco << 6
        val |= v1dco << 4
        val |= v3data << 2
        val |= v1data
        
        self.spi_write(AD9649.REG_OADJ, val)
        self.update_transfer()
    
    def get_pattern(self,ch):
        if ch == 0:
            _, lsb = self.spi_read(self.REG_USR_P1L)
            sleep(.1)
            _, msb = self.spi_read(self.REG_USR_P1M)  
        elif ch == 1:
            _, lsb = self.spi_read(self.REG_USR_P2L)
            sleep(.1)
            _, msb = self.spi_read(self.REG_USR_P2M)
        else:
            self.printer("bad ch given")    
        
        the_pattern = lsb + (msb << 8)
        return the_pattern
    def print_pattern(self, idx):
        strg = "User Pattern %d:\n" % idx
        labs = ''
        self._printback(strg, labs, self.get_pattern(idx))
    def set_pattern(self, idx, val):
        ''' set the bit value on a user pattern register
        @param id [int]: either 0 or 1, for user pattern 1 or 2
        @param val [int]: a value for bit pattern between 2 bytes, on [0,65535]
        '''
        msb = (val >> 8) & 0xff
        lsb = val & 0xff
        
        if idx > 1 or idx < 0:
            self.printer("set_pattern error, ID value must be on [0,1], %d" % id)
            return
        if val > 65535 or val < 0:
            self.printer("set_pattern error, VAL value must be on [0,65535], %d" % val)
            return
        
        if idx == 0:
            # write to pattern 1
            self.spi_write(AD9649.REG_USR_P1L, lsb)
            sleep(.1)
            self.spi_write(AD9649.REG_USR_P1M, msb)
        else:
            # write to pattern 2
            self.spi_write(AD9649.REG_USR_P2L, lsb)
            sleep(.1)
            self.spi_write(AD9649.REG_USR_P2M, msb)
        # no need to update, this is direct controlled
        self.update_transfer()
        
    def get_delay(self):
        ''' grab the settings on the OUTPUT DELAY register 
        @return dcoE, dataE, delay [int]: param setting readback
        '''
        rd_addr, reg = self.spi_read(self.REG_ODEL)
        dcoE = int(reg >> 7)
        dataE = (reg >> 5) & 0x01
        delay = int(reg & 0x07)
        return dcoE, dataE, delay
    def print_delay(self):
        strg = "Output Delay Settings:\n"
        labs = ['enDCO', 'enDATA', 'DELAY']
        self._printback(strg, labs, self.get_delay())
    def set_delay(self, dcoEnable, dataEnable, delay):
        ''' set up output delay 
        @param dcoEnable [int]: 0 for off, 1 for on
        @param dataEnable [int]: 0 for off, 1 for on
        @param delay [int]: 3bits, on [0,7], the amount to delay 
        '''
        if dcoEnable > 1 or dcoEnable < 0:
            self.printer("set_delay error, dcoEnable value must be on [0,1], %d" % dcoEnable)
            return
        if dataEnable > 1 or dataEnable < 0:
            self.printer("set_delay error, dataEnable value must be on [0,1], %d" % dataEnable)
            return    
        if delay > 7 or delay < 0:
            self.printer("set_delay error, DELAY value must be on [0,7], %d" % delay)
            return
        
        val = 0
        val |= dcoEnable << 7
        val |= dataEnable << 5
        val |= delay 
        self.spi_write(AD9649.REG_ODEL, val)
        self.update_transfer()

    def get_or(self):
        ''' grab the OR cnfg 
        @return state [int]: param setting readback for MODE/OR
        '''
        rd_addr, reg = self.spi_read(self.REG_OR_MODE)
        state = int(reg & 0x01)
        return state
    def print_or(self):
        strg = "OR/MODE Settings:\n"
        labs = 'OR'
        self._printback(strg, labs, self.get_or())
    def set_or(self,state):
        ''' set the or/mode config
        @param state [int] 1bit, on [0,1], 0 for MODE, 1 for OR
        '''
        val = state
        self.spi_write(self.REG_OR_MODE, val)
        self.update_transfer()
        
        
    ## ----- I/O -------
    def spi_read(self, addr):
        ''' read a zynq layer SPI command
        @param addr [int]: the register address
        @return r_addr [int]: the read address
        @return r_value [int]: the readback register values
        '''
        rData = self._pack_adc(self.READ, addr, 0)
        # calc total received bytes on readback 
        n_rcv = Lmx2492.nHEADER + 1
        package = self._spi_read(n_rcv, rData)
        self.debug("read back %s" % package)
        
        addr, values_list = self._unpack_adc(package)
        return addr, values_list
    
    def spi_write(self,addr,value):
        '''  SPI write command
        @param addr [u16]: the register address
        @param value [u8]: list of the register values to set, in descending order
        '''
        rData = self._pack_adc(self.WRITE, addr, value)
        self._spi_write(rData)
    
    ## --   Socket Level  ----
    def _spi_write(self,data):
        ''' write a zynq layer SPI command 
        @param data [spiData_t]: data to be written, in packed structure
        '''
        self.debug("write cmd %s" % data)
        self.s.sendall(data)
        
    def _spi_read(self,num, data):
        ''' send a zynq layer SPI read command
        @param num [int]: the number of bytes to readback
        @param data [spiData_t]: data to be written, for a read, in packed structure
        @return package [spiResponse_t]: readback data
        '''
        self.debug("read cmd %s" % data)
        self.s.sendall(data)
        sleep(.1)
        package = self.s.recv(1024).replace(b'\r\n',b'')
        package = package[:num]
        return package
    
    ## ----  struct Level  -----
    def _pack_adc(self, cmd, addr, value):
        """ pack in a ##### for ADC command
        @param cmd [u8]: conditional for doing readback, 
                            0 for read, 1 for write
        @param addr [u8]: the register address  
        @param value [u8]: a single int to set in the register
        @return packet_binary [####]: a binary packed bytes object to tx
        """
        word1 = AD9649.CMD_ADC     # lmx address
        word2 = cmd                # write command
        word3 = int(addr)          # reg address
        word4 = 1                  # single byte 
        packet_header = [word1, word2, word3, word4]
        packet_data = value
        packet_binary = struct.pack('>4BB', *packet_header,packet_data)
  
        return packet_binary
        
    def _unpack_adc(self,package):
        """ unwrap the ##### package from a read
        @param package [######]: the packed struct to unwrap
        @return addr [u8]: the max register address 
        @return value [u8]: the value of registers read
        """
        m = AD9649.nHEADER
        read_ack, addr, length = struct.unpack(('>3B'),package[1:m])
        value = struct.unpack('>%dB' % length, package[m:])
        if len(value)==1:
            value = value[0]
        return addr, value
    

    ## ---- ADMIN ------
    def __del__(self):
        if self.s is not None:
            self.s.close()

    def _printback(self, title, labs, val):
        ''' generic printback of a feature setting
        @param title [str]: the title for the readback"
        @param labs [str or list]: single or list of param names
        @param val [int or list]: single or list of param values
        @return val 
        '''
        if type(val)==tuple:
            for idx, ele in enumerate(val):
                title += labs[idx] + ': %d\t' % ele
        else:
            title += labs + ': %d\t' % val
        self.printer(title)
        return val
    def printer(self,strg):
        name = "ADC Driver"
        print("%s: %s" % (name,strg))  
    def debug(self,strg):
        if self._debug:
            self.printer(strg)
    def set_cmds(self,):
        self.cmds =  {
                    'read' : {
                            'id' : 'get the device id',
                            'grade' : 'get the device speed grade',
                            'reg0' : 'read the default of reg0',
                            },
                    'get' : {'mode' : 'get the mode register config',
                            'test' : 'set config for a test output',
                            'omode' : 'get the output config mode',
                            'outAdjust' : 'get the level config for outputs',
                            'offset' : 'get the zero level offset',
                            'pattern' : 'get the user set output pattern',
                            'delay' : 'get the delay configs',
                            'cdiv' : 'get the clock divide config',
                            'ormode' : 'get the or/mode config',
                            },     
                    'set'  : {
                            'power STATE' : 'set the power of the adc chip',
                            'mode HIGH' : 'set the MODE pin23 high config',
                            'offset OFF' : 'set the level offset',
                            'bist' : 'trigger the bist test',
                            'omode LOGIC, INVERT, CODE' : 'set the output configs',
                            'outAdjust V3DCO, V1DCO, V3DATA, V1DATA' : 'set the level adjustment configs',
                            'offset VAL' : 'set the zero level offset',
                            'pattern ID VAL' : 'set the user pattern',
                            'delay DCO DATA DELAY' : 'set the delay configs',
                            'test SEQ CURVE' : 'set the test mode configs',
                            'cdiv DIV' : 'set the clock divider config',
                            'ormode STE' : 'set the or/mode config, 0 for MODE',
                            },
                    'reset' : 'sw reset the device',
                    }    
        self.help = {
                    'set' : {'power' : 'STATE on [0, 7]',
                             'mode' : 'HIGH on [0,3]',
                             'offset' : 'OFF on  [-128, 127]',
                             'omode' : 'LOGIC one of [0,2]; INVERT on [0,1]; CODE on [0,3]',
                             'outAdjust' : 'V3DCO, V1DCO, V3DATA, V1DATA on [0, 3]',
                             'pattern' : 'ID on [0,1], VAL on [0,65535]',
                             'delay' : 'DCO, DATA on [0,1] mut exclusive, DELAY on [0,7]',
                             'test'  : 'SEQ on [0,3], CURVE on [0,12]',
                             'cdiv' : 'DIV of [1,2,4]',
                             'ormode': 'STE on [0,1]',
                              },         
                    }
    def get_cmds(self,):
        print(json.dumps(self.cmds, indent=4))
    def help_cmds(self, subcmd):
        print(json.dumps(self.help[subcmd], indent=4)) 
    def cmd_parse(self,cmd):
        ''' parse out user given commands 
        @param cmd [str]: string input from user or super-class
        '''
        inpt = cmd.lower()  
        
        if inpt.startswith("help"):
            input_list = inpt.split(" ")
            if len(input_list) == 1:
                self.get_cmds()
            else:
                try:
                    maincmd = input_list[1]
                    # for now just print params for all commands of 1 type
                    self.help_cmds(maincmd)
                except:
                    self.printer('Bad help command:  %s' % maincmd)
        elif inpt.startswith("reset"):
            self.sw_reset()
        elif inpt.startswith("read"):
            input_list = inpt.split(" ")
            subcmd = input_list[1]
            if subcmd == 'id':
                self.print_id()
            elif subcmd == 'grade':
                self.print_grade()
            elif subcmd == 'reg0':
                self.print_reg0()
            else:
                self.printer("Bad READ command:  %s" % inpt)
        elif inpt.startswith("get"):
            input_list = inpt.split(" ")
            subcmd = input_list[1]
            if subcmd == 'omode':
                m = self.print_output_mode()
            elif subcmd == 'mode':
                m = self.print_mode()
            elif subcmd == 'offset':
                m = self.print_offset()
            elif subcmd == 'outadjust':
                m = self.print_output_adj()
            elif subcmd == 'delay':
                m = self.print_delay()
            elif subcmd == 'test':
                m = self.print_test()
            elif subcmd == 'pattern':
                try:
                    param = int(input_list[2])
                    m = self.print_pattern(param)
                except:
                    print("no pattern idx given, returning pattern 1")
                    m = self.print_pattern(1)
            elif subcmd == 'cdiv':
                m = self.print_cdiv()   
            elif subcmd == 'ormode':
                m = self.print_or()          
            else:
                self.printer("Bad GET command:  %s" % inpt)
        elif inpt.startswith("set"):
            input_list = inpt.split(" ")
            subcmd = input_list[1]
            if subcmd == 'power':
                param = int(input_list[2])
                self.set_power(param)
            elif subcmd == 'mode':
                high = int(input_list[2])
                self.set_mode(high)
            elif subcmd == 'offset':
                param = int(input_list[2])
                self.set_offset(param)
            elif subcmd == 'bist':
                self.printer("bist: %d" % self.set_bist())
            elif subcmd == 'omode':
                logic = int(input_list[2])
                invert = int(input_list[3])
                coding = int(input_list[4])
                self.set_output_mode(logic, invert, coding)
            elif subcmd == 'outadjust':
                v3dco = int(input_list[2])
                v1dco = int(input_list[3])
                v3data = int(input_list[4])
                v1data = int(input_list[5])
                self.set_output_adj(v3dco, v1dco, v3data, v1data)
            elif subcmd == 'pattern':
                idx = int(input_list[2])
                val = int(input_list[3])
                self.set_pattern(idx, val)
            elif subcmd == 'delay':
                dco = int(input_list[2])
                data = int(input_list[3])
                delay = int(input_list[4])
                self.set_delay(dco, data, delay)
            elif subcmd == 'test':
                seq = int(input_list[2])
                curve = int(input_list[3])
                self.set_test(seq, curve)
            elif subcmd == 'cdiv':
                div = int(input_list[2])
                self.set_cdiv(div)
            elif subcmd == 'ormode':
                ste = int(input_list[2])
                self.set_or(ste)
            else:
                self.printer("Bad SET command:  %s" % inpt)
        else:
            self.printer("Bad user command:  %s" % inpt)
                
