'''
GUI to run the Cora Zynq 

@author: Darren Reis
12/2019
'''

# gui imports
import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, \
    QApplication, QSpinBox, QPushButton, QCheckBox
from PyQt5 import uic

# regular imports
import os
import argparse

# HG imports
from radar.Zynq import Zynq

## set xml file
qtCreatorFile = os.path.join(os.path.realpath('python'),'zynq_gui.ui')
 
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)
 
 


class ZynqGUI(QMainWindow):
    def __init__(self, rad=None):
        super(ZynqGUI, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # local buttons
        self.sp_loChan = self.findChild(QSpinBox, 'sp_loChan')
        self.sp_rfChan = self.findChild(QSpinBox, 'sp_rfChan')
        self.sp_rxAtt_mult = self.findChild(QSpinBox, 'sp_rxAtt_mult')
        self.sp_rxAtt_level = self.findChild(QSpinBox, 'sp_rxAtt_level')
        self.sp_bbChan = self.findChild(QSpinBox, 'sp_bbChan')
        self.sp_bbGain_mult = self.findChild(QSpinBox, 'sp_bbGain_mult')
        self.sp_bbGain_level = self.findChild(QSpinBox, 'sp_bbGain_level')
        
        self.pb_log = self.findChild(QPushButton, 'pb_log')
        self.pb_plot = self.findChild(QPushButton, 'pb_plot')
        self.pb_temp = self.findChild(QPushButton, 'pb_temp')
        self.pb_ver = self.findChild(QPushButton, 'pb_ver')
        
        
        self.temp = self.findChild(QTextEdit, 'te_temp')
        self.ver = self.findChild(QTextEdit, 'te_ver')
        self.results = self.findChild(QTextEdit, 'te_results')

        # @TODO: handle checkbox        
        self.cb_debug = self.findChild(QCheckBox, 'cb_deubug')

        if rad is not None:
            self.rad = rad
            # on spin change methods
            self.sp_loChan.valueChanged.connect(lambda i : \
                self.onValueChanged(i, 'LO Channel', self.rad.set_loChan(i)))
            self.sp_rfChan.valueChanged.connect(lambda i: \
                self.onValueChanged(i, 'RF Channel', self.rad.set_rfChannel(i)))
            self.sp_rxAtt_level.valueChanged.connect(lambda i : \
                self.onValueChanged(i, 'Rx Att level', self.rad.set_rxAtten(self.sp_rxAtt_mult.value(), i)))
            self.sp_rxAtt_mult.valueChanged.connect(lambda i : \
                self.onValueChanged(i, 'Rx Att Mult', self.rad.set_rxAtten(i, self.sp_rxAtt_level.value())))
            self.sp_bbChan.valueChanged.connect(lambda i : \
                    self.onValueChanged(i, 'BB Channel', self.rad.set_bbChannel(i)))
            self.sp_bbGain_mult.valueChanged.connect(lambda i : \
                    self.onValueChanged(i, 'BB Gain Multi', self.rad.set_bbGain(i, self.sp_bbGain_level.value())))
            self.sp_bbGain_level.valueChanged.connect(lambda i : \
                    self.onValueChanged(i, 'BB Gain Level', self.rad.set_bbGain(self.sp_bbGain_mult.value(),i)))
            
            # on button press
            self.pb_log.clicked.connect('Getting Log', rad.save_log())
            self.pb_plot.clicked.connect('Getting Quick Plot', rad.plot_log())
            
            # @TODO: on these two, update the text edit field with result
            self.pb_temp.clicked.connect('Getting Chip Temp', rad.get_temp())
            self.pb_ver.clicked.connect('Getting FW Ver', rad.get_hw_tag())
        
        
    def onValueChanged(self,val,strg, func):
        print('%s value is %d' % (strg, val))
        func(val)
    
    def buttonPressed(self,strg, func):
        print(strg)
        func()

    def loChan(self, val):
        ''' function to run to change loChan param '''
        print("\thandled %d" % val)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    print("GUI startup\n")
    
    
    # radar settings
    dflt_ip = "192.168.1.15"  # Darren
    
    
    parser = argparse.ArgumentParser(description=''' 
    This script is a controller for the HG radar.   
    it can be made to execute, or can interactively with user input''')


    parser.add_argument('-s','--sim', action='store_true', default=False,
                    help='run lmx in sim mode')
    parser.add_argument('-d','--debug', action='store_true', default=False,
                help='run in debug mode')
    
    # get arguments
    args = parser.parse_args(sys.argv[1:])
    
#     print("Trying to connect to Zynq")
#     rad = Zynq(dflt_ip, args.sim, args.debug)
    
    
    window = ZynqGUI()
    window.show()
    sys.exit(app.exec_())