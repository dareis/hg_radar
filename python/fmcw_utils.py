''' utilities for fmcw processing in python

written by Darren Reis
copyright Higher Ground
5/2018

Update in 11/2019
'''

import numpy as np
import matplotlib.pyplot as plt
import csv
import scipy.signal as sig



## BASIC UTILS
def f_to_d(f, B, Tp):
    ''' convert beat freq to distance
    @param f [float]: beat frequency measured [Hz]
    @param B [float]: swept Bandwidth [Hz]
    @param Tp [float]: chirp duration [s]
    @return d [float]: range [m]
    '''
    c0 = 3e8 # m/s
    d = c0*f*Tp/(2*B)
    return d

def to_ranges(freqs,B,Tp):
    ''' convert a frequency array to distances
    @param freqs [np.array]: vector of frequencies [Hz]
    @param B [float]: swept Bandwidth [Hz]
    @param Tp [float]: chirp duration [s]
    @return ranges [np.array]: range vector [m]
    '''
    ranges = np.array([f_to_d(x,B,Tp) for x in freqs])
    return ranges

def d_to_f(d, B, Tp):
    ''' convert object distance to beat freq 
    @param d [float]: range [m]
    @param B [float]: swept Bandwidth [Hz]
    @param Tp [float]: chirp duration [s]
    @return f [float]: beat frequency measured [Hz]
    '''
    c0 = 3e8 # m/s
    f = d*2*B/(c0*Tp)
    return f

def generate_ranges(M,dt,B,Tp):
    ''' generate only the positive range bins, as  
    @param M [int]: the number of samples in the fast time-domain vector
    @param dt [float]: the time spacing of the signal in fast-time axis
    @param B [float]: swept Bandwidth [Hz]
    @param Tp [float]: chirp duration [s]
    @return ranges [np.array]:  the ranges, the fast-time spectral axis, 
                                [0 : M/2-1]/(dt*M)
    '''
    freqs =  np.fft.fftfreq(M,dt)[:int(M/2)]
    ranges = to_ranges(freqs, B, Tp)
    return ranges

def generate_velos(vmax,N):
    ''' generate the entire domain of the velocity bins
    @param vmax [float]: the max detectable speed (m/s)
    @param N [int]: the number of samples in the slow time-domain vector
    @return speeds [np.array]:  the doppler speeds, the slow-time spectral axis, 
                                [-N/2 : N/2-1]/(dt*N)
    '''
    if is_odd(N):
        inds = np.hstack((np.arange((N-1)/2+1),
                           -1*np.arange(1,(N+1)/2)[::-1]))
    else:
        inds = np.hstack((np.arange(N/2),
                          -1*np.arange(1,N/2+1)[::-1]))
    speeds = inds*vmax/N
    
    return np.fft.fftshift(speeds)


def max_range(Fs,B,Tp):
    ''' find the farthest out system can see
    @param Fs [float]: the sampling frequency
    @param B [float]: swept Bandwidth [Hz]
    @param Tp [float]: chirp duration [s]
    @return d_max [float]: greatest range from chirp [m]
    '''
    c0 = 3e8 # m/s
    # extra div2 factor because only positive ranges
    d_max = c0*Fs*Tp/(4*B)/2 
    return d_max

def get_tRamp(Fs,B,maxRange):
    ''' based on desired max range, determine ramp params
    this sets the Max Range, or the largest bin in the range-power plot
    @param Fs [float]: the sampling frequency
    @param B [float]: swept Bandwidth [Hz]
    @param max_range [float]: the desired max range observed [m]
    @return Tp [float]: up-ramp duration [s]
    @return Tc [float]: full ramp duration [s]
    '''
    c0 = 3e8 # m/s
    
    # duty cycle to allow for clean ramp recovery
    adi_duty = 260./300
    #ti_duty = .98
    
    Tp = maxRange * 4*B *2/(c0*Fs)
    Tc = Tp/adi_duty
    return Tp, Tc

def reso_range(B):
    ''' find the system range resolution
    @param B [float]: swept Bandwidth [Hz]
    @return d_res [float]: the range resolution
    '''
    c0 = 3e8 # m/s
    # hanning weight factor
    k = 1.43
    d_res = c0*k/(2*B)
    return d_res

def max_velo(cf,Tp):
    ''' find the max velocity perceivable 
    @param cf [float]: center frequency of chirping
    @param Tp [float]: chirp duration [s]
    @return v_max [float]: greatest noticeable doppler speed [m/s]
    '''
    c0 = 3e8 # m/s
    wavelength = c0/cf # m
    v_max = wavelength/(4*Tp)
    return v_max

def reso_velo(cf,Tp,N):
    ''' find the binwidth of the doppler fft
    @param cf [float]: center frequency of chirping
    @param Tp [float]: chirp duration [s]
    @param N [int]: the number of pulses 
    @return v_res [float]: doppler bin width [m/s]
    '''
    c0 = 3e8 # m/s
    wavelength = c0/cf # m
    v_res = wavelength/(4*N*Tp)
    return v_res

def to_dB(sig):
    ''' convert number to log scale
    @param sig [float or np.array]: the signal array or value to compute
    @return log_sig [float or np.array]: the log scale signal
    '''
    try:
        # list or array case
        if len(sig)>0:
            # protect div by 0
            sig[sig == 0] = -np.inf     
            log_sig = 10*np.log10(sig)
    except TypeError:
        # float or int case
        if sig==0:
            log_sig = -np.inf 
        else:
            log_sig = 10*np.log10(sig)  
    return log_sig

def to_power(sig,in_log=False):
    ''' convert number to power spectrum
    @param sig [float or np.array]: the signal array or value to compute
    @param in_log [bool]: conditional to set output in log
    @return pwr_sig [float or np.array]: the log scale signal
    '''
    try:
        # list or array case
        if len(sig)>0:
            # protect div by 0
            sig[sig == 0] = -np.inf     
            pwr_sig = np.power(np.abs(sig),2)
    except TypeError:
        # float or int case
        if sig==0:
            pwr_sig = -np.inf 
        else:
            pwr_sig = np.power(np.abs(sig),2)  
    if in_log:
        pwr_sig = 10*np.log10(pwr_sig)
    return pwr_sig

def from_dB(log_sig):
    ''' convert a dB to a gain value
    @param log_sig [float]: the signal or value
    @return sig [float]: the SI unit gain value
    '''
    return np.power(10, .1*log_sig)


def Vpp_to_dBm(Vpp):
    ''' convert peak-peak Voltage to dBm power, assume 50 ohm'''
    # 10*log10(Vrms^2/Z*P0)
    # Vrms = Vpp/(2*sqrt(2)), Z=50 Ohm, P0 is 1 Watt (or 30 dBm)
    P = 2*to_dB(Vpp/(2*np.sqrt(2))) - to_dB(50) + 30
    return P
    
    
def delay_signal(wave,pts):
    ''' zero pad the front of a signal
    @param wave [np.array]: the waveform
    @param pts [int]: the number of cycles to delay
    @return out_wave [np.array]: the delayed waveform (same length)
    '''
    if len(wave)<pts:
        out_wave = np.zeros_like(wave)
    else:
        out_wave = np.hstack((np.zeros(pts), wave[:-pts:]))
    
    return out_wave

def big_decimate(wave,q):
    ''' do multiple decimations to handle a huge downsampling
    @note  it may be better to index every nth sample instead of this
    @param wave [np.array]: the signal 
    @param q [float]: the decimation factor 
    @return out_wave [np.array]: the return waveform 
    '''
    # make copy of sig for analysis
    intermediate_sig = wave[:]
    if q <= 10:
        out_wave = sig.decimate(wave, q, zero_phase=True)
    else:
        reps = q/10
        while reps>10:
            intermediate_sig = sig.decimate(intermediate_sig,
                                             10,
                                              zero_phase=True)
            reps /= 10
        out_wave = sig.decimate(intermediate_sig,
                                             int(reps),
                                              zero_phase=True)
    return out_wave
        
    
## SIM functions
def get_Pr(r, eirp, rcs, gr, cf, snr, if_gain=0):
    ''' get the returned power using radar range equation 
    @param r [float]: range in m
    @param eirp [float]: EIRP in dBm
    @param rcs [float]: cross section in dB
    @param gr [float]: rx antenna gain
    @param cf [float]: center freq in Hz
    @param snr [float]: signal to noise in dB
    @param if_gain [float]: gain through rx, in dB, default 0
    @return Pr [float]: power recieved at A/D
    '''
    c0 = 3e8
    lam = c0/(cf)
    
    # in dB
    lam = -2*to_dB(lam)
    fourPi = 3*to_dB(4*np.pi)
    r = 4*to_dB(r)
    
    Pr = eirp + rcs + gr + if_gain - (lam + fourPi + snr + r)
    return Pr


def generate_tx(cf,B, fgen, Tp, num_chirps, shape='sawtooth'):
    ''' generate a modulation and time domain signal 
    @param cf [float]: carrier frequency (Hz)
    @param B [Float]: swept frequency of chirp (Hz)
    @param fgen [float]: generating frequency (Hz)
    @param Tp [float]: chirp duration (s)
    @param num_chirps [int]: number of chirps in sequence
    @param shape [string]: 'sawtooth' for sawtooth waveform
                           'ramp'  for triangle waveform
    @return gen_t [np.array]: single period of time vector
    @return time_train [np.array]: the array of time steps
    @return mod_train [np.array]: the array of modulation domain values (freqs)
    @return sig_train [np.array]: the array of time domain values (amp)
    '''
    
    # start/stop freqs
    f0 = cf - B/2
    f1 = cf + B/2

    ## at generator frequency
    # half period
    steps = int(fgen*Tp)
    gen_t = np.arange(steps) / fgen
    reps = num_chirps
    
    # modulation domain (list of freqs), single period
    sawtooth = np.linspace(f0,f1,steps)
    ramp = np.hstack((sawtooth, sawtooth[::-1]))

    # time domain, chirp period (ramp is 2x length of sawtooth)
    saw_chirp = sig.chirp(gen_t, f0, gen_t[-1], f1)
    ramp_chirp = np.hstack((saw_chirp, saw_chirp[::-1]))
    
    if shape=='sawtooth':
        mod_train = np.tile(sawtooth,reps)
        sig_train = np.tile(saw_chirp,reps)
        full_time = np.arange(reps*steps) / fgen
    elif shape=='ramp':
        mod_train = np.tile(ramp,reps)
        sig_train = np.tile(ramp_chirp,reps)
        full_time = np.arange(2*reps*steps) / fgen
    
    return gen_t, full_time, mod_train, sig_train
    
def doppler_effect(t, cf, B, reps, v, d):
    ''' create the radar return signal from a target
    @param t [np.array]: the time vector of the waveform
    @param cf [float]: the carrier freq [Hz]
    @param B [float]: the modulation freq width [Hz]
    @param reps [int]: the number of mods in the sequence
    @param v [float]: the radial velocity of the target (m/s)
    @param d [float]: the starting distance to the target (m)
    @return 
    '''
    
    # freq shift
    cf2 = (1 + v/3e8)*cf
    # start/stop freqs
    f0 = cf2 - B/2
    f1 = cf2 + B/2   
    
    # range delay
    delay = 2*d/3e8  #s
    dt = t[1] - t[0]
    delay_pts = int(np.ceil(delay/dt))
    
    chirp = sig.chirp(t, f0, t[-1], f1)
    chirp_train = np.tile(chirp,reps)
    # np.array of len = nSamples*nFrames
    rx_wave = .6*delay_signal(chirp_train, delay_pts)
    
    return rx_wave
    
def gen_filename(dir='',suffix='csv'):
    ''' generate a file with folder loc 
    @param dir [str]: the relative path to directory to put the file
    @param suffix [str]: the file type to use as suffix
    @return fname [str]: the path to the file generated
    '''
    from datetime import datetime
    
    date_str = datetime.now().strftime('%Y_%m_%d_%H%M%S')
    fname = dir + date_str + suffix
    return fname

def file_get_time(fname):
    ''' get back the datetime value of a string
    @param fname [str]: the rel filename to grab from
    @return dtime [datetime]: the datetime of the file
    '''
    from datetime import datetime
    import os
    namey = os.path.basename(fname)
    date = datetime.strptime(namey[:-2], '%Y_%m_%d_%H%M%S')
    
    return date
    
    

def pickle_save(list_objs, fname=None):
    ''' store away a list of objects to save in a pickle file
    @param list_objs [list of variables]: list of structures to save
    @param fname [str]: the filename for storage, default to date-time
    '''
    import pickle
    import os
    
    if fname == None:
        fname = gen_filename('','.p')
        
    os.makedirs(os.path.dirname(fname), exist_ok=True)
    with open(fname, 'w+b') as file_name:
        # use protocol 2 for compatability
        pickle.dump(list_objs, file_name, 2)
    
    print('written %s ' % fname) 

    
def pickle_load(fname):    
    ''' grab the contents of a pickle file 
    @param fname [str]: the filename for storage
    @return data [obj]: a structure of the pickled data
    '''
    import pickle
    
    file_name = open(fname, 'rb')
    data = pickle.load(file_name)
    file_name.close()
    return data

## ANALYSIS Functions

def reject_outliers(data, sig_thresh=1.281):
    stdev = np.std(data)
    mean = np.mean(data)
    maskMin = mean - stdev * sig_thresh
    maskMax = mean + stdev * sig_thresh
    mask = np.ma.masked_outside(data, maskMin, maskMax)
    print('Masking values outside of {} and {}'.format(maskMin, maskMax))
    return mask

def wv_fft(y, dt, w, v=None):
    ''' take a w-pt fft on the data v times and average them together
    @param y [np.array]: 1d real time domain signal
    @param dt [float]: the sample period of the signal
    @param w [int]: the number of points in the fft, 
    @param v [int]: the number of fft's to run and then average, n_a
        improves std of plot by 1/sqrt(n_a)
    @return ranges [np.array]: 1d range array for the x axis, same length as cRanges
    @return power_ranges [np.array]: 1d power dB plot of the average of the ffts
    @return p_ids [np.array]: 1d array of peak indices
    '''

    ## independent variable 
    # as frequency (hz)  
    freq_binsize = 1./(dt*w)
    max_freq = 1./dt*(1./2-1./w)
    f_hz = np.arange(0,max_freq+freq_binsize,freq_binsize)
    # as range
    max_range = 50 # this corresponds limit on 'ranges' in the main code   
    ranges = np.linspace(0, max_range, num=w/2)
    
    if v is None or v==0:
        # no averaging
        cRanges = np.fft.fft(y[:w])/w
        cRanges = cRanges[:w/2]
        v=1
    else:
        assert len(y) > w*v

        # w point FFT averaging over v times
        cRanges_arr = []
        # apply FFT on sequence of sections of time domain signal
        for i in np.arange(1,v):
            long_raw = y[i*w:(i+1)*w]
            long_cRanges = np.fft.fft(long_raw)
            long_cRanges = long_cRanges[:w/2]/w
            cRanges_arr.append(long_cRanges)
        cRanges = np.mean(np.array(cRanges_arr),0)

    total_noise, bin_noise, p_ids = calc_NF(np.abs(cRanges), dt, w*v) 
    print('(%d,%d)\tTotal noise level: %.2f dB' % (w,v,to_dB(total_noise)))
    print('\t\tAvg noise level: %.2f dB' % to_dB(bin_noise))       

    power_ranges = to_power(cRanges, in_log=True)
    return ranges, power_ranges, p_ids

def extended_fft(y, dt, wv):
    ''' take a w-pt fft on the data v times and average them together
    @param y [np.array]: 1d real time domain signal
    @param dt [float]: the sample period of the signal
    @param wv [int]: the number of points in the dwell time fft, w*v 
    @return ranges [np.array]: 1d range array for the x axis, same length as cRanges
    @return power_ranges [np.array]: 1d power dB plot of the average of the ffts
    @return p_ids [np.array]: 1d array of peak indices
    '''
    
    ## independent variable 
    # as frequency (hz)  
    freq_binsize = 1./(dt*wv)
    max_freq = 1./dt*(1./2-1./(wv))
    f_hz = np.arange(0,max_freq+freq_binsize,freq_binsize)
    # as range
    max_range = 50 # this corresponds limit on 'ranges' in the main code   
    ranges = np.linspace(0, max_range, num=(wv)/2)
    
    cRanges = np.fft.fft(y[:wv])/wv
    cRanges = cRanges[:wv/2]

    total_noise, bin_noise, p_ids = calc_NF(np.abs(cRanges), dt, wv) 
    print('(%d)\tTotal noise level: %.2f dB' % (wv,to_power(total_noise,True)))
    print('\t\tAvg noise level: %.2f dB' % to_power(bin_noise,True))       

    power_ranges = to_power(cRanges, in_log=True)
    return ranges, power_ranges, p_ids

def calc_NF(y,dt,n,n_guard=2,do_plot=False):
    ''' calculate the noise per bin in the FFT, i.e. the plot noise floor
    @param y [np.array]: the freq domain signal
    @param dt [float]: the sampling period (s)
    @param n [int]: the number of points in the FFT
    @param n_guard [int]: the number of guard cells to exclude around the peak
    @param do_plot [bool]: conditional to show the local plot of peaks
    @return total_noise [float]: the total noise in the system (linear-scale)    
    @return bin_noise [float]: the magnitude of each bin noise (linear-scale)
    @return f_hz [np.array]: array of x-axis plot values (Hz)
    @return peak_id [np.array]: the list of peak indices
    '''
    
    
    # frequency:  independent variable
    freq_binsize = 1./(dt*n)
    
    window_len = np.int(.05*len(y))
    # find peak
    peak_id = detect_peaks(to_dB(y), num_train=window_len, num_guard=n_guard, rate_fa=1e-7)
    
    mask = np.ones_like(y)
    # block the DC region (assume about 500 points of clutter effects)
#     len_DC = window_len
#     mask[0:len_DC] = 0
    for p in peak_id:
        # block the peaks' regions
        mask[p-n_guard:p+n_guard+1]=0

    # sum over entire plot without the peak to protect from outlier skewing
    nf = np.sum(y*mask*freq_binsize)

    if do_plot:
        plt.plot(y)
        plt.plot(peak_id,y[peak_id],'*')
    
    bin_noise = nf/(n-len(peak_id)*n_guard)
#     bin_noise = nf/(n-len(peak_id)*n_guard - len_DC)
    return nf, bin_noise, peak_id

def detect_peaks(x, num_train, num_guard, rate_fa):
    """
    Detect peaks with CFAR algorithm.
    @note: taken from http://tsaith.github.io/detect-peaks-with-cfar-algorithm.html
    
    @param x [np.array]: 1D signal to be analyzed
    @param num_train [int]: Number of training cells.
    @param num_guard [int]: Number of guard cells.
    @param rate_fa [float]: False alarm rate. 
    @return peak_idx [np.array]: the indices of the peaks
    
    """
    num_cells = x.size
    num_train_half = int(round(num_train / 2))
    num_guard_half = int(round(num_guard / 2))
    num_side = num_train_half + num_guard_half
 
    # threshold factor
    alpha = num_train*(rate_fa**(-1/num_train) - 1) 
    
    peak_idx = []
    for i in range(num_side, num_cells - num_side):
        i = int(i)
        if i != i-num_side+np.argmax(x[i-num_side:i+num_side+1]): 
            continue
        
        sum1 = np.sum(x[i-num_side:i+num_side+1])
        sum2 = np.sum(x[i-num_guard_half:i+num_guard_half+1]) 
        p_noise = (sum1 - sum2) / num_train 
        threshold = alpha * p_noise
        
        if x[i] > threshold: 
            peak_idx.append(i)
    
    peak_idx = np.array(peak_idx, dtype=int)
    
    return peak_idx


    

def cfar_peak2d(mat, d_train, d_guard, rate_fa):
    '''
    2d cfar peak detection
    @note based on https://github.com/kit-cel/gr-radar/blob/master/lib/os_cfar_2d_vc_impl.cc
    @note we assume CUT is 1 pixel
    
    @param mat [2d np.array]: 2D linear-scale signal to be analyzed
    @param d_train [int]: Number of training cells around the CUT. default is 2
    @param d_guard [int]: Number of guard cells. default is 2
    @param rate_fa [float]: False alarm rate. 
    @return peak_idx [2d np.array]: the indices of the peaks 
    '''
    M,N = np.shape(mat)
    w = d_train + d_guard
    
    num_train = 8*d_train
         
    alpha = num_train*(rate_fa**(-1/num_train) - 1)  # threshold factor      
    
    peak_idx = []
    
    for xi in np.arange(M):
        for yi in np.arange(N):
            # survey in w cells from the edges
            if np.any([xi < w, xi > M-w, 
                             yi < w, yi > N-w]):
                continue
            
            avg = 0
            for tr_xi in np.arange(-d_train,d_train):
                for tr_yi in np.arange(-d_train,d_train):
                    i = tr_xi - xi
                    j = tr_yi - yi
                    # protect guarded cells
                    if np.logical_and(np.abs(i) <= d_guard,
                                      np.abs(j) <= d_guard):
                        continue
                    else: 
                        avg += mat[i,j]
            p_noise = avg/num_train
            
            if mat[xi,yi] > alpha*p_noise:
                peak_idx.append([xi,yi])
    
    peak_idx = np.array(peak_idx,dtype=int)
    return peak_idx

''' plotting helpers '''
def build_colors(num):
    ''' build a list of colors 
    @para num [int]: number of colors
    @return colors [list]: list to be used with plotting keyword 'colors'
    '''
    import matplotlib.cm as mplcm
    import matplotlib.colors as col
    # set up the colors 
    cm = plt.get_cmap('gist_rainbow')
    cNorm = col.Normalize(vmin=0, vmax=num-1)
    scalarMap = mplcm.ScalarMappable(norm=cNorm,cmap=cm)
    colors = [scalarMap.to_rgba(i) for i in range(num)]
    return colors[::-1]
    
def build_markers(num):
    ''' build a list of unique markers 
    @para num [int]: number of shapes
    @return marks [iterable]: generator to allow use of .next() with plot keyword 'marker'
    '''
    import itertools
    marks = [(2+i, 1+i%2, i/num*90.0) for i in range(1,num)]
    return itertools.cycle(marks)


def wrap_180(ang):
    ''' return an angle on [-180, 180) '''
    a = (ang + 180) % 360
    if a < 0:
        a += 360
    b = a - 180
    return b

def positive_angle(ang, in_rad=False):
    ''' return an angle on [0,360) or [0,2*pi) '''
    if in_rad:
        lim = 2*np.pi
    else:
        lim = 360
    a = ang % lim
    if a < 0:
        a += lim
    return a
        
## Filter Functions
def make_coords(xvec, yvec):
    ''' take 2 coordinate vectors and make a 3D np.array of their 
            coordinate values located in their indexed cell
    @param xvec, yvec [1d np.array]: the values of the coordinates
    @return coords [3d np.array]: 3d matrix of coordinate values
        shape:  M,N,P
        indexed as c[x_id, y_id] = [xvec[x_id], yvec[y_id]]
    '''
    
    M = len(xvec)
    N = len(yvec)
    coords = np.ones((M,N,2))
    
    for x_id in range(M):
        for y_id in range(N):
            coords[x_id, y_id] = [xvec[x_id],yvec[y_id]]
    
    return coords        

def x_filter(x_pts, x_bound,above=True):
    ''' apply an x coordinate filter to the data in a frame
    @param x_pts [np-array]: the x points of this frame
    @param x_bound [float]: the east-west value to use as boundary
    @param above [bool]: conditional to pass points above the threshold or not (for below)
    @return pass_ids [np-array]: the indices to pass through
    '''
    if above:
        pass_ids = np.where(x_pts >= x_bound)
    else:
        pass_ids = np.where(x_pts < x_bound)
    return pass_ids

def xx_filter(x_pts, x_bound, inside=True):
    ''' apply two x coordinate filters and get the passing ids
    @param x_pts [np-array]: the x points of this frame
    @param x_bound [float]: the velocity value to use as boundary
    @param inside [bool]: conditional to pass points inside the threshold or not (for outside)
    @return pass_ids [np-array]: the indices to pass through
    '''
    if inside:
        pass_ids = np.arange(len(x_pts))[np.logical_and(x_pts >= -x_bound,
                                                 x_pts <= x_bound)]
    else:
        pass_ids = np.arange(len(x_pts))[np.logical_or(x_pts <= -x_bound,
                                                 x_pts >= x_bound)]
    return pass_ids

def y_filter(y_pts, y_bound, above=True):
    ''' apply a y coordinate filter to the data in a frame
    @param y_pts [np-array]: the y points of this frame
    @param y_bound [float]: the velocity value to use as boundary
    @param above [bool]: conditional to pass points above the threshold or not (for below)
    @return pass_pts [np-array]: the indices to pass
    '''
    if above:
        pass_pts = np.where(y_pts >= y_bound)
    else:
        pass_pts = np.where(y_pts < y_bound)
    return pass_pts

def yy_filter(y_pts, y_bound, inside=True):
    ''' apply two y coordinate filters and get the passing ids
    @param y_pts [np-array]: the y points of this frame
    @param y_bound [float]: the velocity value to use as boundary
    @param inside [bool]: conditional to pass points inside the threshold or not (for outside)
    @return pass_ids [np-array]: the indices to pass
    '''
    if inside:
        pass_ids = np.arange(len(y_pts))[np.logical_and(y_pts >= -y_bound,
                                                         y_pts <= y_bound)]
    else:
        pass_ids = np.arange(len(y_pts))[np.logical_or(y_pts <= -y_bound,
                                                        y_pts >= y_bound)]
        
    return pass_ids
    

## FFT Functions
def complex_fft(t,y,c):
    ''' compute the Quadrature components of a time domain sequence.
    the output freqs are on [-n/2, n/2].
    @param t [1d np-array]: time vector spaced uniformly by dt [s]
    @param y [1d np-array]: complex signal in the time domain 
    @param c [int]: the window length of the vector
    @return freqs [np.array]:  xaxis frequencies from fft [Hz]
            Y [np.array]:  complex freq domain output, related to Volts
    '''
    n = len(y)
    dt = t[1] - t[0]
    ## COMPLEX HANDLING
    # fourier coeffs
    Y = np.fft.fft(y)/c
    # natural frequencies (Hz)
    freqs = np.fft.fftfreq(c,dt)
    
    # circle shift so 0 is in center
    Y = np.fft.fftshift(Y)
    freqs = np.fft.fftshift(freqs)
    
    return freqs, Y[:int(n/2)]

def real_fft(t,y,c):
    ''' compute the real output as half-length Spectrum components 
    of a time domain sequence.  that output freqs are on [0, n/2]. 
    @param t [1d np-array]: time vector spaced uniformly by dt [s]
    @param y [1d np-array]: complex signal in the time domain 
    @param c [int]: the window length of the vector
    @return freqs [np.array]:  xaxis frequencies from fft [Hz]
            Y [np.array]:  real freq domain output
    '''
    n = len(y)
    dt = t[1] - t[0]
    ## COMPLEX HANDLING
    # fourier coeffs
    Y = np.fft.fft(y)/c
    # natural frequencies (Hz)
    freqs = np.fft.fftfreq(c,dt)
    
    # grab only positive freqs, for real components
    Y = np.fft.fftshift(Y)

    return freqs[:int(n/2)], Y[:int(n/2)]

def run_fft2(t,mat):
    ''' build spectral plot 
    @param t [2d np-array]: time vector spaced uniformly [s]
        size: M x N
        fast axis (M, range vector): t[:,i]
        slow axis (N, velocity vector): t[i,:] 
    @param mat [2d np-array]: complex signal in the time domain
    @return spect_mat [2d np.array]: complex spectra of data
    @return freq_x, freq_y [np.array]: the frequencies of the matrix (Hz)
    '''
    
    M, N = [int(x) for x in np.shape(mat)]
    range_dt = t[1,0] - t[0,0]
    velo_dt = t[0,1] - t[0,0]
    spect_mat = np.fft.fft2(mat) / mat.size
    # trim because we only look at positive ranges, shift only the velocity axis
    spect_mat = np.fft.fftshift(spect_mat,1)[:M/2,:]

    ''' equivalent code in discrete steps, if we need  it someday '''
#     # 2d, [nSamples,nFrames], complex range-FFT bins
#     cRange_y = np.fft.fft(mat, axis=0)/nSamples  
#     # grab only positive range data
#     cRange_y = cRange_y[0:int(M/2),:]
#     
#     # run doppler fft over complex data by the range axis
#     cMat = np.fft.fft(cRange_y, axis=1)
#     # fftshift the doppler axes so 0 is in center
#     cMat = np.fft.fftshift(cMat,1)
#     # cMat is the complex 2d output for a range-doppler plot


    # raw frequency bins (hz)
    freq_x  = np.fft.fftshift(np.fft.fftfreq(M,range_dt))
    freq_y  = np.fft.fftshift(np.fft.fftfreq(N,velo_dt))

    return spect_mat, freq_x, freq_y 




## PLOT FUNCTIONS

  
def plot_complex_fft(t,y,y2=None):
    ''' plot Power spectrum of positive freqs
    @param t [np-array]: time vector spaced uniformly by dt [s]
    @param y [np-array]: complex signal in the time domain 
    @param y2 [np-array]: (optional) another complex signal in time domain, for comparison
    '''
    
    adc_dBm_origin = Vpp_to_dBm(8.192) # dBm
    
    f, ax = plt.subplots()
    c = len(y)
    freqs, Y = real_fft(t, y,c)
    xdata = freqs
    ax.set_xlabel('Frequency (Hz)', size = 'x-large')  
    
    # convert to power in dBm  (assume 50 ohm)  
    z = adc_dBm_origin + 2*to_dB(np.abs(Y)/50)

    ax.plot(xdata, z,'b',label='Average') 
    ax.set_ylabel(r'$dBm$', size = 'x-large')
    ax.set_title('Compare Averaging (dBm)')
    ax.set_xlim([0,np.max(xdata)])
    
    if y2!=None:
        freqs2, Y2 = complex_fft(t, y2, c)
        z2 = adc_dBm_origin + 2*to_dB(np.abs(Y2)/50)
        ax.plot(xdata, z2,'r',label='Single')
        ax.legend()


## DEMOS
def demo_hilbert():
    duration = 1.0 # s
    fs = 8.156e3 # Hz
    samples = int(fs*duration)
    gen_t = np.arange(samples) / fs
    signal = sig.chirp(gen_t, 20.0, gen_t[-1], 100.0)
    signal *= (1.0 + 0.5 * np.sin(2.0*np.pi*3.0*gen_t) )
    analytic_signal = sig.hilbert(signal)
    # real(analytic_signal) is the original signal
    # imag(analytic_signal) is the transformed signal
    amplitude_envelope = np.abs(analytic_signal)
    instantaneous_phase = np.unwrap(np.angle(analytic_signal))
    instantaneous_frequency = (np.diff(instantaneous_phase) /
                               (2.0*np.pi) * fs)
    
    fig = plt.figure()
    ax0 = fig.add_subplot(211)
    ax0.plot(gen_t, signal, label='signal')
    ax0.plot(gen_t, amplitude_envelope, label='envelope')
#     ax0.set_xlabel("time in seconds")
    ax0.legend()
    ax1 = fig.add_subplot(212)
    ax1.plot(gen_t[1:], instantaneous_frequency)
    ax1.set_xlabel("time in seconds")
    ax1.set_ylabel('frequency')
    ax1.set_ylim(0.0, 120.0)
    plt.show()
    
def hanning_fft(t,y):
    ''' plot the trivial conversion from time domain to frequency domain 
    @param t [np-array]: time vector spaced uniformly by dt [s]
    @param y [np-array]: signal in the time domain 
    '''
    s = y
    dt = t[1] - t[0]
    num_points = t[-1]/dt
    fs = 1.0/dt
    print('sampling frequency: %.2e' % fs)
    f = fs
    
    # fourier coeffs
    Y = np.fft.fft(s)
    
    # determine length of one side of freqs
    N = len(Y)/2+1  # num of real bins

    # make freq bins axis
    X = np.linspace(0, f/2, N, endpoint=True)

    
    plt.plot
    plt.plot(t,s,'r')
    plt.xlabel('Time ($s$)')
    plt.ylabel('Amplitude ($Unit$)')
    plt.title('Signal')
    
    # make a low pass filter
    taps = 100
    f_cut = 200
    h = sig.firwin(numtaps=taps, cutoff=f_cut, nyq=f/2) 
    sfilter = sig.lfilter(h, 1.0, s)
    
    Yhann = np.fft.fft(sfilter)
    
    plt.figure(figsize=(7,3))
    plt.subplot(121)
    plt.plot(t,s,'r')
    plt.title('Time Domain Signal')
    plt.ylim(np.min(s)*3, np.max(s)*3)
    plt.xlabel('Time ($s$)')
    plt.ylabel('Amplitude ($Unit$)')
    
    plt.subplot(122)
    plt.plot(X, 2.0*np.abs(Yhann[:N])/N)

    plt.title('Frequency Domain Signal')
    plt.xlabel('Frequency ($Hz$)')
    plt.ylabel('Amplitude ($Unit$)')
    plt.xlim([0,500])
    plt.annotate("FFT",
                xy=(0.0, 0.1), xycoords='axes fraction',
                xytext=(-0.8, 0.2), textcoords='axes fraction',
                size=30, va="center", ha="center",
                arrowprops=dict(arrowstyle="simple",
                                connectionstyle="arc3,rad=0.2"))
    plt.tight_layout()
 

        
def gen_beat():
    ''' generate a waveform as a beat of 2 freqs '''
    # Number of data points
    n = 2**12
    # Sampling period (in meters)
    dx = 5.0 
    # x coordinates
    x = dx*np.arange(0,n) 
    # wavelength (meters)
    w1 = 100.0 
    # wavelength (meters)
    w2 = 20.0 
    # signal
    fx = np.sin(2*np.pi*x/w1) + 2*np.cos(2*np.pi*x/w2) 
    # plot the three axes representation
    complex_fft(x, fx)



def extents(f):
    '''  Convert a vector into the 2-element extents vector imshow needs
    @param f [np.array]: the vector in ascending order
    @return extents [tuple]: the data-coordinate of tick mark past the last datapoint 
    '''
    span = f[1] - f[0]
    return [f[0] - span / 2, f[-1] + span / 2]

def fix_pulse(v):
    ''' put a signal on the interval [0,1] 
    @param v [np.array]:  the signal vector to adjust
    @return fixed_v [np.array]:  the signal put onto the standard interval
    '''
    a = 0
    b = 1
    fixed_v = (v-np.min(v))*(b-a)/(np.max(v)-np.min(v)) + a
    return fixed_v

def plotSpectrum(F, xf=None, yf=None, tit=None, in_dB=False):
    '''Plot a spectrum array with vectors of x and y frequency spacings
    @param F [M x N np.array]: the complex spectrum mat, plots M as x, N as y axis
    @param xf, yf [np.array]: the data limits of the x and y axes
    @param tit [string]: the title for the plot
    @param in_dB [bool]: a conditional for plotting in dB
    '''

    y = to_power(F.T, in_log=in_dB)

    im = plt.imshow(y,
                    cmap='coolwarm',
                    aspect="auto",
                    interpolation="nearest",
                    origin="lower",)
    cbar = plt.colorbar()
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel('Power dB',)
    if (xf is not None) and (yf is not None):
        im.set_extent(extents(xf) + extents(yf))
        plt.xlabel('f_x (Hz)')
        plt.ylabel('f_y (Hz)')
    if tit is not None:
        plt.title(tit)
    plt.show(block=False)

def demo_plotSpectrum():
        # In seconds
    x = np.linspace(0, 4, 20)
    y = np.linspace(0, 4, 30)
    # Uncomment the next two lines and notice that the spectral peak is no
    # longer equal to 1.0! That's because `makeSpectrum` expects its input's
    # origin to be at the top-left pixel, which isn't the case for the following
    # two lines.
    # x = np.linspace(.123 + 0, .123 + 4, 20)
    # y = np.linspace(.123 + 0, .123 + 4, 30)

    # Sinusoid frequency, in Hz
    x0 = 1.9
    y0 = -2.9


    def makeSpectrum(E, dx, dy, upsample=10):
        """
        Convert a time-domain array `E` to the frequency domain via 2D FFT. `dx` and
        `dy` are sample spacing in x (left-right, 1st axis) and y (up-down, 0th
        axis) directions. An optional `upsample > 1` will zero-pad `E` to obtain an
        upsampled spectrum.
    
        Returns `(spectrum, xf, yf)` where `spectrum` contains the 2D FFT of `E`. If
        `Ny, Nx = spectrum.shape`, `xf` and `yf` will be vectors of length `Nx` and
        `Ny` respectively, containing the frequencies corresponding to each pixel of
        `spectrum`.
    
        The returned spectrum is zero-centered (via `fftshift`). The 2D FFT, and
        this function, assume your input `E` has its origin at the top-left of the
        array. If this is not the case, i.e., your input `E`'s origin is translated
        away from the first pixel, the returned `spectrum`'s phase will *not* match
        what you expect, since a translation in the time domain is a modulation of
        the frequency domain. (If you don't care about the spectrum's phase, i.e.,
        only magnitude, then you can ignore all these origin issues.)
        """
        import numpy.fft as fft
        
#         # DARREN STYLE
#         nX = np.shape(E)[0]
#         nY = np.shape(E)[1]

#         # 2d, [nSamples,nFrames], complex range-FFT bins
#         cRange = np.fft.fft(E, axis=0)/nX
#         
#         # run doppler fft over complex data by the range axis
#         cMat = np.fft.fft(cRange, axis=1)
#         # fftshift the doppler axes so 0 is in center
#         cMat = np.fft.fftshift(cMat)
#         F = cMat
#         xf = fft.fftshift(fft.fftfreq(nX, d=dx))
#         yf = fft.fftshift(fft.fftfreq(nY, d=dy))
#         plotSpectrum(F, xf, yf, 'Shift All',in_dB=True)

        zeropadded = np.array(E.shape) * upsample
        F = fft.fftshift(fft.fft2(E, zeropadded)) / E.size
        xf = fft.fftshift(fft.fftfreq(zeropadded[1], d=dx))
        yf = fft.fftshift(fft.fftfreq(zeropadded[0], d=dy))
        plotSpectrum(F, xf, yf, 'Shift All',)
        
        return (F, xf, yf)

    # Generate data
    im = np.exp(2j * np.pi * (y[:, np.newaxis] * y0 + x[np.newaxis, :] * x0))

    # Generate spectrum and plot
    spectrum, xf, yf = makeSpectrum(im, x[1] - x[0], y[1] - y[0])
    plt.show()

    # Report peak
    peak = spectrum[:, np.isclose(xf, x0)][np.isclose(yf, y0)]
    peak = peak[0, 0]
    print('spectral peak={}'.format(peak))

def chunk_diff(arr,width):
    ''' do a diff operation on an array in chunks of given size
    @param arr [np.array]: the 1d vector of array
    @param width [int]: the length of the chunking 
    @return diff_arr [np.array]: the array after the diff'ing, 
    '''
    
    ''' tests
    a = np.arange(10)
    b = chunk_diff(a,5)
    c = np.ones(5)*(5)
    np.array_equal(b,c)
     
    d = np.arange(13)
    e = chunk_diff(d,5)
    np.array_equal(e,c)
    '''
    n = len(arr)
    reps = np.floor_divide(n,width)
    extra_len = np.mod(n,width)
    if extra_len>0:
        arr = arr[:-extra_len]
    
    mat = np.reshape(arr,(reps,width))
    mti_mat = np.diff(mat,axis=0)
    #    remember diff is a[0] - a[1]
    diff_arr = np.ravel(mti_mat)
    
    # sanity check
    assert len(diff_arr) == n - extra_len - width
    
    return diff_arr
    
def is_odd(num):
    ''' determine if number is odd
    @param num [int]: the number under test
    @return bool: bool of oddness
    '''
    return num % 2 != 0

def zero_pad(array,axis=0):
    ''' zero pad to double lengthen the array before the FFT 
    @param axis [int]: the axis to double with zeros
    '''
    ax, ay = np.shape(array)
    
    if axis==0:
        StrtIdx = int(ax/2)
        zs = np.zeros((2*ax, ay),dtype="complex_")
        zs[StrtIdx:StrtIdx+ax,:] = array
    elif axis==1:
        StrtIdx = int(ay/2)
        zs = np.zeros((ax, 2*ay),dtype='complex_')
        zs[:,StrtIdx:StrtIdx+ay] = array
    return zs

if __name__ == '__main__':

    M = 6
    N = 12
    P = 2
    a = np.arange(M)
    b = np.arange(N)
    
    
    
    demo_plotSpectrum()