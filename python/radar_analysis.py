#!/usr/bin/env python

'''
Python analysis blocks for HG radar
Darren Reis
11/2019 
'''


import os, sys
import pprint
import argparse
import json 
import struct


import numpy as np
import matplotlib.pyplot as plt
import pickle

# hg imports
import fmcw_utils as fmcw


   
    
### ----------------   Experiment Class Definition ------------------ #####
class Experiment():    
    ## space for class-wide variable
    EIRP = 46       # dB
    GR = 13         # dB
    SNR = 13.4      # dB
    IF_GAIN = 0     # dB
    
    do_hann = False   #  do pulse shaping
    do_zpad = False   #  do zero padding

    # Constructor
    def __init__(self, fname):
        ''' Constructor
        @param fname [str]: the filename of the pickle
        '''
        
        exp = fmcw.pickle_load(fname)
        
        self.name = os.path.basename(fname)
#         self.id = Experiment.newid()
        
        if exp['ver'] is not None:
            self.ver = int(exp['ver'])
        
        self.load_experiement(exp)
        
        # do analysis
        self.compute_ranging()
#         self.compute_doppler()
    
    def load_experiement(self, exp):
        if self.ver==0:
            # store away less-often-used hw settings as dict 
            settings = {
                        'fs' : exp['fs']*1e6,       # sampling freq (Hz)
                        'fScale' : 1,               # a-d scaling to FS
                        'B'  : exp['B']*1e6,        # freq swept over ramp (Hz)
                        'cf' : exp['cf']*1e9,       # center frequency of ramp (Hz)
                        'Tp'  : exp['Tp']*1e-6,     # modulation period (s)   
                        'N'  : exp['N'],            # samples per upramp      
                        }
            self.settings = settings
            
            # store numerical params            
            self.dt = 1/exp['fs']                 # s
                    
    
            # store away the actual data of the experiment
            stack_data = np.array(exp['data'])
            self.raw_sig = stack_data[2]
                # raw_sig[:]: the data vector over full time collection
                
            self.ramp = self.primp_data(stack_data)
            

        else:
            print("experiment load version error")    
        
    ## ------ Ramp Collection -------
    def primp_data(self, stack_data):
        '''
        @param stack_data [np.array]: the data grouped in 3 vectors
            stack_data[i][:]: the vector of interest over full time collection
        @return rampData [np.array]: data in sets ready for analysis
            rampData[set_idx][flat_idx]  -- left with flat_idx to match existing code
                set_idx in [0, self.nSets]
                flat_idx in [0, N*nFrames]
        '''
        data, n_ramps, N = self.gather_ramps(stack_data)
        
        # INPUT PARAM
        nFrames = 2        # number of frames to group into a set
              
        rampData, nSets = self.clip_data(data, n_ramps, N, nFrames)
        
        self.nSamples = N              # num samples per up-ramp
        self.nFrames = nFrames         # num ramps per set
        self.nSets = nSets             # num of set in this experiment
        self.settings['Tp'] = N*self.dt
        return rampData
        
    def gather_ramps(self, stack_data):
        '''  take parsed data and form valid ramps and groupings
        @param stack_data [np.array]: an array of [f0, f1, data] 
        @return data [np.array]: an array of data over the full collection
        @return n_full [int]: the number of complete ramps in collection
        @return N [int]: the number of samples per up-ramp
        '''
        b15, b14, data = stack_data
        starts = np.nonzero(b15)[0]
        ends = np.nonzero(b14)[0]
        
        n_starts = len(starts)
        n_ends = len(ends)
        n_full = min(n_starts, n_ends)
        
        N = np.diff(starts)[0]
        import math
        if math.isclose(N,self.settings['N']):
            print("mismatch in samples per up ramp")
            print(N)
            print(self.settings['N'])

        return data, n_full, N
        
    def clip_data(self, data, n_ramps, N, nFrames):
        ''' clip the dataset to be only complete sets in the experiment
        @param data [np.array]: the time domain collection
        @param n_ramps [int]: the number of complete ramps in collection
        @param N [int]: the number of samples in an up-ramp
        @param nFrames [int]:  the number of ramps in a set
        @return dataSets [np.array]: the data clipped to a clean length
            dataSets[set_idx][flat_idx]  -- left 2d (not 3d) to match with analysis class
        '''
        # define set sizes by 
        #    length of ramp (R)
        #    number of ramps (M)
        nSets = int(n_ramps/nFrames)
        clipData = data[:(N*nFrames*nSets)]
        dataSets = np.reshape(clipData, (nSets, N*nFrames))
        return dataSets, nSets
    
    
    # easy wrappers for returning a numeric value
    def get_rangeRes(self):
        fs = self.settings['fs']
        B = self.settings['B']
        Tp = self.settings['Tp']
        return fmcw.f_to_d(fs/self.nSamples, B, Tp)
    
    def get_veloRes(self):
        fs = self.settings['fs']
        B = self.settings['B']
        Tp = self.settings['Tp']
        return fmcw.max_range(fs,B,Tp)
    
    def get_rangeMax(self):
        fs = self.settings['fs']
        B = self.settings['B']
        Tp = self.settings['Tp']
        return fmcw.max_range(fs,B,Tp)
    
    def get_veloMax(self):
        cf = self.settings['cf']
        Tp = self.settings['Tp']
        return fmcw.max_velo(cf, Tp)
    
    def get_Pr(self, r, rcs):
        ''' easy wrapper for getting power received at distance
        @param r [float]: distance of object, in m
        @param rcs [int]: radar cross section, in dBsm 
         '''
        eirp = self.EIRP
        gr = self.GR
        snr = self.SNR
        ifg = self.IF_GAIN
        cf = self.settings['cf']
        return fmcw.get_Pr(r, eirp, rcs, gr, cf, snr, ifg)
    
    ## wrappers for generating vectors
    def get_ranges(self):
        B = self.settings['B']
        Tp = self.settings['Tp']
        if Experiment.do_zpad:
            N = 2*self.nSamples
        else:
            N = self.nSamples
        ranges = fmcw.generate_ranges(N, self.dt, B, Tp)
        return ranges
    
    def get_speeds(self):
        velo_max = self.get_veloMax()
        speeds = fmcw.generate_velos(velo_max, self.nFrames)
        return speeds
    
    
    def compute_ranging(self):
        ''' compute the range power of each frame for channel 0 '''
        use_zeropad = Experiment.do_zpad
        use_han = Experiment.do_hann
        
        if use_han:
            win = np.hanning(self.nSamples)
            norm_fact = np.sum(win)
            win_2d = np.tile(win, (self.nFrames,1))
        else:
            norm_fact = self.nSamples
        
        cRange_list = []
        
        adc_voltage_scalar = self.settings['fScale']  # to Volts
        
        for set_idx in np.arange(self.nSets):
            #     1d:  [nFrames*nSamples]
#             y = self.ramp[set_idx]    
            y = self.ramp[set_idx]    
            
            # real time domain signal
            #    2d: [nFrames,nSamples]
            rMat = np.reshape(y, (self.nFrames, self.nSamples)) 
            
            if use_han:
                rMat = rMat*win_2d
            if use_zeropad:
                rMat = fmcw.zero_pad(rMat,1)
                n_plot = int(self.nSamples)
            else:
                n_plot = int(self.nSamples/2)
            
            # complex range-FFT spectra bins
            #    2d: [nFrames,nSamples]
            cRange = np.fft.fft(rMat, 2*n_plot, axis=1)/norm_fact * adc_voltage_scalar
            cRange = cRange[:,:n_plot]  # throw out negative ranges
            # cRange[frIdx][sampIdx]
      
            cRange_list.append(cRange)
        
        self.range_spectra = cRange_list
        # range_spectra[set_idx][frame_idx][samp_idx]
        # range_spectra[set_idx][0,:] - signal over a chirp, fast-time axis, len=nSamples    
        # range_spectra[set_idx][:,0] - UNUSED
    
    def compute_doppler(self):
        ''' compute the range-doppler plot of each set  '''
        use_han = Experiment.do_hann
        
        cDop_list = []

        for set_idx in np.arange(self.nSets):
            # start with complex range matrix already found
            cMat = np.copy(self.range_spectra[set_idx])
            # cMat[frIdx][sampIdx]
            # cMat[0,:] - signal over a chirp, fast-time axis, len=nSamples    
            # cMat[:,0] - across chirps, slow-time axis, len=nFrames
            
            if use_han:
                win = np.hanning(self.nFrames)
                norm_fact = np.sum(win)
                win_2d = np.tile(win, (self.nSamples,1)).T
                cMat = cMat*win_2d
            else:
                norm_fact = self.nFrames
                
            # complex 2d output for a range-doppler
            #    2d: [nFrames,nSamples]
            cDop = np.fft.fft(cMat, self.nFrames, axis=0)/norm_fact
            # circ-shift velocity axis for 0 at center
            cDop = np.fft.fftshift(cDop.T,1)
            
            cDop_list.append(cDop)
         
        self.dop_spectra = cDop_list
        ''' notice the flip flop of axes here '''
        # dop_spectra[set_idx][samp_idx][frame_idx]
        # dop_spectra[set_idx][0,:] - signal across chirps, slow-time axis, len=nFrames
        # dop_spectra[set_idx][:,0] - signal over a chirp, fast-time axis, len=nSamples
        
    def gen_time_simple(self):
        t = np.arange(self.nSamples*self.nFrames)*self.dt
        return t
               
    def gen_time(self):
        ''' generate time vectors
        @return times [np.arrays]: list of different time representations 
        '''
        
        print("NEEDS REWRITE!!!")
        tSet_1d = np.arange(self.nSamples*self.nFrames)*self.dt
        tSet_2d = np.reshape(np.copy(tSet_1d),(self.nSamples,self.nFrames))  
        # tSet_2d[samp_id, frame_id]   
            # tSet_2d[0,:] - time across chirps, slow-time axis, len=nFrames
            # tSet_2d[:,0] - time over a chirp, fast-time axis, len=nSamples   
        
        tExperiment_3d = []
        tExperiment_flat = []
        for idx in np.arange(self.nSets):
            tStart = self.tStarts[idx]
            
            set_time = np.copy(tSet_2d) + tStart
            flat = np.copy(tSet_1d) + tStart
    
            tExperiment_3d.append(set_time)
            tExperiment_flat.append(flat)
            
        tExperiment_3d = np.array(tExperiment_3d)
        # tExperiment_3d[samp_id, frame_id, col_id]   
            # true timing of samples over the entire collection, as 3d object
        tExperiment_flat = np.array(tExperiment_flat)
        # tExperiment_flat[samp_id]
            # true timing of entire collection, as 1d object
        
        return [tSet_1d, tSet_2d, tExperiment_3d, tExperiment_flat]
    
    ## EXPORTING
    def gen_csv(self):
        ''' generate an output csv file of raw data to use for simulation '''
        # UNTESTED 1/2019
        ''' data is formated as 
        Header1, Header2, ...
        Fff, fff, ...
        Fff, fff, ...
        '''
        
        time_arrays = self.gen_time()
        
        set_time = []
        flat_data = []
        # grab only 1 channel for the entire experiment
        for set_idx in np.arange(self.nSets):
            # grab channel 0
            #     1d:  [nFrames*nSamples]
            y = self.ramp[set_idx][0]
            
            set_time.append(time_arrays[0])
            flat_data.append(y)
        
        set_time = np.array(set_time)
        flat_data = np.array(flat_data)
        
        # store away as .csv
        fname = self.name + '.csv'
        heady = 'Time (s),Voltage (V)'
        gather_data = np.stack(set_time,flat_data)
        np.savetxt(fname, gather_data, delimiter=',', header=heady,)
        
        
    ## PLOTTING    
    
    def _plot(self, ranges, power, lab=None, tit=None, col=None):
        
        fig = plt.gcf()
        ax = plt.gca()
        ##  trivial range-power plot 
        if ax is None:
            fig, ax = plt.subplots()
        
        if lab is not None: 
            if col is not None:
                ax.plot(ranges, power,'-*',color=col, label=lab, )
            else:
                ax.plot(ranges, power,'-*',label=lab, )
            ax.legend()
        else:
            if col is not None:
                ax.plot(ranges, power,'-*',color=col)
            else:
                ax.plot(ranges, power,'-*')
            
        ax.set_xlabel('Range ($m$)')
        ax.set_ylabel('Power ($dB$)')
        if tit is not None:
            ax.set_title(tit)
        ax.set_xlim([0, ranges[-1]/2])
        ax.grid()
        return fig, ax    
    def plot_raw(self):
        ''' quick and dirty view of raw signal and flags'''
        b15, b14, data14 = self.raw_sig
        t = np.arange(len(data14))*self.dt
        fig, ax = plt.subplots()
        ax.plot(t,data14, '-b', label='raw data')
        ax.set_xlabel('time (s)')
        ax.set_ylabel('ADC Values')
        ax.tick_params(axis='y', labelcolor='b')
        ax2 = ax.twinx()
        ax2.plot(t,b15, '*r', label='flag0')
        ax2.plot(t,b14, '^m', label='flag1')
        ax2.set_ylabel('Flags')
        ax2.tick_params(axis='y', labelcolor='r')
        ax.set_title('Data and Flags')
        ax.legend()
    def plot_quick_fft(self):       
        y = self.ramp[0]
        t = self.gen_time_simple()*1e6
        fy = np.fft.fft(y)
        M = len(y)
        ft = np.fft.fftfreq(M,self.dt)[:int(M/2)]/1e6
        fig, ax = plt.subplots(2,1)
        ax[0].plot(t, y,'-*')
        ax[1].plot(ft,fy[:int(M/2)],'--*')
        fig.suptitle('Raw and FFT Signal Quick Plot')    
        ax[0].set_xlabel('Time ($us$)')
        ax[0].set_ylabel('-')
        ax[0].grid()
        ax[1].set_xlabel('Frequency ($MHz$)')
        ax[1].set_ylabel('Spectra')
        ax[1].grid()
        return fig, ax  
        
    def plot_range(self, spectra, lab=None, col=None):
        ''' quick dirty plot of a vector of fft spectra 
        @param spectra [np.array]: vector of spectra before put in power mag or dB
        @param lab [str]: label for this curve
        @param col [str]: color for this curve
        '''
        ranges = self.get_ranges()
        range_power = fmcw.to_power(spectra,in_log=True)
        return self._plot(ranges,range_power)
    
    def plot_range_00(self, lab, col):    
        ''' quick dirty plot of range fft for S0F0, with label and color
        @param lab [str]: label for this curve
        @param col [str]: color for this curve
        '''
        y = self.range_spectra[0][0,:]
        ranges = self.get_ranges()
        range_power = fmcw.to_power(y,in_log=True)
        return self._plot(ranges, range_power, lab, col=col)
        
    
    def plot_time_f(self,fr_idx=0):
        ''' plot the time domain literal, for a given frame '''
        x = np.arange(self.nSamples)*self.dt
        adc_voltage_scalar = self.settings['fScale']  # to Volts
        
        #   raw_sig[samp_idx]
        #      1d:  [nSamples*nFrames]
        y = self.raw_sig[fr_idx*self.nSamples:(1+fr_idx)*self.nSamples]
        y = y * adc_voltage_scalar
        
        tit = 'Time Domain Plot Frame %d' % fr_idx
        fig, ax = plt.subplots()
        ax.plot(x,y)
        ax.set_title(tit)
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Volts (V)')
        
        ax.grid()
        return fig, ax   
        
    def plot_range_sf(self,set_idx=0,fr_idx=0):
        ''' plot a specific set and frame case, TESTED 11/8 '''
        ranges = self.get_ranges()
        y = self.range_spectra[set_idx][fr_idx,:]
        range_power = fmcw.to_power(y, in_log=True)
        lab = 'Set%d,Fr%d' % (set_idx, fr_idx)
        tit = '%s\nRange PSD' % self.name
        return self._plot(ranges, range_power, lab, tit)
        
    def plot_range_avg_s(self,set_idx=0,lab=None,col=None,verbose=False):
        ''' plot the avg power of a set, TESTED 11/8 
        @param set_idx [int]: the set to plot
        @param lab [str]: label for this curve
        @param col [str]: color for this curve
        '''
        if verbose:
            ''' plot a couple of different frames to visually see things '''
            for fr_id in [0, 10, 80, 100]:
                self.plot_range_sf(set_idx,fr_id)
        
        plt.figure()
        ranges = self.get_ranges()
        y = self.range_spectra[set_idx]
        
        # non-coherent averaging happens !!AFTER!! square law 
        y_pwr = fmcw.to_power(y)
        # take average of fft results across frames
        avRange_pwr = np.mean(y_pwr,0)            #    1D: [nSamples]
        range_power_dB = fmcw.to_dB(avRange_pwr)
        
        tit = '%s\nRange Avg PSD' % self.name
        if lab is None:
            lab = 'Set%d' % set_idx
        if col is None:
            return self._plot(ranges, range_power_dB, lab, tit)
        else:
            return self._plot(ranges, range_power_dB, lab, tit, col=col)
        

    def plot_range_sequence(self,set_idx=0):
        ''' plot the sequence of target peaks over the different Sets 
            UNTESTED since mid November 
        '''
        ranges = self.get_ranges()
        t_frames = np.arange(self.nFrames)*(self.dt*self.nSamples)
        
        range_list = []
        for fr_idx in np.arange(self.nFrames):
            y = self.range_spectra[set_idx][fr_idx]
            
            ## find peak Power using CFAR
            window_len = np.int(.4*len(ranges))
            # find peak
            # TODO: error protection for when no peak is found
            peak_id = fmcw.detect_peaks(y, num_train=window_len,
                                         num_guard=2, rate_fa=1e-5)
            
            x_peak = ranges[peak_id[0][0]]
            range_list.append(x_peak)
            
        # plot target range over the course of the experiment
        figA, axA = plt.subplots()    
        
        axA.plot(t_frames, x_peak, '*', )
        axA.set_xlabel('Set Time (s)')
        axA.set_ylabel('Range (m)')
        axA.set_title('%s\nTarget Range vs Time' % self.name)
        
        return figA, axA
    
    def plot_refline(self,rcs):
        ''' generate a reference curve for an object with certain RCS
        @param rcs [int]: radar cross section, in dBsm 
        '''
        ranges = self.get_ranges()
        fun = lambda x: self.get_Pr(x, rcs)
        reflevel = np.fromiter(map(fun, ranges),dtype=np.float)
        plt.plot(ranges, reflevel, '--', label='%d dBsm Ref'%rcs)
        plt.legend()
        
    def plot_doppler_s(self,set_idx=0):
        ''' plot a single frame of range-doppler plot for given set 
        TESTED 11/8  '''
        range_max = self.get_rangeMax()
        velo_max = self.get_veloMax()
        ranges = self.get_ranges()
        speeds = self.get_speeds()
        
        cDop = self.dop_spectra[set_idx]
        
        fig, ax = plt.subplots()
        tit = '%s\nRange-Doppler Set#%d' % (self.name, set_idx)
        fmcw.plotSpectrum(cDop, ranges, speeds, tit, in_dB=True)
        ax.set_xlabel('Range ($m$)')
        ax.set_ylabel('Velocity ($m/s$)') 
        ax.set_xlim([0, range_max])
        ax.set_ylim([-velo_max/2, velo_max/2])
        
        return fig, ax
    
    def plot_anim_doppler(self):
        ''' plot an animation of the range-doppler over time
        TESTED 11/8 '''
        range_max = self.get_rangeMax()
        velo_max = self.get_veloMax()
        ranges = self.get_ranges()
        speeds = self.get_speeds()
        plot_setup = [range_max, velo_max, ranges, speeds]
        
#         self.anime = doppler_anime(self.nSets, plot_setup)
#         self.anime.animate(self.dop_spectra, self.tStarts, self.name)
        
    # Destructor
    
    def __del__(self):
        class_name = self.__class__.__name__
        print(class_name, "destroyed")  
    

def get_data_files():
    ''' return a list of files in directory '''
    list_of_files = []
    with os.scandir('dat/') as entries:
        for entry in entries:
            if entry.is_file():
                list_of_files.append(entry)
    return list_of_files


if __name__ == '__main__':
    
    print("radar_analysis.py startup\n")
    
    
    # 2020_03_19_112250.p   -  50khz sinewave for 600us, ramp 200u / 10d
    # 2020_03_19_112622.p   -  inside test, wall at 2.5m
    
    # darrens house - on 4th floor
    # 2020_03_20_104238.p   -  at darren's house, looking across pool
    # 2020_03_20_104827.p   -  at darren's, looking inside wall at 4 or 5 m
    # 2020_03_20_105842.p   -  at darren's, looking across pool, tilted 15 or 30 degrees
    # 2020_03_20_110633.p   -  looking across, tilted 45
    # 2020_03_20_110828.p   -  looking accross, tilt 45, vertical config
    # 2020_03_20_111018.p   -  looking across, no tilt, vertical config
    
    # now with 2.6 - 2.8A instead of 2.4A
    # 2020_03_20_111230.p   -  across, no tilt, vertical
    # 2020_03_20_111428.p   -  across, 45 tilt, vertical
    # 2020_03_20_111553.p   -  across, 45, horiztonal config
    # 2020_03_20_111727.p   -  across no tilt, horiz   (may have a cable problem here.  tbd)

    parser = argparse.ArgumentParser(description=''' 
    This script is a controller for the HG radar analysis.  Use it for analyzing
    the pickle log files of the hg radar.''')

    # ACTIONS
    parser.add_argument('-o','--open', action="store", 
                        help=''' the pickle file to do analysis on. pickle files
                        are kept in hg_radar/dat/ and are specified as "-o FILE", 
                        where FILE is the filename and extension''')
    parser.add_argument('-c','--compare', action="store_true", 
                        help=''' compare range plots in dat''')
    
    
    # get arguments
    args = parser.parse_args(sys.argv[1:])
    
    from os.path import dirname, abspath
    homeDir = dirname(dirname(abspath(__file__)))  # /hg_radar
    datDir = os.path.join(homeDir, 'dat')
    
    
    if args.compare:
#         trials = [
# #                    ['2020_03_20_104238.p', 'Outside, Horiz, 0deg'],
# #                    ['2020_03_20_104827.p', 'Inside, wall at 5m'],
# #                    ['2020_03_20_105842.p', 'Outside, Horiz, 15deg'],
# #                     ['2020_03_20_110633.p', 'Outside, Horiz, 45deg'],
#                     ['2020_03_20_110828.p', 'Outside, Vert, 45deg'],
#                     ['2020_03_20_111018.p', 'Outside, Vert, 0deg'],
#                     ]
#         lFiles = np.array(trials).T[0]
#         labs = np.array(trials).T[1]
        
        hp_trials = [
#                     ['2020_03_20_111230.p', 'Outside, Vert, 0deg'],
#                     ['2020_03_20_111428.p', 'Outside, Vert, 45deg'],
                    ['2020_03_20_111553.p', 'Outside, Horiz, 45deg'],
#                     ['2020_03_20_111727.p', 'Outside, Horiz, 0deg'],
                      
#                     ['2020_03_20_152021.p', 'Ledge, Horiz, 0deg'],
                    ['2020_03_20_152148.p', 'Ledge, Horiz, 30deg'],
                    ['2020_03_20_152434.p', 'Ledge, Horiz, 45deg'],
#                     ['2020_03_20_152623.p', 'Ledge, Horiz, 45deg, gain 2,14'],

                    ]
        lFiles = np.array(hp_trials).T[0]
        labs = np.array(hp_trials).T[1]
        
        nFiles = len(lFiles)
        colors = ['b','r','m','g','pink','orange']
        for idx, the_file in enumerate(lFiles):
            file_path = os.path.join(*[homeDir, 'dat2', the_file])
            exp = Experiment(file_path)
            exp.plot_range_00(labs[idx], colors[idx])
            
    else:
        if args.open is None:
            ''' no pickle file given to load, grab newest file in the folder '''  
            import ntpath 
            list_of_files = get_data_files()
            fname = max(list_of_files, key=ntpath.getctime)
        else:
            fname = os.path.join(datDir, args.open)
    
        
        
        exp = Experiment(fname)
            
    
        #--------------------------------------------------------------------------
        # FMCW ANALYSIS 
        #--------------------------------------------------------------------------
        #     exp.plot_raw()
        #     exp.plot_range_00('S0F0','r')       # plot range for set 0, frame 0
        #     exp.plot_quick_fft()                # plot time+freq for set 0
        #     exp.plot_time_f(0)                  # plot time domain of frame N
        #     exp.plot_range_sf(0, 0)             # plot range graph for set N, frame M
        exp.plot_range_avg_s(0)             #plot range graph averaged over full exp
        #     exp.plot_refline(150)
        ## plots built on doppler data
        #     exp.plot_doppler_s(0)
    
        ## plots built on peak finding 
        #     exp.plot_range_sequence(0)
    

    plt.show()
    
   
    print("\nradar_analysis.py shutdown")
    exit()